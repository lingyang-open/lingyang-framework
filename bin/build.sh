#! /bin/bash
# 上一级目录
ROOT_PATH=$(dirname "$PWD")
echo "root path: $ROOT_PATH"
sleep 2

cd $ROOT_PATH/lingyang-dependencies && mvn -T 1C clean install -Dmaven.test.skip=true && cd ..
cd $ROOT_PATH && mvn clean install -T 1C -Dmaven.test.skip=true  -Dmaven.compile.fork=true