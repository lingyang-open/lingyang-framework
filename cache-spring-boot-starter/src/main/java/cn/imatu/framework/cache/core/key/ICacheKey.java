package cn.imatu.framework.cache.core.key;

import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * <blockquote><pre>
 *  private final String key;
 *  private final String keyArgs;
 *  private final int expire;
 *  private final TimeUnit unit;
 *
 *  private final String hashKey;
 *  private final String hashKeyArgs;
 * </pre></blockquote>
 * @author shenguangyang
 */
public interface ICacheKey {

    /**
     * 支持指定变量名, eg: orderInfo::${orderNo}
     * 支持指定符号: eg: orderInfo::%s
     */
    String getKey();

    default String getHashKey() {
        return "";
    }

    int getExpire();

    TimeUnit getUnit();

    /**
     * key中的变量用%s代替, args就是每个变量
     */
    @SuppressWarnings("unchecked")
    default String formatKey(Object... args) {
        if (args == null || args.length == 0) {
            return null;
        }
        if (args[0] instanceof Map) {
            Map<String, Object> argMap = (Map<String, Object>) args[0];
            String[] key = {getKey()};
            argMap.forEach((k, v) -> {
                String kayPlaceholder = String.format("${%s}", k);
                if (v != null) {
                    key[0] = key[0].replace(kayPlaceholder, v.toString());
                }
            });
            return key[0];
        }
        return String.format(getKey().replaceAll("\\$\\{[^}]*}", "%s"), args);
    }

    /**
     * hashKey中的变量用%s代替, args就是每个变量
     */
    @SuppressWarnings("unchecked")
    default String formatHashKey(Object... args) {
        if (args == null || args.length == 0) {
            return null;
        }

        if (args[0] instanceof Map) {
            String[] key = {getHashKey()};
            Map<String, Object> argMap = (Map<String, Object>) args[0];
            argMap.forEach((k, v) -> {
                String kayPlaceholder = String.format("${%s}", k);
                if (v != null) {
                    key[0] = key[0].replace(kayPlaceholder, v.toString());
                }
            });
            return key[0];
        }
        return String.format(getHashKey().replaceAll("\\$\\{[^}]*}", "%s"), args);
    }
}
