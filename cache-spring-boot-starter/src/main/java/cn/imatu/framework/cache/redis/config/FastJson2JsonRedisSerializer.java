package cn.imatu.framework.cache.redis.config;

import cn.imatu.framework.tool.core.CollectionUtils;
import cn.hutool.core.collection.ConcurrentHashSet;
import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONReader;
import com.alibaba.fastjson2.JSONWriter;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.SerializationException;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Redis使用FastJson序列化
 *
 * @author shenguangyang
 */
public class FastJson2JsonRedisSerializer implements RedisSerializer<Object> {
    public static final Charset DEFAULT_CHARSET = StandardCharsets.UTF_8;
    private static final Set<String> AUTO_TYPE_FILTER_SET = new ConcurrentHashSet<>();
    private final Class<?> clazz;

    public FastJson2JsonRedisSerializer(Class<?> clazz) {
        super();
        this.clazz = clazz;
    }

    @Override
    public byte[] serialize(Object t) throws SerializationException {
        if (t == null) {
            return new byte[0];
        }
        return JSON.toJSONString(t, JSONWriter.Feature.WriteClassName).getBytes(DEFAULT_CHARSET);
    }

    public static void addDeserializeAutoTypeFilter(String... names) {
        if (CollectionUtils.isEmpty(names)) {
            return;
        }
        AUTO_TYPE_FILTER_SET.addAll(Arrays.stream(names).collect(Collectors.toSet()));
    }

    @Override
    public Object deserialize(byte[] bytes) throws SerializationException {
        if (bytes == null || bytes.length <= 0) {
            return null;
        }
        String str = new String(bytes, DEFAULT_CHARSET);
        // 遇到auto type可以自定义
        JSONReader.AutoTypeBeforeHandler autoTypeBeforeHandler = JSONReader.autoTypeFilter(
                AUTO_TYPE_FILTER_SET.toArray(new String[]{})
        );

        return JSON.parseObject(str, this.clazz, autoTypeBeforeHandler, JSONReader.Feature.FieldBased, JSONReader.Feature.SupportAutoType);
    }

    protected JavaType getJavaType(Class<?> clazz) {
        return TypeFactory.defaultInstance().constructType(clazz);
    }
}
