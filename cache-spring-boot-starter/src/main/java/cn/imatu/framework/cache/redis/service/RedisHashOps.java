package cn.imatu.framework.cache.redis.service;

import cn.imatu.framework.cache.core.service.HashOps;
import cn.imatu.framework.cache.redis.utils.RedisUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author shenguangyang
 */
@Component
@SuppressWarnings(value = {"unchecked", "rawtypes"})
public class RedisHashOps implements HashOps {
    @Resource
    private RedisTemplate redisTemplate;

    @Override
    public <T> void put(String key, String entryKey, T entryValue) {
        RedisUtils.HashOps.hPut(key, entryKey, entryValue);
    }

    @Override
    public <HK, HV> void putAll(Object key, Map<? extends HK, ? extends HV> m) {
        redisTemplate.opsForHash().putAll(String.valueOf(key), m);
    }

    @Override
    public <T> boolean putIfAbsent(String key, Object entryKey, T entryValue) {
        return RedisUtils.HashOps.hPutIfAbsent(key, entryKey, entryValue);
    }

    @Override
    public <T> T get(String key, Object entryKey) {
        return (T) RedisUtils.HashOps.hGet(key, entryKey);
    }

    @Override
    public <T> Map<String, T> getAll(String key) {
        return (Map<String, T>) RedisUtils.HashOps.hGetAll(key);
    }

    @Override
    public <T> List<T> multiGet(String key, Collection<Object> entryKeys) {
        return (List<T>) RedisUtils.HashOps.hMultiGet(key, entryKeys);
    }

    @Override
    public long delete(String key, Object... entryKeys) {
        return RedisUtils.HashOps.hDelete(key, entryKeys);
    }

    @Override
    public boolean exists(String key, String entryKey) {
        return RedisUtils.HashOps.hExists(key, entryKey);
    }

    @Override
    public long incrementBy(String key, Object entryKey, long increment) {
        return RedisUtils.HashOps.hIncrBy(key, entryKey, increment);
    }

    @Override
    public double incrementByFloat(String key, Object entryKey, double increment) {
        return RedisUtils.HashOps.hIncrByFloat(key, entryKey, increment);
    }

    @Override
    public Set<Object> keys(String key) {
        return RedisUtils.HashOps.hKeys(key);
    }

    @Override
    public <T> List<T> values(String key) {
        return (List<T>) RedisUtils.HashOps.hValues(key);
    }
}
