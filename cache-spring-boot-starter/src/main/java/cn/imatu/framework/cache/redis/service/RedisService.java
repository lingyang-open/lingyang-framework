package cn.imatu.framework.cache.redis.service;

import cn.imatu.framework.cache.core.key.ICacheKey;
import cn.imatu.framework.cache.core.service.CacheService;
import cn.imatu.framework.cache.core.service.HashOps;
import cn.imatu.framework.cache.core.service.ListOps;
import cn.imatu.framework.cache.core.service.ValueOps;
import cn.imatu.framework.cache.core.service.*;
import cn.imatu.framework.cache.redis.utils.RedisUtils;
import org.springframework.data.redis.core.BoundSetOperations;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * spring redis 工具类
 *
 * @author shenguangyang
 **/
@SuppressWarnings(value = {"unchecked", "rawtypes"})
@Component
public class RedisService implements CacheService {
    @Resource
    public RedisTemplate redisTemplate;

    @Resource
    private RedisHashOps hashOps;

    @Resource
    private RedisValueOps valueOps;

    @Resource
    private RedisListOps listOps;

    @Override
    public ListOps opsForList() {
        return listOps;
    }

    @Override
    public ValueOps opsForValue() {
        return valueOps;
    }

    @Override
    public HashOps opsForHash() {
        return hashOps;
    }

    /**
     * 缓存基本的对象，Integer、String、实体类等
     *
     * @param key   缓存的键值
     * @param value 缓存的值
     */
    public <T> void setCacheObject(final String key, final T value) {
        redisTemplate.opsForValue().set(key, value);
    }

    /**
     * 缓存基本的对象，Integer、String、实体类等
     *
     * @param key       缓存key
     * @param value     缓存的值
     * @param keyParams key参数
     */
    public <T> void setCacheObject(final ICacheKey key, final T value, Object... keyParams) {
        redisTemplate.opsForValue().set(key.formatKey(keyParams), value);
    }

    /**
     * 缓存基本的对象，Integer、String、实体类等
     *
     * @param key      缓存的键值
     * @param value    缓存的值
     * @param timeout  时间
     * @param timeUnit 时间颗粒度
     */
    public <T> void setCacheObject(final String key, final T value, final Long timeout, final TimeUnit timeUnit) {
        redisTemplate.opsForValue().set(key, value, timeout, timeUnit);
    }

    @Override
    public boolean delete(String key) {
        return RedisUtils.KeyOps.delete(key);
    }

    @Override
    public long delete(Collection<String> keys) {
        return RedisUtils.KeyOps.delete(keys);
    }

    @Override
    public byte[] dump(String key) {
        return RedisUtils.KeyOps.dump(key);
    }

    @Override
    public void restore(String key, byte[] value, long timeToLive, TimeUnit unit) {
        RedisUtils.KeyOps.restore(key, value, timeToLive, unit);
    }

    @Override
    public boolean hasKey(String key) {
        return RedisUtils.KeyOps.hasKey(key);
    }

    @Override
    public Set<String> keys(String pattern) {
        return RedisUtils.KeyOps.keys(pattern);
    }

    /**
     * 减1
     *
     * @param key       缓存键
     * @param keyParams key参数
     * @return 剩余数量
     */
    public Long decrement(ICacheKey key, String keyParams) {
        return redisTemplate.opsForValue().decrement(key.formatKey(keyParams));
    }

    /**
     * 设置有效时间
     *
     * @param key     Redis键
     * @param timeout 超时时间
     * @return true=设置成功；false=设置失败
     */
    @Override
    public boolean expire(final String key, final long timeout) {
        return expire(key, timeout, TimeUnit.SECONDS);
    }

    /**
     * 设置有效时间
     *
     * @param key       Redis键
     * @param keyParams key的参数
     * @return true=设置成功；false=设置失败
     */
    @Override
    public boolean expire(final ICacheKey key, final Map<String, Object> keyParams) {
        return expire(key.formatKey(keyParams), key.getExpire(), key.getUnit());
    }

    /**
     * 设置有效时间
     *
     * @param key     Redis键
     * @param timeout 超时时间
     * @param unit    时间单位
     * @return true=设置成功；false=设置失败
     */
    @Override
    public boolean expire(final String key, final long timeout, final TimeUnit unit) {
        return Boolean.TRUE.equals(redisTemplate.expire(key, timeout, unit));
    }

    /**
     * 获得缓存的基本对象。
     *
     * @param key 缓存键值
     * @return 缓存键值对应的数据
     */
    public <T> T getCacheObject(final String key) {
        ValueOperations<String, T> operation = redisTemplate.opsForValue();
        return operation.get(key);
    }

    /**
     * 获得缓存的基本对象。
     *
     * @param key       key
     * @param keyParams key参数
     * @return 缓存键值对应的数据
     */
    public <T> T getCacheObject(final ICacheKey key, final Object... keyParams) {
        ValueOperations<String, T> operation = redisTemplate.opsForValue();
        return operation.get(key.formatKey(keyParams));
    }

    /**
     * 删除单个对象
     *
     * @param key
     */
    public boolean deleteObject(final String key) {
        return Boolean.TRUE.equals(redisTemplate.delete(key));
    }

    /**
     * 删除单个对象
     *
     * @param key
     * @param keyParams key参数
     */
    public boolean deleteObject(final ICacheKey key, final Object... keyParams) {
        return Boolean.TRUE.equals(redisTemplate.delete(key.formatKey(keyParams)));
    }

    /**
     * 删除集合对象
     *
     * @param collection 多个对象
     * @return 删除的数量
     */
    public long deleteObject(final Collection collection) {
        return redisTemplate.delete(collection);
    }

    /**
     * 缓存List数据
     *
     * @param key      缓存的键值
     * @param dataList 待缓存的List数据
     * @return 缓存的对象
     */
    public <T> long setCacheList(final String key, final List<T> dataList) {
        Long count = redisTemplate.opsForList().rightPushAll(key, dataList);
        return count == null ? 0 : count;
    }

    /**
     * 获得缓存的list对象
     *
     * @param key 缓存的键值
     * @return 缓存键值对应的数据
     */
    public <T> List<T> getCacheList(final String key) {
        return redisTemplate.opsForList().range(key, 0, -1);
    }

    /**
     * 缓存Set
     *
     * @param key     缓存键值
     * @param dataSet 缓存的数据
     * @return 缓存数据的对象
     */
    public <T> BoundSetOperations<String, T> setCacheSet(final String key, final Set<T> dataSet) {
        BoundSetOperations<String, T> setOperation = redisTemplate.boundSetOps(key);
        for (T t : dataSet) {
            setOperation.add(t);
        }
        return setOperation;
    }

    /**
     * 获得缓存的set
     */
    public <T> Set<T> getCacheSet(final String key) {
        return redisTemplate.opsForSet().members(key);
    }

    /**
     * 缓存Map
     *
     * @param key
     * @param dataMap
     */
    public <T> void setCacheMap(final String key, final Map<String, T> dataMap) {
        if (dataMap != null) {
            redisTemplate.opsForHash().putAll(key, dataMap);
        }
    }

    /**
     * 获得缓存的Map
     *
     * @param key key
     * @return map
     */
    public <T> Map<String, T> getCacheMap(final String key) {
        return redisTemplate.opsForHash().entries(key);
    }

    /**
     * 往Hash中存入数据
     *
     * @param key   Redis键
     * @param hKey  Hash键
     * @param value 值
     */
    public <T> void setCacheMapValue(final String key, final String hKey, final T value) {
        redisTemplate.opsForHash().put(key, hKey, value);
    }

    /**
     * 获取Hash中的数据
     *
     * @param key  Redis键
     * @param hKey Hash键
     * @return Hash中的对象
     */
    public <T> T getCacheMapValue(final String key, final String hKey) {
        HashOperations<String, String, T> opsForHash = redisTemplate.opsForHash();
        return opsForHash.get(key, hKey);
    }

    /**
     * 获取多个Hash中的数据
     *
     * @param key   Redis键
     * @param hKeys Hash键集合
     * @return Hash对象集合
     */
    public <T> List<T> getMultiCacheMapValue(final String key, final Collection<Object> hKeys) {
        return redisTemplate.opsForHash().multiGet(key, hKeys);
    }

//    /**
//     * list集合相关操作
//     */
//    public class ListOps {
//        /**
//         * 移除集合中右边的元素在等待的时间里，如果超过等待的时间仍没有元素则退出。
//         * @param key 键
//         * @param seconds 超时等待时间
//         * @return list集合中的元素
//         */
//        public <T> T rightPop(final String key, final long seconds) {
//            return (T) redisTemplate.opsForList().rightPop(key, seconds, TimeUnit.SECONDS);
//        }
//
//        /**
//         * 在变量左边添加元素值。
//         * @param key 键
//         * @param value 值
//         */
//        public void leftPush(final String key, Object value) {
//            redisTemplate.opsForList().leftPush(key, value);
//        }
//    }


}
