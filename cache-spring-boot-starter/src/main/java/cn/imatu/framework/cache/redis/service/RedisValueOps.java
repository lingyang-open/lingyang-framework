package cn.imatu.framework.cache.redis.service;

import cn.imatu.framework.cache.core.key.ICacheKey;
import cn.imatu.framework.cache.core.service.ValueOps;
import cn.imatu.framework.cache.redis.utils.RedisUtils;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author shenguangyang
 */
@Component
@SuppressWarnings("unchecked")
public class RedisValueOps implements ValueOps {
    @Override
    public <T> void set(String key, T value) {
        RedisUtils.StringOps.set(key, value);
    }

    @Override
    public <T> void set(String key, T value, long timeout, TimeUnit unit) {
        RedisUtils.StringOps.setEx(key, value, timeout, unit);
    }

    @Override
    public <T> void set(T value, ICacheKey cacheKey, Object... keyArgs) {
        this.set(cacheKey.formatKey(keyArgs), value, cacheKey.getExpire(), cacheKey.getUnit());
    }

    @Override
    public <T> void set(ICacheKey cacheKey, Map<String, Object> keyArgs, T value) {
        this.set(cacheKey.formatKey(keyArgs), value, cacheKey.getExpire(), cacheKey.getUnit());
    }

    @Override
    public <T> void set(String key, T value, long timeout) {
        RedisUtils.StringOps.setEx(key, value, timeout);
    }

    @Override
    public <T> T get(String key) {
        return (T) RedisUtils.StringOps.get(key);
    }

    @Override
    public <T> T getAndSet(String key, T newValue) {
        return (T) RedisUtils.StringOps.getAndSet(key, newValue);
    }

    @Override
    public long incrementBy(String key, long increment) {
        return RedisUtils.StringOps.incrBy(key, increment);
    }

    @Override
    public double incrementByFloat(String key, double increment) {
        return RedisUtils.StringOps.incrByFloat(key, increment);
    }
}
