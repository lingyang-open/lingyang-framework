package cn.imatu.framework.cache;

import cn.hutool.core.lang.UUID;
import cn.imatu.framework.cache.core.key.DefaultCacheKey;
import cn.imatu.framework.cache.core.service.CacheService;
import lombok.Data;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.*;

/**
 * @author shenguangyang
 */
@SpringBootTest
public class TestCache {
    @Resource
    private RedisTemplate<String, Object> redisTemplate;
    @Resource
    private CacheService cacheService;

    public static void main(String[] args) {
        Map<String, String> map = new HashMap<>();
        map.put("tenantId", "123123");
        map.put("userId", "234234234234");
        System.out.println(DefaultCacheKey.TEST.formatKey("123", "456"));
        System.out.println(DefaultCacheKey.TEST.formatKey(map));
    }

    @Data
    public static class Demo01 implements Serializable {
        private String data1;
        private List<String> data2;
        private Demo02 demo02;

        public Demo01() {
            this.data1 = UUID.randomUUID().toString();
            this.data2 = Arrays.asList( UUID.randomUUID().toString(),  UUID.randomUUID().toString());
            this.demo02 = new Demo02();
            this.demo02.setData3(UUID.randomUUID().toString());
            this.demo02.data4 = Arrays.asList( UUID.randomUUID().toString(),  UUID.randomUUID().toString());
        }
    }


    @Data
    public static class Demo implements Serializable {
        private String data1;
        private List<String> data2;
        private Demo02 demo02;

        public Demo() {
            this.data1 = UUID.randomUUID().toString();
            this.data2 = Arrays.asList( UUID.randomUUID().toString(),  UUID.randomUUID().toString());
            this.demo02 = new Demo02();
            this.demo02.setData3(UUID.randomUUID().toString());
            this.demo02.data4 = Arrays.asList( UUID.randomUUID().toString(),  UUID.randomUUID().toString());
        }
    }

    @Data
    public static class Demo02 implements Serializable {
        private String data3;
        private List<String> data4;

    }
    @Test
    public void test() {
        redisTemplate.opsForValue().set("test", new Demo01());
        Demo01 demo01 = (Demo01) redisTemplate.opsForValue().get("test");
        System.out.println(demo01);
    }

    @Test
    public void test02() {
        cacheService.opsForValue().set("test", new Demo01());
        Demo01 demo01 = cacheService.opsForValue().get("test");
        System.out.println(demo01);
    }

    @Test
    public void test03() {
        Demo demo = cacheService.opsForValue().get("test");
        System.out.println(demo);
    }
}
