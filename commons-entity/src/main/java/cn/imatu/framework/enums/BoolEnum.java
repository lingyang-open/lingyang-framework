package cn.imatu.framework.enums;

import cn.hutool.core.util.BooleanUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author shenguangyang
 */
@Getter
@AllArgsConstructor
public enum BoolEnum {
    TRUE("1"),
    FALSE("0");
    private final String code;

    public static BoolEnum of(Boolean bool) {
        return Boolean.TRUE.equals(bool) ? TRUE : FALSE;
    }

    public static BoolEnum of(String str) {
        return TRUE.getCode().equals(str) ? TRUE : FALSE;
    }

    public static Boolean toObject(String valueStr) {
        return BooleanUtil.toBooleanObject(valueStr);
    }
}
