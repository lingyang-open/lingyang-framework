package cn.imatu.framework.exception;

/**
 * @author shenguangyang
 */
public class AsyncTaskException extends BaseException {
    public AsyncTaskException() {
    }

    public AsyncTaskException(Integer code, String message) {
        super(code, message);
    }

    public AsyncTaskException(String message) {
        super(message);
    }
}
