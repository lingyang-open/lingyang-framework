package cn.imatu.framework.exception;

import cn.hutool.core.util.StrUtil;
import org.springframework.http.HttpStatus;

/**
 * Extends your error codes in your App by implements this Interface.
 *
 * <code>
 * private final Integer code;
 * private final Integer message;
 * </code>
 */
public interface BaseError {

    Integer getCode();

    String getMessage();

    /**
     * 200-OK	处理成功
     * 202 - Accepted	        服务器已接受请求，但尚未处理	请使用原参数重复请求一遍 <br/>
     * 204 - No Content	        处理成功，                无返回Body <br/>
     * 400 - Bad Request	    协议或者参数非法	        请根据接口返回的详细信息检查您的程序 <br/>
     * 401 - Unauthorized	    签名验证失败或者登录超时	    请检查签名参数和方法是否都符合签名算法要 <br/>
     * 403 - Forbidden	        权限异常	请开通资源相关权限。请联系产品或商务申请 <br/>
     * 404 - Not Found	        请求的资源不存在	        请检查需要查询的id或者请求URL是否正确 <br/>
     * 429 - Too Many Requests	请求超过频率限制	        请求未受理，请降低频率后重试 <br/>
     * 500 - Server Error	    系统错误	                按具体接口的错误指引进行重试 <br/>
     * 502 - Bad Gateway	    服务下线，暂时不可用	    请求无法处理，请稍后重试 <br/>
     * 503 - Service Unavailable    服务不可用，过载保护	请求无法处理，请稍后重试 <br/>
     */
    HttpStatus getStatus();

    default String formatMsg(Object... msgParams) {
        return StrUtil.format(getMessage(), msgParams);
    }
}