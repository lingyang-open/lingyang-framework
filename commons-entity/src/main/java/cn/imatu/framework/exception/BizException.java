package cn.imatu.framework.exception;

import cn.hutool.core.util.StrUtil;

/**
 * 自定义异常
 *
 * @author shenguangyang
 */
public class BizException extends BaseException {
    private static final long serialVersionUID = 1L;

    public BizException(String message) {
        this.message = message;
    }

    public BizException(CharSequence template, Object... params) {
        this.message = StrUtil.format(template, params);
    }

    public BizException(BaseError baseError) {
        super(baseError);
    }

    public BizException(BaseError baseError, Object... params) {
        super(baseError);
        this.message = StrUtil.format(baseError.getMessage(), params);
    }

    public BizException(Integer code, String message) {
        this.message = message;
        this.code = code;
    }

    public BizException(boolean isShow, Integer code, String message) {
        super(isShow, code, message);
    }

    public BizException(boolean isShow, Integer code, String msgTemplate, Object... msgValues) {
        super(isShow, code, msgTemplate, msgValues);
    }

    public BizException(boolean isShow, String msgTemplate, Object... msgValues) {
        super(isShow, msgTemplate, msgValues);
    }

    public BizException(boolean isShow, String message) {
        super(isShow, message);
    }

    public BizException(Throwable cause, BaseError baseError) {
        super(cause, baseError);
    }

    public BizException(Throwable cause, String msg) {
        super(cause, msg);
    }

    @Override
    public String getMessage() {
        return message;
    }

    public Integer getCode() {
        return code;
    }
}
