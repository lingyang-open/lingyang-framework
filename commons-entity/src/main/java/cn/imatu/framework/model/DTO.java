package cn.imatu.framework.model;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson2.JSONObject;

import java.io.Serializable;
import java.util.Collection;

/**
 * @author shenguangyang
 */
public interface DTO extends Serializable {

    default boolean hasFilterValue() {
        JSONObject jsonObject = JSONObject.from(this);
        return !jsonObject.values().stream().allMatch(e -> {
            if (e == null) {
                return true;
            }
            if (e instanceof Collection && ((Collection<?>) e).isEmpty()) {
                return true;
            }
            return StrUtil.isEmpty(e.toString());
        });
    }
}
