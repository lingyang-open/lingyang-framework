package cn.imatu.framework.tool.captcha;

/**
 * @author shenguangyang
 * @date 2022-08-19 11:41
 */
class CaptchaUtilsTest {
    /**
     * 主要提供了两种验证码的生成方式，一种是直接生成到指定目录，另一种是返回二进制的数据，然后进一步处理。
     *
     * @param args
     */
    public static void main(String[] args) throws Exception {
        CaptchaUtils utils = CaptchaUtils.getInstance(6);
        //直接生成图片的方式
        String str = utils.getCode("D:/xxx.jpg");
        System.out.println("验证码1:\t" + str);

        //第二种生成方式，获取到字节码后，再生成图片
        //获取二进制数据
        CaptchaUtils.Captcha code = utils.getCode();
        //获取
        System.out.println("验证码2:\t" + code.getText());
        //将二进制的数据拷贝到成文件
        utils.copyByte2File(code.getData(), "D:/dd.jpg");
    }
}