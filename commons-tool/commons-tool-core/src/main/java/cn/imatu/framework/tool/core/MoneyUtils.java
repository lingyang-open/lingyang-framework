package cn.imatu.framework.tool.core;

import cn.imatu.framework.exception.BizException;
import cn.imatu.framework.tool.core.exception.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

/**
 * 金钱相关工具类
 *
 * @author shenguangyang
 */
public class MoneyUtils {
    /**
     * 将单位为元的金额转换为单位为分
     *
     * @param yuan 单位为元的字符型值
     */
    public static long yuan2Fen(String yuan) {
        if (StringUtils.isEmpty(yuan)) {
            throw new BizException("金额为空");
        }
        return yuan2Fen(new BigDecimal(yuan));
    }

    /**
     * 将单位为元的金额转换为单位为分
     *
     * @param yuan 单位为元的字符型值
     */
    public static long yuan2Fen(BigDecimal yuan) {
        if (Objects.isNull(yuan)) {
            throw new BizException("金额为空");
        }
        long value = 0;
        try {
            BigDecimal var = yuan.multiply(new BigDecimal(100));
            value = Long.parseLong(var.stripTrailingZeros().toPlainString());
        } catch (Exception e) {
            throw new BizException(String.format("非法金额[%s]", yuan));
        }
        Assert.isTrue(value >= 0, String.format("非法金额[%s]", yuan));
        return value;
    }

    /**
     * 将单位为分的金额转换为单位为元
     * @param fen 单位为分的字符型值
     */
    public static String fen2YuanStr(long fen) {
        BigDecimal fenVar = new BigDecimal(fen);
        BigDecimal var1 = fenVar.divide(new BigDecimal(100), RoundingMode.HALF_DOWN);
        return var1.stripTrailingZeros().toPlainString();
    }

    /**
     * 将单位为分的金额转换为单位为元
     * @param fen 单位为分的字符型值
     */
    public static BigDecimal fen2YuanDecimal(long fen) {
        return new BigDecimal(fen).divide(new BigDecimal(100), RoundingMode.HALF_DOWN);
    }
}
