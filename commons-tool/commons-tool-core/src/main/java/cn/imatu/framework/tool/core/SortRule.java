package cn.imatu.framework.tool.core;

/**
 * @author shenguangyang
 */
public enum SortRule {
    /**
     * 升序
     */
    ASE,
    /**
     * 降序
     */
    DESC
}
