package cn.imatu.framework.tool.core;

import cn.imatu.framework.tool.core.exception.UtilException;
import cn.hutool.core.text.AntPathMatcher;
import cn.hutool.core.text.StrFormatter;
import cn.hutool.core.util.URLUtil;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * 字符串工具类
 *
 * @author shenguangyang
 */
public class StringUtils extends org.apache.commons.lang3.StringUtils {
    /**
     * 空字符串
     */
    private static final String NULLSTR = "";

    /**
     * 下划线
     */
    private static final char SEPARATOR = '_';

    /**
     * 对字符加星号处理：除前面几位和后面几位外，其他的字符以星号代替
     *
     * @param content  传入的字符串
     * @param frontNum 保留前面字符的位数
     * @param endNum   保留后面字符的位数
     * @param starNum  指定star的数量
     * @return 带星号的字符串
     */
    public static String str2Star(String content, int frontNum, int endNum, int starNum) {
        if (frontNum >= content.length() || frontNum < 0) {
            return content;
        }
        if (endNum >= content.length() || endNum < 0) {
            return content;
        }
        if (frontNum + endNum >= content.length()) {
            return content;
        }
        StringBuilder starStr = new StringBuilder();
        for (int i = 0; i < starNum; i++) {
            starStr.append("*");
        }
        return content.substring(0, frontNum) + starStr + content.substring(content.length() - endNum);
    }


    /**
     * 拼接url参数
     */
    public static String appendUrlQuery(String url, Map<String, Object> map) {
        if (org.apache.commons.lang3.StringUtils.isEmpty(url) || map == null || map.isEmpty()) {
            return url;
        }
        StringBuilder sb = new StringBuilder(url);
        if (!url.contains("?")) {
            sb.append("?");
        }

        // 是否包含query条件
        boolean isHasCondition = url.contains("=");

        for (String k : map.keySet()) {
            if (k != null && map.get(k) != null) {
                if (isHasCondition) {
                    sb.append("&"); // 包含了查询条件， 那么应当拼接&符号
                } else {
                    isHasCondition = true; // 变更为： 已存在query条件
                }
                sb.append(k).append("=").append(URLUtil.encodeQuery(map.get(k).toString()));
            }
        }
        return sb.toString();
    }

    /**
     * 获取参数不为空值
     *
     * @param value defaultValue 要判断的value
     * @return value 返回值
     */
    public static <T> T nvl(T value, T defaultValue) {
        return value != null ? value : defaultValue;
    }

    /**
     * * 判断一个Collection是否为空， 包含List，Set，Queue
     *
     * @param coll 要判断的Collection
     * @return true：为空 false：非空
     */
    public static boolean isEmpty(Collection<?> coll) {
        return isNull(coll) || coll.isEmpty();
    }

    /**
     * * 判断一个Collection是否非空，包含List，Set，Queue
     *
     * @param coll 要判断的Collection
     * @return true：非空 false：空
     */
    public static boolean isNotEmpty(Collection<?> coll) {
        return !isEmpty(coll);
    }

    /**
     * * 判断一个对象数组是否为空
     *
     * @param objects 要判断的对象数组
     *                * @return true：为空 false：非空
     */
    public static boolean isEmpty(Object[] objects) {
        return isNull(objects) || (objects.length == 0);
    }

    /**
     * * 判断一个对象数组是否非空
     *
     * @param objects 要判断的对象数组
     * @return true：非空 false：空
     */
    public static boolean isNotEmpty(Object[] objects) {
        return !isEmpty(objects);
    }

    /**
     * * 判断一个Map是否为空
     *
     * @param map 要判断的Map
     * @return true：为空 false：非空
     */
    public static boolean isEmpty(Map<?, ?> map) {
        return isNull(map) || map.isEmpty();
    }

    /**
     * * 判断一个Map是否为空
     *
     * @param map 要判断的Map
     * @return true：非空 false：空
     */
    public static boolean isNotEmpty(Map<?, ?> map) {
        return !isEmpty(map);
    }

    /**
     * * 判断一个字符串是否为空串
     *
     * @param str String
     * @return true：为空 false：非空
     */
    public static boolean isEmpty(String str) {
        return isNull(str) || NULLSTR.equals(str.trim());
    }

    /**
     * * 判断一个字符串是否为非空串
     *
     * @param str String
     * @return true：非空串 false：空串
     */
    public static boolean isNotEmpty(String str) {
        return !isEmpty(str);
    }

    /**
     * * 判断一个对象是否为空
     *
     * @param object Object
     * @return true：为空 false：非空
     */
    public static boolean isNull(Object object) {
        return object == null;
    }

    /**
     * * 判断一个对象是否非空
     *
     * @param object Object
     * @return true：非空 false：空
     */
    public static boolean isNotNull(Object object) {
        return !isNull(object);
    }

    /**
     * * 判断一个对象是否是数组类型（Java基本型别的数组）
     *
     * @param object 对象
     * @return true：是数组 false：不是数组
     */
    public static boolean isArray(Object object) {
        return isNotNull(object) && object.getClass().isArray();
    }

    /**
     * 是否 http 或 https连接
     **/
    public static boolean isAvailableUrl(String url) {
        if (isEmpty(url)) {
            return false;
        }
        return url.startsWith("http://") || url.startsWith("https://");
    }

    /**
     * 去空格
     */
    public static String trim(String str) {
        return (str == null ? "" : str.trim());
    }

    /**
     * 截取字符串
     *
     * @param str   字符串
     * @param start 开始
     * @return 结果
     */
    public static String substring(final String str, int start) {
        if (str == null) {
            return NULLSTR;
        }

        if (start < 0) {
            start = str.length() + start;
        }

        if (start < 0) {
            start = 0;
        }
        if (start > str.length()) {
            return NULLSTR;
        }

        return str.substring(start);
    }

    /**
     * 截取字符串
     *
     * @param str   字符串
     * @param start 开始
     * @param end   结束
     * @return 结果
     */
    public static String substring(final String str, int start, int end) {
        if (str == null) {
            return NULLSTR;
        }

        if (end < 0) {
            end = str.length() + end;
        }
        if (start < 0) {
            start = str.length() + start;
        }

        if (end > str.length()) {
            end = str.length();
        }

        if (start > end) {
            return NULLSTR;
        }

        if (start < 0) {
            start = 0;
        }
        if (end < 0) {
            end = 0;
        }

        return str.substring(start, end);
    }

    /**
     * 判断是否为空，并且不是空白字符
     *
     * @param str 要判断的value
     * @return 结果
     */
    public static boolean hasText(String str) {
        return (str != null && !str.isEmpty() && containsText(str));
    }

    private static boolean containsText(CharSequence str) {
        int strLen = str.length();
        for (int i = 0; i < strLen; i++) {
            if (!Character.isWhitespace(str.charAt(i))) {
                return true;
            }
        }
        return false;
    }

    /**
     * 格式化文本, {} 表示占位符<br>
     * 此方法只是简单将占位符 {} 按照顺序替换为参数<br>
     * 如果想输出 {} 使用 \\转义 { 即可，如果想输出 {} 之前的 \ 使用双转义符 \\\\ 即可<br>
     * 例：<br>
     * 通常使用：<code>format("this is {} for {}", "a", "b")</code> -> this is a for b<br>
     * 转义{}： <code>format("this is \\{} for {}", "a", "b")</code> -> this is \{} for a<br>
     * 转义\： <code>format("this is \\\\{} for {}", "a", "b")</code> -> this is \a for b<br>
     *
     * @param template 文本模板，被替换的部分用 {} 表示
     * @param params   参数值
     * @return 格式化后的文本
     */
    public static String format(String template, Object... params) {
        if (isEmpty(params) || isEmpty(template)) {
            return template;
        }
        return StrFormatter.format(template, params);
    }

    /**
     * 驼峰转下划线命名 humpToUnderline
     */
    public static String humpToUnderline(String str) {
        String regex = "([A-Z])";
        Matcher matcher = Pattern.compile(regex).matcher(str);
        while (matcher.find()) {
            String target = matcher.group();
            str = str.replaceAll(target, "_" + target.toLowerCase());
        }
        return str;
    }

    /**
     * Object.toString()
     *
     * @param obj the obj
     * @return string string
     */
    public static String toString(Object obj) {
        if (obj == null) {
            return "null";
        }
        if (obj.getClass().isPrimitive()) {
            return String.valueOf(obj);
        }
        if (obj instanceof String) {
            return (String) obj;
        }
        if (obj instanceof Number || obj instanceof Character || obj instanceof Boolean) {
            return String.valueOf(obj);
        }
        if (obj instanceof Date) {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").format(obj);
        }
        if (obj instanceof Collection) {
            StringBuilder sb = new StringBuilder();
            sb.append("[");
            if (!((Collection) obj).isEmpty()) {
                for (Object o : (Collection) obj) {
                    sb.append(toString(o)).append(",");
                }
                sb.deleteCharAt(sb.length() - 1);
            }
            sb.append("]");
            return sb.toString();
        }
        if (obj instanceof Map) {
            StringBuilder sb = new StringBuilder();
            sb.append("{");
            if (!((Map) obj).isEmpty()) {
                for (Object k : ((Map) obj).keySet()) {
                    Object v = ((Map) obj).get(k);
                    sb.append(toString(k)).append("->").append(toString(v)).append(",");
                }
                sb.deleteCharAt(sb.length() - 1);
            }
            sb.append("}");
            return sb.toString();
        }
        StringBuilder sb = new StringBuilder();
        Field[] fields = obj.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            sb.append(field.getName());
            sb.append("=");
            try {
                Object f = field.get(obj);
                if (f.getClass() == obj.getClass()) {
                    sb.append(f);
                } else {
                    sb.append(toString(f));
                }
            } catch (Exception e) {
            }
            sb.append(";");
        }
        return sb.toString();
    }

    /**
     * 是否包含字符串
     *
     * @param str  验证字符串
     * @param strs 字符串组
     * @return 包含返回true
     */
    public static boolean inStringIgnoreCase(String str, String... strs) {
        if (str != null && strs != null) {
            for (String s : strs) {
                if (str.equalsIgnoreCase(trim(s))) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * <p>
     * 将下划线大写方式命名的字符串转换为驼峰式。如果转换前的下划线大写方式命名的字符串为空，则返回空字符串。 例如：HELLO_WORLD->HelloWorld
     * </p>
     *
     * @param name 转换前的下划线大写方式命名的字符串
     * @return 转换后的驼峰式命名的字符串
     */
    public static String convertToCamelCase(String name) {
        StringBuilder result = new StringBuilder();
        // 快速检查
        if (name == null || name.isEmpty()) {
            // 没必要转换
            return "";
        } else if (!name.contains("_")) {
            // 不含下划线，仅将首字母大写
            return name.substring(0, 1).toUpperCase() + name.substring(1);
        }
        // 用下划线将原始字符串分割
        String[] camels = name.split("_");
        for (String camel : camels) {
            // 跳过原始字符串中开头、结尾的下换线或双重下划线
            if (camel.isEmpty()) {
                continue;
            }
            // 首字母大写
            result.append(camel.substring(0, 1).toUpperCase());
            result.append(camel.substring(1).toLowerCase());
        }
        return result.toString();
    }

    /**
     * 驼峰式命名法 例如：user_name->userName
     */
    public static String underlineToHump(String str) {
        String regex = "_(.)";
        Matcher matcher = Pattern.compile(regex).matcher(str);
        while (matcher.find()) {
            String target = matcher.group(1);
            str = str.replaceAll("_" + target, target.toUpperCase());
        }
        return str;
    }

    /**
     * 将字符串转为16禁制
     *
     * @param str 字符串
     * @return 返回16进制字符串
     */
    public static String toHex(String str) {
        StringBuilder stringBuilder = new StringBuilder();
        char[] charArray = str.toCharArray();
        for (char c : charArray) {
            String charToHex = Integer.toHexString(c);
            stringBuilder.append(charToHex).append(" ");
        }
        stringBuilder.delete(stringBuilder.lastIndexOf(" "), stringBuilder.length());
        return stringBuilder.toString();
    }

    /**
     * 查找指定字符串是否匹配指定字符串列表中的任意一个字符串
     *
     * @param str  指定字符串
     * @param strs 需要检查的字符串数组
     * @return 是否匹配
     */
    public static boolean matches(String str, List<String> strs) {
        if (isEmpty(str) || isEmpty(strs)) {
            return false;
        }
        for (String pattern : strs) {
            if (isMatch(pattern, str)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 查找指定字符串是否匹配指定字符串列表中的任意一个字符串
     *
     * @param str        指定字符串
     * @param strs       需要检查的字符串数组
     * @param ignoreCase 是否忽略大小写
     * @return 是否匹配
     */
    public static boolean matches(String str, List<String> strs, boolean ignoreCase) {
        if (isEmpty(str) || isEmpty(strs)) {
            return false;
        }
        str = str.toLowerCase(Locale.ROOT);
        for (String pattern : strs) {
            pattern = pattern.toLowerCase(Locale.ROOT);
            if (isMatch(pattern, str)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 判断url是否与规则配置:
     * ? 表示单个字符;
     * * 表示一层路径内的任意字符串，不可跨层级;
     * ** 表示任意层路径;
     *
     * @param pattern 匹配规则
     * @param url     需要匹配的url
     * @return 是否匹配
     */
    public static boolean isMatch(String pattern, String url) {
        AntPathMatcher matcher = new AntPathMatcher();
        return matcher.match(pattern, url);
    }

    @SuppressWarnings("unchecked")
    public static <T> T cast(Object obj) {
        return (T) obj;
    }

    public static <T> List<T> toList(String sourceStr, String separate, Class<T> targetType) {
        return toList(sourceStr, separate, targetType, null);
    }

    /**
     * 只保留中文, 其余字符都会被去掉
     *
     * @param str 字符串
     * @return 返回字符串中的中文
     */
    @NotNull
    public static String retainChinese(String str) {
        return str.replaceAll("[^\u4E00-\u9FA5]", "");
    }

    /**
     * 保留数字 + 字母, 其他字符会被去掉
     *
     * @param str 字符串
     * @return 返回目标字符串
     */
    public static String retainNumAndAlphabet(String str) {
        String string = "123abcABC!@#$%^&*()-_+={}[];:',<.>/?";
        return string.replaceAll("[^0-9a-zA-Z]", "");
    }

    /**
     * 字符串转为list
     *
     * @param sourceStr 源字符串
     * @param separate  字符串中的分隔符
     */
    @SuppressWarnings("unchecked")
    public static <T> List<T> toList(String sourceStr, String separate, Class<T> targetType, Consumer<T> consumer) {
        List<T> result = new ArrayList<>();
        if (isNotEmpty(sourceStr)) {
            String[] split = sourceStr.split(separate);
            for (String str : split) {
                T t = null;
                if (targetType.isAssignableFrom(String.class)) {
                    t = ((T) str);
                } else if (targetType.isAssignableFrom(Long.class)) {
                    t = (T) Long.valueOf(str);
                } else if (targetType.isAssignableFrom(Integer.class)) {
                    t = (T) Integer.valueOf(str);
                } else if (targetType.isAssignableFrom(Double.class)) {
                    t = (T) Double.valueOf(str);
                } else if (targetType.isAssignableFrom(Short.class)) {
                    t = (T) Short.valueOf(str);
                } else if (targetType.isAssignableFrom(Float.class)) {
                    t = (T) Float.valueOf(str);
                } else {
                    throw new UtilException(targetType.getName() + " not supported");
                }
                result.add(t);
                if (consumer != null) {
                    consumer.accept(t);
                }
            }
        }
        return result;
    }

    /**
     * list 转字符串
     *
     * @param sourceList list数据
     * @param separate   分隔符号
     * @return 以 separate 分割的字符串
     */
    public static <T> String toStr(List<T> sourceList, String separate) {
        if (CollectionUtils.isEmpty(sourceList)) {
            return "";
        }
        return sourceList.stream().map(String::valueOf).collect(Collectors.joining(separate));
    }

    /**
     * 去除两符号间内容
     *
     * @param context 文本
     * @param left    左侧符号
     * @param right   右侧符号
     * @return 去除两符号间内容
     */
    public static String clearBracket(String context, char left, char right) {
        int head = context.indexOf(left);
        if (head == -1) {
            return context;
        } else {
            int next = head + 1;
            int count = 1;
            do {
                if (context.charAt(next) == left) {
                    count++;
                } else if (context.charAt(next) == right) {
                    count--;
                }
                next++;
                if (count == 0) {
                    String temp = context.substring(head, next);
                    context = context.replace(temp, "");
                    head = context.indexOf(left);
                    next = head + 1;
                    count = 1;
                }
            } while (head != -1);
        }
        return context;
    }

    /**
     * 优先选择非空字符串, 遍历集合, 选择第一个非空字符串
     * @param strList 字符串集合
     * @return 第一个非空字符串, 如果没有找到就返回为空
     */
    public static <T extends CharSequence> T priorityFirstNotEmpty(T... strList) {
        return Arrays.stream(strList).filter(StringUtils::isNotEmpty).findFirst().orElse(null);
    }
}