package cn.imatu.framework.tool.core.compress;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * Description: java对文本字符串的压缩与解压
 * 这段代码的压缩效率还是比较高的，对于大段文本，能压缩到原文本的10%左右。但是有个问题时，
 * 这个压缩会出现特殊字符，如果用web service调用或者其他封装成xml格式数据时候，会报异常。
 * 那么，我们可以，将压缩后的文本通过base64进行编码，解压缩之前先进行base64解码，这样就
 * 可以避免特殊字符问题了。如下面代码
 *
 * @author shenguangyang
 */
public class GzipUtils {
    /**
     * @param str 文本
     * @return 压缩
     */
    public static String compressText(String str) throws IOException {
        if (str == null || str.length() == 0) {
            return str;
        }
        String compress = "";
        try (ByteArrayOutputStream out = new ByteArrayOutputStream();
             GZIPOutputStream gzip = new GZIPOutputStream(out)) {
            gzip.write(str.getBytes());

            byte[] compressed = out.toByteArray();
            compress = Base64.getEncoder().encodeToString(compressed);
        }
        return compress;
    }

    /**
     * @param str 待解压文本
     * @return 解压缩
     */
    public static String uncompressText(String str) throws IOException {
        if (str == null || str.length() == 0) {
            return str;
        }
        String uncompress = "";
        byte[] compressed = Base64.getDecoder().decode(str);
        try (ByteArrayOutputStream out = new ByteArrayOutputStream();
             ByteArrayInputStream in = new ByteArrayInputStream(compressed);
             GZIPInputStream gzip = new GZIPInputStream(in);) {

            byte[] buffer = new byte[1024];
            int offset = -1;
            while ((offset = gzip.read(buffer)) != -1) {
                out.write(buffer, 0, offset);
            }
            uncompress = out.toString();
        }
        return uncompress;
    }
}
