package cn.imatu.framework.tool.core.compress;

import cn.hutool.core.io.IoUtil;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author shenguangyang
 */
public class ZipUtils {
    /**
     * 将每个文件的的字节数组添加到压缩包中
     *
     * @param inputStreamMap  输入流map
     * @param zipOutputStream zip输出流
     */
    public static void zipFile(Map<String, byte[]> inputStreamMap, ZipOutputStream zipOutputStream) {
        inputStreamMap.forEach((k, v) -> {
            try (InputStream is = new ByteArrayInputStream(v)) {
                ZipEntry zipEntry = new ZipEntry(k);
                try {
                    zipOutputStream.putNextEntry(zipEntry);
                } catch (IOException e) {
                    throw e;
                }
                IoUtil.copy(is, zipOutputStream);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }
}
