package cn.imatu.framework.tool.core.exception;

import cn.imatu.framework.exception.BaseError;
import cn.imatu.framework.exception.BaseException;

/**
 * 工具类异常
 *
 * @author shenguangyang
 */
public class UtilException extends BaseException {
    private static final long serialVersionUID = 8247610319171014183L;

    public UtilException() {
    }

    public UtilException(Integer code, String message) {
        super(code, message);
    }

    public UtilException(String message) {
        super(message);
    }

    public UtilException(BaseError baseError) {
        super(baseError);
    }
}
