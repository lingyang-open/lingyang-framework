package cn.imatu.framework.tool.core.jar;

/**
 * 通过MavenAssemblyPlugin进行打包
 * <p>
 * 打包之后的jar包内的目录结构 (所有的第三方库都打包在了一起)
 * - com/xxx/Xxx.class
 * - 资源文件
 *
 * @author shenguangyang
 */
public class JarBuildByMavenAssemblyPlugin extends JarBuildWay {
    public JarBuildByMavenAssemblyPlugin(CopyResourcesInfo copyResourcesInfo) {
        super(copyResourcesInfo);
    }

    @Override
    protected void doCopyResourcesToLocal() {

    }
}
