package cn.imatu.framework.tool.core.jar;

import java.io.File;
import java.io.IOException;

/**
 * @author shenguangyang
 */
public class JarResourcesCopyConstant {
    /**
     * 库缓存的路径
     */
    private static final String LIB_CACHE_PATH_PRE = ".lib" + File.separator + "cache";
    public static String LIB_CACHE_PATH = "";

    static {
        try {
            File cacheDirectory = createCacheDirectory();
            LIB_CACHE_PATH = cacheDirectory.getPath();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static File createCacheDirectory() throws IOException {
        String userHomeDir = System.getProperty("user.home");
        if (!userHomeDir.endsWith(File.separator)) {
            userHomeDir = userHomeDir + File.separator;
        }
        File generatedDir = new File(userHomeDir + JarResourcesCopyConstant.LIB_CACHE_PATH_PRE);
        if (generatedDir.exists()) {
            return generatedDir;
        }
        if (!generatedDir.mkdirs())
            throw new IOException("Failed to create cache directory " + generatedDir.getPath());

        return generatedDir;
    }
}
