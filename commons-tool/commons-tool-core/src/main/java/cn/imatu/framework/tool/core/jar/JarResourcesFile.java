package cn.imatu.framework.tool.core.jar;

import java.io.InputStream;

/**
 * jar包中文件信息
 *
 * @author shenguangyang
 */
public class JarResourcesFile {
    private InputStream inputStream;

    /**
     * 所属jar包名称
     */
    private String jarName;
    /**
     * 文件名称
     * config.yam xx.txt
     */
    private String classPath;
    /**
     * 资源路径
     * 格式为 init/config.yaml (linux / mac) or init/config.yaml (window)
     */
    private String path;

    public JarResourcesFile(InputStream inputStream, String classPath) {
        this.inputStream = inputStream;
        this.classPath = classPath;
    }

    public JarResourcesFile(String classPath, String jarName, String path) {
        this.jarName = jarName;
        this.path = path;
        this.classPath = classPath;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public String getClassPath() {
        return classPath;
    }

    public void setClassPath(String classPath) {
        this.classPath = classPath;
    }

    public String getJarName() {
        return jarName;
    }

    public void setJarName(String jarName) {
        this.jarName = jarName;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
