用于将某个包下的动态库拷贝到主机上

使用方式:

1. 一定要main最开始就注册相关的动态库所在包

```java
@SpringBootApplication
public class Test {
    public static void main(String[] args) {
        JarPackageOfRegister.register(new JarPackageInfo(EPlatform.Windows, new HikWin64PackageMark(), "component-lib-hik-win64-1.0.0.jar"));
        JarPackageOfRegister.register(new JarPackageInfo(EPlatform.Linux, new HikLinux64PackageMark(), "component-lib-hik-linux64-1.0.0.jar"));
        JarPackageOfRegister.register(new JarPackageInfo(EPlatform.Windows, new MyLibWin64PackageMark(), "component-lib-test.jar"));
        JarPackageOfRegister.register(new JarPackageInfo(EPlatform.Linux, new MyLibLinux64PackageMark(), "component-lib-test.jar"));
        
        if (OSInfo.isLinux()) {
            String hikLinux64libRootPath = JarPackageOfRegister.getLibRootPath(HikLinux64PackageMark.class);
            System.out.println("hik linux64 lib path: " + hikLinux64libRootPath);

            String myLinux64libRootPath = JarPackageOfRegister.getLibRootPath(MyLibLinux64PackageMark.class);
            System.out.println("my linux64 lib path: " + myLinux64libRootPath);
        } else {
            String hikWin64libRootPath = JarPackageOfRegister.getLibRootPath(HikWin64PackageMark.class);
            System.out.println("hik win64 lib path: " + hikWin64libRootPath);

            String myWin64libRootPath = JarPackageOfRegister.getLibRootPath(MyLibWin64PackageMark.class);
            System.out.println("my win64 lib path: " + myWin64libRootPath);
        }
        SpringApplication.run(Test.class, args);
    }
}
```

有了动态库跟路径, 你就可以通过根路径拼接具体的动态库路径了

pom文件如下:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <parent>
        <artifactId>component-lib</artifactId>
        <groupId>com.concise</groupId>
        <version>1.0.0</version>
    </parent>
    <modelVersion>4.0.0</modelVersion>

    <artifactId>component-lib-test</artifactId>

    <properties>
        <maven.compiler.source>8</maven.compiler.source>
        <maven.compiler.target>8</maven.compiler.target>
        <maven-compiler-plugin.version>3.8.1</maven-compiler-plugin.version>
        <spring-boot.version>2.6.8</spring-boot.version>
    </properties>

    <dependencies>
        <dependency>
            <groupId>com.concise</groupId>
            <artifactId>component-web</artifactId>
        </dependency>
        <dependency>
            <groupId>com.concise</groupId>
            <artifactId>component-lib-common</artifactId>
        </dependency>

        <dependency>
            <groupId>com.concise</groupId>
            <artifactId>component-lib-hik-linux64</artifactId>
        </dependency>

        <dependency>
            <groupId>com.concise</groupId>
            <artifactId>component-lib-hik-win64</artifactId>
        </dependency>
    </dependencies>

    <build>
        <finalName>component-lib-test</finalName>
        <resources>
            <resource>
                <directory>src/main/java</directory>
                <targetPath>${project.build.directory}/classes</targetPath>
                <includes>
                    <include>**/*.*</include>
                    <include>**/*.*</include>
                    <include>**/*.*</include>
                </includes>
                <excludes>
                    <exclude>**/*.java</exclude>
                </excludes>
                <filtering>false</filtering>
            </resource>

            <resource>
                <directory>src/main/resources</directory>
                <includes>
                    <include>**/*.*</include>
                </includes>
            </resource>
        </resources>

        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>${maven-compiler-plugin.version}</version>
                <configuration>
                    <encoding>utf-8</encoding>
                    <source>${java.version}</source>
                    <target>${java.version}</target>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <version>${spring-boot.version}</version>
                <configuration>
                    <fork>true</fork>
                    <!-- 引入本地jar之后, 加入如下这行, maven才会打包进jar-->
                    <includeSystemScope>true</includeSystemScope>
                    <addResources>true</addResources>
                    <mainClass>org.test.Test</mainClass>
                </configuration>
                <executions>
                    <execution>
                        <goals>
                            <goal>repackage</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

</project>
```

动态库所在模块一定要添加

```xml
<resources>
    <resource>
        <directory>src/main/java</directory>
        <targetPath>${project.build.directory}/classes</targetPath>
        <includes>
            <include>**/*.*</include>
            <include>**/*.*</include>
            <include>**/*.*</include>
        </includes>
        <excludes>
            <exclude>**/*.java</exclude>
        </excludes>
        <filtering>false</filtering>
    </resource>

    <resource>
        <directory>src/main/resources</directory>
        <includes>
            <include>**/*.*</include>
        </includes>
    </resource>
</resources>
```

