package cn.imatu.framework.tool.core.compress;

import java.io.IOException;

/**
 * @author shenguangyang
 * @date 2022-08-17 10:54
 */
class GzipUtilsTest {
    public static void main(String[] args) throws IOException {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 10000; i++) {
            sb.append("代码笔记：www.note52.com");
        }
        String str = sb.toString();
        System.out.println("原长度：" + str.length());

        System.out.println("压缩后长度：" + GzipUtils.compressText(str).length());
        String compress = GzipUtils.compressText(str);
        System.out.println("压缩后内容：" + compress);
        System.out.println("解压后内容：" + GzipUtils.uncompressText(compress));
    }
}