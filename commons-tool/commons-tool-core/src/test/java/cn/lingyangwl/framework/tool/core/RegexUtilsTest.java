package cn.imatu.framework.tool.core;

import org.junit.jupiter.api.Test;

/**
 * @author shenguangyang
 * @date 2022-08-18 20:03
 */
class RegexUtilsTest {

    @Test
    void getRegex() {
        String test = RegexUtils.getRegex("test");
        System.out.println(test);
        boolean b1 = RegexUtils.find("BOOT-INF/classes/test.md", test);
        System.out.println(b1);
    }

    @Test
    public void matches() {
        String excludePath = RegexUtils.getRegex("**/test/**");
        System.out.println(RegexUtils.matches("BOOT-INF/classes/test/test.md", excludePath));
    }
}