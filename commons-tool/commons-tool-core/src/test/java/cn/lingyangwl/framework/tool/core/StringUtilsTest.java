package cn.imatu.framework.tool.core;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * @author shenguangyang
 */
class StringUtilsTest {

    @Test
    void retainChinese() {
        System.out.println(StringUtils.retainChinese("牛啊@dsdfsd123123#%&&&哈哈就好……&*12312"));
    }

    @Test
    void clearBracket() {
        System.out.println(StringUtils.clearBracket("varchar(test)", '(', ')'));
    }

    @Test
    void appendUrlQuery() {
        Map<String, Object> data = new HashMap<>();
        data.put("d1", "123123");
        data.put("d2", "???4234");
        data.put("d3", "erwer");
        data.put("d4", "fsdfsdf");
        System.out.println(StringUtils.appendUrlQuery("http://127.0.0.1:8080/", data));
    }
}