package cn.imatu.framework.tool.core;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * @author shenguangyang
 */
class UrlUtilsTest {

    @Test
    void getUrl() {
        Map<String, String> urlParams = new HashMap<>();
        urlParams.put("t1", "t1");
        urlParams.put("t2", "t1");
        urlParams.put("t3", "t1");
        System.out.println(UrlUtils.getUrl("http://127.0.0.1:8080/api", "test/now/", urlParams));
    }

    @Test
    void removeRepeatSlashOfUrl() {
        System.out.println(UrlUtils.removeRepeatSlashOfUrl("/////test/1/t2"));
    }
}