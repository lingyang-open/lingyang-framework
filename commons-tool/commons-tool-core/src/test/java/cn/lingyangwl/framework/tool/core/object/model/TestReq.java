package cn.imatu.framework.tool.core.object.model;

import lombok.Getter;
import lombok.Setter;

/**
 * @author shenguangyang
 */
@Getter
@Setter
public class TestReq extends BaseTestReq {
    private String name;
}
