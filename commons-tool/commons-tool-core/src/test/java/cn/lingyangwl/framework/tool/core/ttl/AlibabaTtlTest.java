package cn.imatu.framework.tool.core.ttl;

import cn.imatu.framework.tool.core.thread.AsyncTaskExecutor;
import com.alibaba.fastjson2.JSON;
import com.alibaba.ttl.TransmittableThreadLocal;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/**
 * @author shenguangyang
 * @date 2022-09-25 15:22
 */
public class AlibabaTtlTest {
    public static void main(String[] args) {
        TransmittableThreadLocal<MyData> ttlData = new TransmittableThreadLocal<>();
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(10);
        ttlData.set(new MyData());

        AsyncTaskExecutor.init(executor)
                .addTask(ttlData, ttl -> ttl.get().setAge(1))
                .addTask(ttlData, ttl -> ttl.get().setName("haha"))
                .execute();
        System.out.println(JSON.toJSONString(ttlData.get()));
    }

    public static class MyData {
        private String name;
        private Integer age;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getAge() {
            return age;
        }

        public void setAge(Integer age) {
            this.age = age;
        }
    }
}
