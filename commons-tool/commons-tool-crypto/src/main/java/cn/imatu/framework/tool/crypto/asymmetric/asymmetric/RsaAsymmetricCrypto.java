package cn.imatu.framework.tool.crypto.asymmetric;

import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.RSA;

/**
 * @author shenguangyang
 */
public class RsaAsymmetricCrypto extends BaseAsymmetricCrypto {

    public RsaAsymmetricCrypto() {

    }

    @Override
    public String decrypt(String data, KeyType keyType, String key) {
        RSA rsa = new RSA(keyType == KeyType.PrivateKey ? key : null, keyType == KeyType.PublicKey ? key : null);
        return rsa.decryptStr(data, keyType);
    }

    @Override
    public String encrypt(String data, KeyType keyType, String key) {
        RSA rsa = new RSA(keyType == KeyType.PrivateKey ? key : null, keyType == KeyType.PublicKey ? key : null);
        return rsa.encryptBase64(data, keyType);
    }

    @Override
    public CryptoKeyDto getKey() {
        RSA rsa = new RSA();
        return CryptoKeyDto.builder()
                .privateKey(rsa.getPrivateKeyBase64())
                .publicKey(rsa.getPublicKeyBase64())
                .build();
    }

    @Override
    public AsymmetricCryptoType type() {
        return AsymmetricCryptoType.RSA;
    }
}
