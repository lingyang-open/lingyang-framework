package cn.imatu.framework.tool.crypto;

import cn.hutool.crypto.SecureUtil;
import cn.imatu.framework.tool.crypto.asymmetric.RsaKeyType;
import cn.imatu.framework.tool.crypto.asymmetric.RsaUtils;
import org.apache.commons.lang3.time.StopWatch;

import java.util.concurrent.TimeUnit;

/**
 * @author shenguangyang
 */
class RsaUtilsTest {
    public static void main(String[] args) throws Exception {
        RsaUtils.KEY_SIZE = 2048;
        long temp = System.currentTimeMillis();
        //生成公钥和私钥
        RsaUtils.genKeyPair();
        //加密字符串
        System.out.println("公钥:" + RsaUtils.getKey(RsaKeyType.PUBLIC));
        System.out.println("私钥:" + RsaUtils.getKey(RsaKeyType.PRIVATE));
        System.out.println("生成密钥消耗时间:" + (System.currentTimeMillis() - temp) / 1000.0 + "秒");
        StopWatch stopWatch1 = StopWatch.createStarted();
        int count = 5;
        for (int i = 0; i < count; i++) {
            StopWatch stopWatch = StopWatch.createStarted();
            System.out.println("==============================================" + i);
            //客户id + 授权时间 + 所用模块
            String message = "4028138151b3cf300151b419df0904028138151b3cf300151b419df0900074028138151b3cf300151b419df0900074028138151b3cf300151b419df090007007" + "2015-12-17 11:30:22" + "A01,A02";
            System.out.println("原文:" + message);
            message = SecureUtil.sha256(message);

            System.out.println("原文摘要 : " + message);
            //通过原文，和公钥加密。
            String messageEn = RsaUtils.encrypt(message, RsaUtils.getKey(RsaKeyType.PUBLIC));
            System.out.println("密文:" + messageEn);
            System.out.println("加密消耗时间:" + stopWatch.getTime(TimeUnit.MILLISECONDS) + "ms");
            stopWatch.reset();

            //通过密文，和私钥解密。
            String messageDe = RsaUtils.decrypt(messageEn, RsaUtils.getKey(RsaKeyType.PRIVATE));
            System.out.println("解密:" + messageDe);
            System.out.println("解密消耗时间:" + stopWatch.getTime(TimeUnit.MILLISECONDS) + "ms");
        }
        long useTime = stopWatch1.getTime(TimeUnit.MILLISECONDS);
        System.out.println("总耗时: " + useTime + " ms" + ", 平均每次耗时" + (useTime /count) + " ms");
//
//        String message1 = "402813818151b3cf300151b419df090007007" + "2015-12-17 11:30:22" + "A01,A02";
//        System.out.println("message1: " + message1);
//        System.out.println("原文MD5加密后 : " + (message1 = MD5Utils.md5(message1)));
//        String messageEn1 = RsaUtils.encrypt(message1, RsaUtils.getKey(RsaKeyType.PUBLIC));
//        System.out.println("密文:" + messageEn1);
//        String messageDe1 = RsaUtils.decrypt(messageEn1, RsaUtils.getKey(RsaKeyType.PRIVATE));
//        System.out.println("解密:" + messageDe1);
    }
}