package cn.imatu.framework.poi.excel;

import cn.hutool.core.util.StrUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

/**
 * @author shenguangyang
 */
@Getter
@AllArgsConstructor
public enum ExcelType {
    XLS("xls", "application/vnd.ms-excel"),
    XLSX("xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"),
    ;
    private final String name;
    private final String contentType;

    public static ExcelType ofByName(String name) {
        return Arrays.stream(values())
                .filter(e -> StrUtil.equalsIgnoreCase(e.name, name)).findFirst().orElse(null);
    }

    public static ExcelType ofByContentType(String contentType) {
        return Arrays.stream(values())
                .filter(e -> StrUtil.equalsIgnoreCase(e.contentType, contentType)).findFirst().orElse(null);
    }
}
