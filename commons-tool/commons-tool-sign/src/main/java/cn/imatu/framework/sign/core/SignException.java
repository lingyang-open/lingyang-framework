package cn.imatu.framework.sign.core;

import cn.imatu.framework.exception.BaseError;
import cn.imatu.framework.exception.BizException;

/**
 * @author shenguangyang
 */
public class SignException extends BizException {
    public SignException(String message) {
        super(message);
    }

    public SignException(CharSequence template, Object... params) {
        super(template, params);
    }

    public SignException(BaseError baseError) {
        super(baseError);
    }

    public SignException(Integer code, String message) {
        super(code, message);
    }

    public SignException(boolean isShow, Integer code, String message) {
        super(isShow, code, message);
    }

    public SignException(boolean isShow, Integer code, String msgTemplate, Object... msgValues) {
        super(isShow, code, msgTemplate, msgValues);
    }

    public SignException(boolean isShow, String msgTemplate, Object... msgValues) {
        super(isShow, msgTemplate, msgValues);
    }

    public SignException(boolean isShow, String message) {
        super(isShow, message);
    }
}
