package cn.imatu.framework.sign.core;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

/**
 * @author shenguangyang
 */
@Getter
@AllArgsConstructor
public enum SignType {
    MD5("md5", true),
    SHA256("sha256", true),
    RSA2("rsa2", false),
    ;
    private final String type;
    private final boolean isHashAlgo;

    public static SignType ofByType(String type) {
        return Arrays.stream(SignType.values()).filter(e -> e.getType().equalsIgnoreCase(type))
                .findFirst().orElseThrow(() -> new SignException("不支持的signType: {}", type));
    }
}
