package cn.imatu.framework.sign.core;

import cn.imatu.framework.tool.core.MD5Utils;

/**
 * @author shenguangyang
 */
public class Md5AlgoProcess extends HashAlgoProcess {
    @Override
    public String process(String req) throws Exception {
        return MD5Utils.md5(req);
    }

    @Override
    public SignType getSignTYpe() {
        return SignType.MD5;
    }
}
