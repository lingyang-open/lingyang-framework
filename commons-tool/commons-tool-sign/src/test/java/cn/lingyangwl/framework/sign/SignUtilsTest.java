package cn.imatu.framework.sign;

import cn.imatu.framework.sign.core.RsaKeyType;
import cn.imatu.framework.sign.core.RsaUtils;
import cn.imatu.framework.sign.core.SignPayload;
import cn.imatu.framework.sign.core.SignType;
import cn.imatu.framework.sign.core.SignUtils;
import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import lombok.Getter;
import lombok.Setter;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * @author shenguangyang
 */
class SignUtilsTest {

    public static void main(String[] args) throws Exception {
        SignUtilsTest test = new SignUtilsTest();
        test.testSign02();
    }

    @Test
    public void testHashSign() throws Exception {
        SignUtils.SIGN_TIMEOUT_SECONDS = 100;
        for (int i = 0; i < 1; i++) {
            String secretId = "123123131231";
            String secretKey = "sdfsdfsdfadsfasfdaefwef";

            TestReq testReq = new TestReq();
            testReq.setName("2");
            JSONObject jsonObject = SignUtils.genSign(SignType.SHA256, secretId, secretKey, testReq);

            TestReqCopy newTestReq = new TestReqCopy();
            newTestReq.setSecretId(testReq.getSecretId());
            newTestReq.setDate(testReq.getDate());
            newTestReq.setDate1(testReq.getDate1());
            newTestReq.setNonce(testReq.getNonce());
            newTestReq.setTimeStamp(testReq.getTimeStamp());
            newTestReq.setSignType(testReq.getSignType());
            newTestReq.setSign(testReq.getSign());
            newTestReq.setName(testReq.getName());

            // 不需要进行签名的字段
            newTestReq.setName03("123123132");
            newTestReq.getTest01().setK5("12313132");
            SignUtils.checkSign(newTestReq, secretKey, "name03", "test01.k5");

            // TODO 修改参数会导致签名校验失败
//        System.out.println("修改数据");
//        testReq.setName("1");
//        SignUtils.checkSign(testReq, secretKey);

//            // TODO 已经被排除的签名字段, 修改也不会导致签名验证失败
//            testReq.setName2("1");
//            testReq.getTest01().setK2("123123123123");
//            SignUtils.checkSign(testReq, secretKey);
        }


    }

    @Getter
    @Setter
    public static class Test03 extends SignPayload {
        private String name;
        private String name2;
        private String name2_1;
        private String name3;
    }

    @Test
    public void testSign02() throws Exception {
        SignUtils.SIGN_TIMEOUT_SECONDS = 100;
        for (int i = 0; i < 1; i++) {
            String secretId = "123123131231";
            String secretKey = "sdfsdfsdfadsfasfdaefwef";

            Map<String, Object> testMap = new HashMap<>();
            testMap.put("name2_1", 123123);
            testMap.put("name", "123123");
            testMap.put("name2", "12312435435");
            JSONObject jsonObject = SignUtils.genSign(SignType.SHA256, secretId, secretKey, testMap);

            System.out.println(JSON.toJSONString(testMap));
            Test03 test03 = JSON.parseObject(JSON.toJSONString(testMap), Test03.class);
            test03.setName("123123");
            test03.setName3("12312313123");
            SignUtils.checkSign(test03, secretKey, "name3");
        }
    }

    /**
     * 测试非对称加密算法签名
     * @throws Exception
     */
    @Test
    public void testAsymmetricSign() throws Exception {
        SignUtils.SIGN_TIMEOUT_SECONDS = 100;
        Map<RsaKeyType, String> rsaKeyMap = RsaUtils.genKeyPair();
        for (int i = 0; i < 5; i++) {
            String secretId = "123123131231";
            String secretKey = rsaKeyMap.get(RsaKeyType.PRIVATE);

            TestReq testReq = new TestReq();
            testReq.setName("2");
            SignUtils.genSign(SignType.RSA2, secretId, secretKey, testReq);
            System.out.println("生成签名:" + JSON.toJSONString(testReq));

            System.out.println("开始验证签名");
            secretKey = rsaKeyMap.get(RsaKeyType.PUBLIC);
            SignUtils.checkSign(testReq, secretKey);

            // TODO 修改参数会导致签名校验失败
//        System.out.println("修改数据");
//        testReq.setName("1");
//        SignUtils.checkSign(testReq, secretKey);

            // TODO 已经被排除的签名字段, 修改也不会导致签名验证失败
            testReq.setName2("1");
            testReq.getTest01().setK2("123123123123");
            SignUtils.checkSign(testReq, secretKey, "name2");
        }


    }
}