package cn.imatu.framework.sign;

import cn.imatu.framework.sign.core.SignPayload;
import com.alibaba.fastjson2.annotation.JSONField;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @author shenguangyang
 */
@Getter
@Setter
public class TestReqCopy extends SignPayload {
    private String name03;
    private String name;
    private String name2 = "1234";
    private String payDataType = "test01";
    private BigDecimal price;

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date date = new Date();


    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime date1 = LocalDateTime.now();

    private Test01 test01 = new Test01();

    private List<Test02> test02List = Arrays.asList(new Test02(), new Test02());

    @Data
    public static class Test01 {
        private List<TestReq.Test02> test02List = Arrays.asList(new TestReq.Test02(), new TestReq.Test02());
        private String k1 = "test01";
        private String k2 = "test01.k2";
        private Test03 k3 = new Test03();
        private Test04 k4 = new Test04();
        private String k5;
    }

    @Data
    public static class Test02 {
        private String couponPrice = "test01";
        private String goodsId = "test01";
        private String costPrice = "test01";
        private String goodsName = "test01";

    }

    @Data
    public static class Test03 {
        private String k1 = "test01";
    }

    @Data
    public static class Test04 {
        private String testData_4 = "test01";
        private Test04_1 testData_4_1 = new Test04_1();
        private List<Test04_2> testData_4_2 = Collections.singletonList(new Test04_2());
    }

    @Data
    public static class Test04_1 {
        private String payDataType = "test01";
        private List<Test02> test02List = Arrays.asList(new Test02(), new Test02());

    }

    @Data
    public static class Test04_2 {
        private String payDataType = "test01";

    }
}
