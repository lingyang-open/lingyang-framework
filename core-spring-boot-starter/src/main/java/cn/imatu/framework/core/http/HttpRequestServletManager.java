package cn.imatu.framework.core.http;

import cn.imatu.framework.core.constant.WebType;
import cn.imatu.framework.core.utils.IpUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * @author shenguangyang
 */
public class HttpRequestServletManager implements HttpRequestManager {
    private final HttpServletRequest httpServletRequest;

    public HttpRequestServletManager(HttpServletRequest httpServletRequest) {
        this.httpServletRequest = httpServletRequest;
    }

    @Override
    public String getHeader(String key) {
        return httpServletRequest.getHeader(key);
    }

    @Override
    public String getRequestUri() {
        return httpServletRequest.getRequestURI();
    }

    @Override
    public WebType getWebType() {
        return WebType.SERVLET;
    }

    @Override
    public String getRequestIp() {
        return IpUtils.getRequestIp(this);
    }

    @Override
    public String getRemoteAddress() {
        return httpServletRequest.getRemoteAddr();
    }
}
