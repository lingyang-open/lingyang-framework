package cn.imatu.framework.core.json;

import cn.hutool.core.util.ObjUtil;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.math.BigDecimal;

/**
 * 主要用于 解决 BigDecimal 类型属性整数返回到前端不显示.00的问题
 *
 * 使用方式: 在响应属性标记注解 @JsonSerialize(using = ToPlainStringSerializer.class)
 *
 * @author shenguangyang
 */
public class ToPlainStringSerializer extends JsonSerializer<BigDecimal> {
 
    @Override
    public void serialize(BigDecimal value, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        if (ObjUtil.isNotNull(value)) {
            jsonGenerator.writeString(value.toPlainString());
        }
    }
    
}