package cn.imatu.framework.core.page;

import cn.imatu.framework.core.utils.servlet.ServletUtils;
import cn.hutool.core.date.DateUtil;

import java.util.Objects;

/**
 * 表格数据处理
 * @deprecated
 * @author shenguangyang
 */
public class TableSupport {
    /**
     * 当前记录起始索引
     */
    public static final String PAGE_NUM = "pageNum";

    /**
     * 每页显示记录数
     */
    public static final String PAGE_SIZE = "pageSize";

    /**
     * 排序列
     */
    public static final String ORDER_BY_COLUMN = "orderByColumn";

    /**
     * 排序的方向 "desc" 或者 "asc".
     */
    public static final String IS_ASC = "isAsc";

    public static final String BEGIN_DATE  = "beginDate";
    public static final String END_DATE  = "endDate";

    /**
     * 封装分页对象
     */
    public static PageModel getPageModel() {
        PageModel pageModel = new PageModel();
        Integer pageNum = ServletUtils.getParameterToInt(PAGE_NUM);
        Integer pageSize = ServletUtils.getParameterToInt(PAGE_SIZE);

        pageModel.setPageNum(Objects.isNull(pageNum) ? 1 : pageNum);
        pageModel.setPageSize(Objects.isNull(pageSize) ? 20 : pageSize);
        pageModel.setOrderByColumn(ServletUtils.getParameter(ORDER_BY_COLUMN));
        pageModel.setIsAsc(ServletUtils.getParameter(IS_ASC));
        pageModel.setBeginDate(DateUtil.parse(ServletUtils.getParameter(BEGIN_DATE)));
        pageModel.setEndDate(DateUtil.parse(ServletUtils.getParameter(END_DATE)));
        return pageModel;
    }

    public static PageModel buildPageRequest() {
        return getPageModel();
    }
}
