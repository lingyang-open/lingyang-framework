package cn.imatu.framework.core.response;

import cn.imatu.framework.core.constant.WebType;

/**
 * @author shenguangyang
 */
public class ReactiveRequestHandlerImpl implements RequestHandler {

    @Override
    public String getHeader(String key) {
        return null;
    }

    @Override
    public WebType getWebType() {
        return WebType.REACTIVE;
    }
}
