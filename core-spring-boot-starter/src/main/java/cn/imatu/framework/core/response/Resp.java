package cn.imatu.framework.core.response;

import cn.imatu.framework.exception.BaseError;
import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.slf4j.MDC;
import org.springframework.http.HttpStatus;

import java.io.Serializable;
import java.util.Optional;

/**
 * 响应体
 *
 * @author shenguangyang
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class Resp<T> implements Serializable {
    private static final long serialVersionUID = 1L;
    protected static DefaultResponseInfo defaultResponseInfo;

    public static String traceIdKey = "traceId";

    private Integer code;

    private Boolean success;

    private String msg;

    private T data;

    /**
     * 请求链路跟踪id
     */
    private String traceId;

    @JsonIgnore
    @JSONField(serialize = false)
    @Getter
    @Setter
    private HttpStatus status;

    public static <T> Resp<T> ok() {
        return restResult(HttpStatus.OK, null, true, defaultResponseInfo.getSuccess(), defaultResponseInfo.getSuccessMsg());
    }

    public static <T> Resp<T> ok(T data) {
        return restResult(HttpStatus.OK, data, true, defaultResponseInfo.getSuccess(), defaultResponseInfo.getSuccessMsg());
    }

    public static <T> Resp<T> ok(T data, String msg) {
        return restResult(HttpStatus.OK, data, true, defaultResponseInfo.getSuccess(), msg);
    }

    public static <T> Resp<T> fail() {
        return restResult(HttpStatus.OK, null, false, defaultResponseInfo.getFail(), defaultResponseInfo.getFailMsg());
    }

    public static <T> Resp<T> fail(String msg) {
        return restResult(HttpStatus.OK, null, false, defaultResponseInfo.getFail(), msg);
    }

    public static <T> Resp<T> fail(HttpStatus status, String msg) {
        return restResult(status, null, false, defaultResponseInfo.getFail(), msg);
    }

    public static <T> Resp<T> fail(BaseError baseError) {
        return restResult(baseError.getStatus(),  null,false, baseError.getCode(), baseError.getMessage());
    }

    public static <T> Resp<T> fail(Integer code, String msg) {
        code = Optional.ofNullable(code).orElse(defaultResponseInfo.getFail());
        return restResult(HttpStatus.OK, null, false, code, msg);
    }

    public static <T> Resp<T> fail(HttpStatus status, Integer code, String msg) {
        code = Optional.ofNullable(code).orElse(defaultResponseInfo.getFail());
        return restResult(status, null, false, code, msg);
    }

    public static <T> Resp<T> fail(Integer code, String msg, T data) {
        code = Optional.ofNullable(code).orElse(defaultResponseInfo.getFail());
        return restResult(null, data, false, code, msg);
    }

    public static <T> Resp<T> fail(HttpStatus status, Integer code, String msg, T data) {
        code = Optional.ofNullable(code).orElse(defaultResponseInfo.getFail());
        return restResult(status, data, false, code, msg);
    }

    public static <T> Resp<T> fail(BaseError error, T data) {
        Integer code = Optional.ofNullable(error.getCode()).orElse(defaultResponseInfo.getFail());
        return restResult(error.getStatus(), data, false, code, error.getMessage());
    }

    // 链式调用 ======================================================================

    public Resp<T> code(Integer code) {
        this.code = Optional.ofNullable(code)
                .orElse(defaultResponseInfo.getFail());
        return this;
    }

    public Resp<T> data(T data) {
        this.data = data;
        return this;
    }

    public Resp<T> msg(String msg) {
        this.msg = msg;
        return this;
    }

    public Resp<T> codeAndMsg(BaseError baseError) {
        this.msg = baseError.getMessage();
        this.code = baseError.getCode();
        return this;
    }

    public static <T> Resp<T> restResult(HttpStatus status, T data, boolean success, Integer code, String msg) {
        status = Optional.ofNullable(status).orElse(HttpStatus.OK);
        Resp<T> apiResult = new Resp<>();
        apiResult.setCode(code);
        apiResult.setData(data);
        apiResult.setStatus(status);
        apiResult.setMsg(msg);
        apiResult.setSuccess(success);
        apiResult.setTraceId(Optional.ofNullable(MDC.get(traceIdKey)).orElse(""));
        return apiResult;
    }

    public static void setTraceIdKey(String traceIdKey) {
        Resp.traceIdKey = traceIdKey;
    }

    public static void setDefaultResponseInfo(DefaultResponseInfo value) {
        Resp.defaultResponseInfo = value;
    }

    public RetOps<T> toOps() {
        return RetOps.of(this);
    }
}
