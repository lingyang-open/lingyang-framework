package cn.imatu.framework.core.response;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author shenguangyang
 */
@Configuration
public class ResponseConfig {

    @Bean
    @ConditionalOnMissingBean(DefaultResponseInfo.class)
    public DefaultResponseInfo defaultResponseCode() {
        DefaultResponseInfo responseCode = new DefaultResponseInfo();
        responseCode.setFail(500);
        responseCode.setSuccess(200);
        return responseCode;
    }
}
