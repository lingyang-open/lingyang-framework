package cn.imatu.framework.core.response;

import cn.imatu.framework.core.constant.WebType;
import cn.imatu.framework.core.utils.servlet.ServletUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

/**
 * @author shenguangyang
 */
public class ServletRequestHandlerImpl implements RequestHandler {

    @Override
    public String getHeader(String key) {
        String value = "";
        Optional<HttpServletRequest> request = ServletUtils.getRequest();
        if (request.isPresent()) {
            value = request.get().getHeader(key);
        }
        return value;
    }

    @Override
    public WebType getWebType() {
        return WebType.SERVLET;
    }
}
