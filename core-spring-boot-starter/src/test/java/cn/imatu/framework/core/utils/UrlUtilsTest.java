package cn.imatu.framework.core.utils;

import cn.imatu.framework.tool.core.UrlUtils;
import org.junit.jupiter.api.Test;

/**
 * @author shenguangyang
 * @date 2022-03-29 21:27
 */
class UrlUtilsTest {

    @Test
    void test() {
        System.out.println("removeStartsSlash = " + UrlUtils.removeStartsSlash("/opt/test"));
        System.out.println("removeEndSlash = " + UrlUtils.removeEndSlash("/opt/test/"));
        System.out.println("addStartsSlash = " + UrlUtils.addStartsSlash("/opt/test"));
        System.out.println("addStartsSlash = " + UrlUtils.addStartsSlash("opt/test"));
        System.out.println("addEndSlash = " + UrlUtils.addEndSlash("opt/test/"));
        System.out.println("addEndSlash = " + UrlUtils.addEndSlash("opt/test"));
    }
}