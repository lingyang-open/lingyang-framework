package cn.imatu.framework.data.mate;

import cn.imatu.framework.data.mate.fieldbind.DefaultDataBind;
import cn.imatu.framework.data.mate.fieldbind.DefaultFieldBindAnnotationGetter;
import cn.imatu.framework.data.mate.fieldbind.FieldBindHandler;
import cn.imatu.framework.data.mate.fieldbind.FieldValueManager;
import cn.imatu.framework.data.mate.fieldbind.GetSourceFieldValueHandler;
import cn.imatu.framework.data.mate.fieldbind.SetTargetFieldValueHandler;
import cn.imatu.framework.data.mate.fieldbind.aspect.FieldBindSetterAspect;
import cn.imatu.framework.data.mate.fieldbind.inter.IDataBind;
import cn.imatu.framework.data.mate.fieldconvert.ConvertFieldTypeHandler;
import cn.imatu.framework.data.mate.fieldconvert.DefaultFieldConvertAnnotGetter;
import cn.imatu.framework.data.mate.fieldconvert.ScanConvertFieldHandler;
import cn.imatu.framework.data.mate.fieldconvert.SetTargetFieldValueManager;
import cn.imatu.framework.data.mate.fieldconvert.aspect.FileUrlConvertAspect;
import cn.imatu.framework.data.mate.fieldconvert.inter.DefaultFieldValueConvert;
import cn.imatu.framework.data.mate.fieldconvert.inter.IFieldValueConvert;
import cn.imatu.framework.data.mate.fieldsensitive.SensitiveHandler;
import cn.imatu.framework.data.mate.fieldsensitive.aspect.FieldSensitiveHandleAspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

import javax.annotation.PostConstruct;

/**
 * @author shenguangyang
 */
@AutoConfiguration
@Import({
    FieldBindHandler.class, FieldBindSetterAspect.class, DefaultFieldBindAnnotationGetter.class,
    SensitiveHandler.class, FieldSensitiveHandleAspect.class,

    FieldValueManager.class,
    SetTargetFieldValueHandler.StringSet.class, SetTargetFieldValueHandler.StringArray.class,
    SetTargetFieldValueHandler.StringList.class, SetTargetFieldValueHandler.StringType.class,
    GetSourceFieldValueHandler.StringType.class, GetSourceFieldValueHandler.IntegerArray.class,
    GetSourceFieldValueHandler.StringList.class, GetSourceFieldValueHandler.StringSet.class,
    GetSourceFieldValueHandler.IntegerType.class,

    ConvertFieldTypeHandler.StringSet.class, ConvertFieldTypeHandler.StringArray.class,
    ConvertFieldTypeHandler.StringList.class, ConvertFieldTypeHandler.StringType.class,
    ConvertFieldTypeHandler.StringType.class, ScanConvertFieldHandler.class,
    SetTargetFieldValueManager.class, FileUrlConvertAspect.class

})
public class LyDataMateAutoConfiguration {
    private static final Logger log = LoggerFactory.getLogger(LyDataMateAutoConfiguration.class);

    @PostConstruct
    public void init() {
        log.info("init {}", this.getClass().getName());
    }

    @Bean
    @ConditionalOnMissingBean(IDataBind.class)
    public DefaultDataBind defaultDataBind() {
        return new DefaultDataBind();
    }

    @Bean
    @ConditionalOnMissingBean(DefaultFieldConvertAnnotGetter.class)
    public DefaultFieldConvertAnnotGetter fieldConvertAnnotGetter() {
        return new DefaultFieldConvertAnnotGetter();
    }

    @Bean
    @ConditionalOnMissingBean(IFieldValueConvert.class)
    public IFieldValueConvert fileUrlConvert() {
        return new DefaultFieldValueConvert();
    }
}
