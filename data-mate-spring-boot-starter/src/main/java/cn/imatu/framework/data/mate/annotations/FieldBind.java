package cn.imatu.framework.data.mate.annotations;

import java.lang.annotation.*;

/**
 * 字段绑定 (回写字典), 在对象属性上使用
 *
 * @author shenguangyang
 */
@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.ANNOTATION_TYPE})
public @interface FieldBind {
    String sharding() default "";

    /**
     * 分隔每个元素的分隔符号(字典字段分隔符)
     * <p>
     * 当前注解所在字符串字段code时候, 每个code采用什么分割这里要指明 <br/>
     * eg:
     * <blockquote><pre>
     * {@literal @FieldBind(type = "test", target = "testText", delimiter = "|")}
     *  private String test;
     *  private String testText;
     * </pre></blockquote>
     * 当 test = 1|2|3(假如code = 1 对应 data1, 依次类推), 则testText将被赋值为
     * testText = data1|data2|data3<br/>
     * 目标字段分隔符, 请看 {@link #targetDelimiter()}
     * </p>
     *
     * 只有当目标字段是字符串类型时才生效, 其他类型不生效
     * @apiNote 只有在字段类型是 {@code String} 时才会生效
     */
    String delimiter() default "";

    /**
     * 目标字段分隔符, 只针对目标字段时字符串类型, 集合和数组类型不生效
     */
    String targetDelimiter() default "";

    /**
     * 字典划分成如下几部分
     * <p>
     * type: 类型, 一组键值对是属于什么类型的 <br/>
     * code: 类型中每个元素的key就是code编码 <br/>
     * value: code对应的值 <br/>
     * </p>
     */
    String type();

    /**
     * 目标字段名
     *
     * 注意: 目标字段必须为字符串类型, eg: List<String>、Set<String>、String、String[]
     */
    String target();


    /**
     * 应用场景: 针对字段code等于某个值时, 从其他字段获取详细信息
     *
     * 应用场景: 如果当编码为其他编码值时(eg: 99), 需要从对象中映射目标字段 {@link #mapperTarget()}
     * 获取值赋值到目标字段上 {@link #target()}
     *
     * 如果 {@link #mapperTarget()} 为空, 该值无效, 同样的, 如果该值为空, {@link #mapperTarget()} 也无效
     */
    String codeDefault() default "";

    /**
     * 应用场景: 针对字段code等于某个值时, 从其他字段获取详细信息
     *
     * 映射目标字段名, 当 {@link #codeDefault()} 不为空 且 与注解所在字段相匹配时候才会从当前指定
     * 的字段上获取值赋值到 {@link #target()} 字段上
     */
    String mapperTarget() default "";

    /**
     * 应用场景, 针对字段不存在的场景
     *
     * 当通过code没有找到目标值时, 是否将code值, 直接赋值到目标字段上
     */
    boolean targetEqCode() default false;
}

