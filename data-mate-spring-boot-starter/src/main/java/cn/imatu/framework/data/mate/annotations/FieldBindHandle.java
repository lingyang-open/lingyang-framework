package cn.imatu.framework.data.mate.annotations;

import java.lang.annotation.*;

/**
 * 字段绑定 (回写字典), 在某个方法上使用, 则返回的对象数据会被进行字段绑定处理
 *
 * @author shenguangyang
 */
@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.ANNOTATION_TYPE})
public @interface FieldBindHandle {

}

