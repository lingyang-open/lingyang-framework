package cn.imatu.framework.data.mate.annotations;

import cn.imatu.framework.data.mate.fieldsensitive.enums.StrategyType;

import java.lang.annotation.*;

/**
 * 数据脱敏
 *
 * @author shenguangyang
 */
@Target({ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface FieldSensitive {
    /**
     * 策略
     * 当值为 {@link StrategyType#CUSTOM} 时, 则需要你去指定 {@link #frontNum()} 和 {@link #endNum()} 来实现脱敏
     */
    String strategy() default StrategyType.CUSTOM;

    /**
     * 保留前面字符的位数
     */
    int frontNum() default 2;

    /**
     * 保留后面字符的位数
     */
    int endNum() default 2;

    int starNum() default -1;

    /**
     * 字段描述, 当脱敏失败或者还原脱敏对象失败会进行提示
     */
    String desc() default "";
}
