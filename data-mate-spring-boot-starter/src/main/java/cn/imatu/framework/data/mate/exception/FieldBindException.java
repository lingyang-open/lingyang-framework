package cn.imatu.framework.data.mate.exception;

import cn.hutool.core.util.StrUtil;
import cn.imatu.framework.exception.BaseError;
import cn.imatu.framework.exception.BaseException;

/**
 * @author shenguangyang
 */
public class FieldBindException extends BaseException {
    private static final long serialVersionUID = 1L;

    public FieldBindException(String message) {
        this.message = message;
    }

    public FieldBindException(CharSequence template, Object... params) {
        this.message = StrUtil.format(template, params);
    }

    public FieldBindException(BaseError baseError) {
        super(baseError);
    }

    public FieldBindException(Integer code, String message) {
        this.message = message;
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public Integer getCode() {
        return code;
    }
}