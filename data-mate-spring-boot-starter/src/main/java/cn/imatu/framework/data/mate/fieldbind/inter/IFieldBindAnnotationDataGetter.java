package cn.imatu.framework.data.mate.fieldbind.inter;

import cn.imatu.framework.data.mate.fieldbind.model.AnnotationMetadata;

import java.lang.annotation.Annotation;

/**
 * @author shenguangyang
 */
@FunctionalInterface
public interface IFieldBindAnnotationDataGetter<T extends Annotation> {
    AnnotationMetadata initFieldBindAnnotation(T fieldAnnotation);
}
