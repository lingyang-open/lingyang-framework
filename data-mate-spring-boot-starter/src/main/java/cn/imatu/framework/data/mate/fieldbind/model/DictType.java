package cn.imatu.framework.data.mate.fieldbind.model;

import cn.imatu.framework.data.mate.annotations.FieldBind;
import lombok.*;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 字典类型
 * <p>
 * 同一个类型的字典可能会在一个对象中多个字段中, 只是code值不一样
 * </p>
 *
 * @author shenguangyang
 * @see FieldBind
 * @see DictClassCache
 * @see AnnotationMetadata
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DictType {
    /**
     * 字典类型
     *
     * @see FieldBind#type()
     */
    private String type;

    /**
     * 注解所在字段值
     * <p>
     * 如果字段值是采用 {@link FieldBind#delimiter()} 进行分割的, 会自动拆分成一个一个元素
     * 的, eg: 字段值为 1|2|3|4, 则这里的 fieldValues = [1,2,3,4]
     * </p>
     */
    private Set<Object> fieldValues = ConcurrentHashMap.newKeySet();
}
