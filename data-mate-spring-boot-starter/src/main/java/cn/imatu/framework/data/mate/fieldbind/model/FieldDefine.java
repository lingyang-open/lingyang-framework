package cn.imatu.framework.data.mate.fieldbind.model;

import cn.imatu.framework.data.mate.annotations.FieldBind;
import lombok.Data;

import java.lang.reflect.Field;

/**
 * 基本类型的字段数据, 这里的基本类型包含 String, 基本类型包装类, 基本类型非包装类
 */
@Data
public class FieldDefine {
    /**
     * 注解
     */
    private AnnotationMetadata annotationMetadata;

    /**
     * 注解所在字段
     */
    private Field field;
    /**
     * 目标字段
     */
    private Field targetField;

    /**
     * 映射字段
     * @see FieldBind#codeDefault()
     */
    private Field mapperField;
}
