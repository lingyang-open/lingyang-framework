package cn.imatu.framework.data.mate.fieldconvert.aspect;

import cn.imatu.framework.data.mate.fieldconvert.ScanConvertFieldHandler;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Arrays;

/**
 * 字段绑定处理
 *
 * @author shenguangyang
 */
@Aspect
@Component
@Slf4j
public class FileUrlConvertAspect {
    @Resource
    private ScanConvertFieldHandler scanConvertFieldHandler;

    // 配置织入点
    @Pointcut("@annotation(cn.imatu.framework.data.mate.annotations.FieldConvertHandle)")
    public void pointcut() {
    }


    @Around(value = "pointcut()")
    public Object fieldConvert(ProceedingJoinPoint point) throws Throwable {
        Object[] args = point.getArgs();
        if (args != null) {
            Arrays.stream(args).forEach(e -> scanConvertFieldHandler.handleField(e, false));
        }

        Object result = point.proceed();
        scanConvertFieldHandler.handleField(result, true);
        return result;
    }
}
