package cn.imatu.framework.data.mate.fieldconvert.inter;

import cn.imatu.framework.data.mate.annotations.FieldConvert;
import cn.imatu.framework.data.mate.fieldconvert.model.AnnotInfo;
import cn.imatu.framework.data.mate.fieldconvert.model.FieldDefine;
import lombok.extern.slf4j.Slf4j;

import java.lang.annotation.Annotation;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author shenguangyang
 */
@Slf4j
public class DefaultFieldValueConvert implements IFieldValueConvert {

    @Override
    public boolean isConvertToTarget(List<String> sourceList, AnnotInfo annotInfo) {
        Annotation customAnnot = annotInfo.getCustomAnnot();
        return customAnnot instanceof FieldConvert;
    }

    @Override
    public boolean isConvertToSource(List<String> targetList, AnnotInfo annotInfo) {
        Annotation customAnnot = annotInfo.getCustomAnnot();
        return customAnnot instanceof FieldConvert;
    }

    @Override
    public List<String> toTarget(FieldDefine fieldDefine, List<String> sourceList) {
        return sourceList.stream().map(e -> e.replace("https://", "")).collect(Collectors.toList());
    }

    @Override
    public List<String> toSource(FieldDefine fieldDefine, List<String> targetList) {
        return targetList.stream().map(e -> "https://" + e).collect(Collectors.toList());
    }
}
