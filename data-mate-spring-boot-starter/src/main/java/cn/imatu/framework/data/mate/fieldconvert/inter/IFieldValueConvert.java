package cn.imatu.framework.data.mate.fieldconvert.inter;

import cn.imatu.framework.data.mate.annotations.FieldConvert;
import cn.imatu.framework.data.mate.fieldconvert.model.AnnotInfo;
import cn.imatu.framework.data.mate.fieldconvert.model.FieldDefine;

import java.util.List;

/**
 * @author shenguangyang
 */
public interface IFieldValueConvert {
    /**
     * 是否对目标url转成key
     * @param sourceList {@link FieldConvert} 注解所在字段值
     */
    default boolean isConvertToTarget(List<String> sourceList, AnnotInfo annotInfo) {
        return true;
    }

    default boolean isConvertToSource(List<String> targetList, AnnotInfo annotInfo) {
        return true;
    }

    /**
     * @param sourceList {@link FieldConvert} 注解所在字段值
     * @return {@link FieldConvert#targetField()} 指定的字段值
     */
    List<String> toTarget(FieldDefine fieldDefine, List<String> sourceList);

    /**
     *
     * 将数据从目标字段 {@link FieldConvert#targetField()} 加工成 {@link FieldConvert} 注解所在字段值
     * @param targetList 目标字段值
     * @return {@link FieldConvert} 注解所在字段值
     */
    List<String> toSource(FieldDefine fieldDefine, List<String> targetList);
}
