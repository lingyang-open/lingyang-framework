package cn.imatu.framework.data.mate.fieldconvert.model;

import lombok.Data;

import java.lang.reflect.Field;

/**
 * 基本类型的字段数据, 这里的基本类型包含 String, 基本类型包装类, 基本类型非包装类
 */
@Data
public class FieldDefine {
    /**
     * 注解
     */
    private AnnotInfo annot;

    /**
     * 注解所在字段
     */
    private Field field;
    /**
     * 目标字段
     */
    private Field targetField;
}
