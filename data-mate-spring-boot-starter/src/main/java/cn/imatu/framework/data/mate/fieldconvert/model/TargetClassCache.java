package cn.imatu.framework.data.mate.fieldconvert.model;

import cn.imatu.framework.data.mate.annotations.FieldConvert;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * 存放需要实体类中标注 {@link FieldConvert} 注解的信息
 *
 * @author shenguangyang
 */
@Getter
@Setter
public class TargetClassCache {
    private Class<?> clazz;
    /**
     * 添加了 {@link FieldConvert} 注解的字段
     */
    private List<FieldDefine> fieldList;

    public TargetClassCache() {
        this.fieldList = new ArrayList<>();
    }
}
