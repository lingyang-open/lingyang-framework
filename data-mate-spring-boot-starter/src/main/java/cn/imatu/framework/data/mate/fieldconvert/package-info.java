/**
 * 转换字段值处理器:
 * eg: 转换文件url
 * 对于请求: 将url http://www.file.com/image/test.jpg?sign=xxxx 转成 key: /image/test.jpg
 * 对于响应: 将key /image/test.jpg 转成 http://www.file.com/image/test.jpg?sign=xxxx
 * @author shenguangyang
 */
package cn.imatu.framework.data.mate.fieldconvert;