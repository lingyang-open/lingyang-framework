package cn.imatu.framework.data.mate.fieldsensitive;

import cn.imatu.framework.tool.core.StringUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * @author shenguangyang
 */
@Slf4j
public class SensitiveDataUtils {

    /**
     * [中文姓名] 只显示第一个汉字，其他隐藏为2个星号<例子：李**>
     *
     * @param fullName 需要脱敏的数据
     * @return 脱敏后的数据
     */
    public static String chineseName(String fullName) {
        if (StringUtils.isBlank(fullName)) {
            return "";
        }
        String name = StringUtils.left(fullName, 1);
        return StringUtils.rightPad(name, StringUtils.length(fullName), "*");
    }

    /**
     * [中文姓名] 只显示第一个汉字，其他隐藏为2个星号<例子：李**>
     *
     * @param familyName 姓
     * @param givenName  名
     * @return 脱敏后的数据
     */
    public static String chineseName(String familyName, Object givenName) {
        if (StringUtils.isBlank(familyName) || StringUtils.isBlank(String.valueOf(givenName))) {
            return "";
        }
        return chineseName(familyName + givenName);
    }


    /**
     * [身份证号] 显示最后四位，其他隐藏。共计18位或者15位。<例子：*************5762>
     *
     * @param id 需要脱敏的数据
     * @return 脱敏后的数据
     */
    public static String idCardNum(String id) {
        if (StringUtils.isBlank(id)) {
            return "";
        }
        String num = StringUtils.right(id, 4);
        return StringUtils.leftPad(num, StringUtils.length(id), "*");
    }

    /**
     * [固定电话] 后四位，其他隐藏<例子：****1234>
     *
     * @param num 需要脱敏的数据
     * @return 脱敏后的
     */
    public static String fixedPhone(String num) {
        if (StringUtils.isBlank(num)) {
            return "";
        }
        return StringUtils.leftPad(StringUtils.right(num, 4), StringUtils.length(num), "*");
    }

    /**
     * [手机号码] 前三位，后四位，其他隐藏<例子:138******1234>
     *
     * @param num 需要脱敏的数据
     * @return 脱敏后的数据
     */
    public static String mobilePhone(String num) {
        if (StringUtils.isBlank(num)) {
            return "";
        }
        return StringUtils.left(num, 3).concat(StringUtils.removeStart(StringUtils.leftPad(StringUtils.right(num, 4), StringUtils.length(num), "*"), "***"));
    }

    public static void main(String[] args) {
        String num = "12312312312312313";
        System.out.println();
    }

    /**
     * 通用的脱敏, 不显示详细信息；我们要对个人信息增强保护<例子：北京市海淀区****>
     *
     * @param data       需要脱敏的数据
     * @param reserveSize 保留大小
     * @return 脱敏后的数据
     */
    public static String setReserveSize(String data, int reserveSize) {
        if (StringUtils.isBlank(data)) {
            return "";
        }
        int length = StringUtils.length(data);
        return StringUtils.rightPad(StringUtils.left(data, reserveSize), length, "*");
    }

    /**
     * [电子邮箱] 邮箱前缀仅显示第一个字母，前缀其他隐藏，用星号代替，@及后面的地址显示<例子:g**@163.com>
     *
     * @param email 需要脱敏的数据
     * @return 脱敏后的数据
     */
    public static String email(String email) {
        if (StringUtils.isBlank(email)) {
            return "";
        }
        int index = StringUtils.indexOf(email, "@");
        if (index <= 1) {
            return email;
        } else {
            return StringUtils.rightPad(StringUtils.left(email, 3), index, "*").concat(StringUtils.mid(email, index, StringUtils.length(email)));
        }
    }

    /**
     * [银行卡号] 前六位，后四位，其他用星号隐藏每位1个星号<例子:6222600**********1234>
     *
     * @param cardNum 需要脱敏的数据
     * @return 脱敏后的数据
     */
    public static String bankCard(String cardNum) {
        if (StringUtils.isBlank(cardNum)) {
            return "";
        }
        return StringUtils.left(cardNum, 6).concat(StringUtils.removeStart(StringUtils.leftPad(StringUtils.right(cardNum, 4), StringUtils.length(cardNum), "*"), "******"));
    }

    /**
     * [公司开户银行联号] 公司开户银行联行号,显示前两位，其他用星号隐藏，每位1个星号<例子:12********>
     *
     * @param code 需要脱敏的数据
     * @return 脱敏后的数据
     */
    public static String bankNumber(String code) {
        if (StringUtils.isBlank(code)) {
            return "";
        }
        return StringUtils.rightPad(StringUtils.left(code, 2), StringUtils.length(code), "*");
    }
}
