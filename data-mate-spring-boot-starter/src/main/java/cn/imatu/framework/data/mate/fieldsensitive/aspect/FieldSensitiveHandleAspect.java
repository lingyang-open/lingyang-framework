package cn.imatu.framework.data.mate.fieldsensitive.aspect;

import cn.imatu.framework.data.mate.annotations.FieldSensitiveHandle;
import cn.imatu.framework.data.mate.fieldsensitive.SensitiveHandler;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.lang.reflect.Method;
import java.util.Objects;

/**
 * 字段绑定处理
 *
 * @author shenguangyang
 */
@Aspect
@Component
public class FieldSensitiveHandleAspect {
    private static final Logger log = LoggerFactory.getLogger(FieldSensitiveHandleAspect.class);
    @Resource
    private SensitiveHandler sensitiveHandler;

    // 配置织入点
    @Pointcut("@annotation(cn.imatu.framework.data.mate.annotations.FieldSensitiveHandle)")
    public void pointcut() {
    }

    @AfterReturning(pointcut = "pointcut()", returning = "result")
    public void doAfterReturning(JoinPoint joinPoint, Object result) {
        // 获得注解
        FieldSensitiveHandle anno = getAnnotationLog(joinPoint);
        if (Objects.isNull(anno)) {
            return;
        }
        sensitiveHandler.handleField(result);
    }


    /**
     * 是否存在注解，如果存在就获取
     */
    private FieldSensitiveHandle getAnnotationLog(JoinPoint joinPoint) {
        Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        Method method = methodSignature.getMethod();

        if (method != null) {
            return method.getAnnotation(FieldSensitiveHandle.class);
        }
        return null;
    }
}
