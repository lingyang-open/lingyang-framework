package cn.imatu.framework.data.mate.fieldbind;

import cn.imatu.framework.data.mate.fieldbind.model.*;
import com.alibaba.fastjson2.JSON;
import org.apache.commons.lang3.time.StopWatch;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author shenguangyang
 */
@SpringBootTest
class FieldBindHandlerTest {
    @Resource
    private FieldBindHandler fieldBindHandler;

    @Test
    void createDictClassCache() {
        DictClassCache dictClassCache = fieldBindHandler.createDictClassCache(new TestReq());
        System.out.println(dictClassCache);
    }

    public static void main(String[] args) {
        TestReq testReq = new TestReq();
        List<String> key1 = new ArrayList<>();
//        key1.add("123123");
        List<String> key2 = new ArrayList<>();
        Map<Object, String> map = new HashMap<>();
        System.out.println(System.identityHashCode(testReq));
        System.out.println(testReq.hashCode());
        map.put(System.identityHashCode(key1), "");
        map.put(System.identityHashCode(key2), "");
        System.out.println("map");
    }

    @Test
    public void test() {
        for (int i = 0; i < 1; i++) {
            StopWatch started = StopWatch.createStarted();
            TestReq testReq = new TestReq();
            fieldBindHandler.handleField(testReq);
            System.out.println(JSON.toJSONString(testReq));

            // 测试code默认值时映射字段赋值到目标字段
            testReq = new TestReq();
            testReq.setName("99");
            fieldBindHandler.handleField(testReq);
            System.out.println(JSON.toJSONString(testReq));
            System.out.println("============================> time: " + started.getTime(TimeUnit.MILLISECONDS) + " ms");
        }
    }
}