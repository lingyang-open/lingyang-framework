package cn.imatu.framework.data.mate.fieldbind.model;

import cn.imatu.framework.data.mate.annotations.FieldBind;
import lombok.Getter;
import lombok.Setter;

/**
 * @author shenguangyang
 */
@Getter
@Setter
public class BaseTestReq {
    @FieldBind(type = "test1", target = "baseTest1")
    private String baseName = "1";

    private String baseTest1;
}
