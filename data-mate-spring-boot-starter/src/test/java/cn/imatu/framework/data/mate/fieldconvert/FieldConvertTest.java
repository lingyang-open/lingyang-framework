package cn.imatu.framework.data.mate.fieldconvert;

import cn.imatu.framework.data.mate.fieldconvert.mode.Test02Controller;
import cn.imatu.framework.data.mate.fieldconvert.mode.TestReq02;
import com.alibaba.fastjson2.JSON;
import org.apache.commons.lang3.time.StopWatch;
import org.junit.jupiter.api.Test;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * @author shenguangyang
 */
@SpringBootApplication(scanBasePackages = "cn.imatu.framework.data.mate")
@SpringBootTest(classes = FieldConvertTest.class)
public class FieldConvertTest {
    @Resource
    private ScanConvertFieldHandler scanConvertFieldHandler;
    @Resource
    private Test02Controller test02Controller;

    public static void main(String[] args) {
        SpringApplication.run(FieldConvertTest.class, args);
    }
    @Test
    public void test() {
        for (int i = 0; i < 2; i++) {
            StopWatch started = StopWatch.createStarted();
            TestReq02 testReq = new TestReq02();
            scanConvertFieldHandler.handleField(testReq, false);
            System.out.println("req: " + JSON.toJSONString(testReq));

            testReq.clearUrl();
            System.out.println("clearUrl-req: " + JSON.toJSONString(testReq));
            scanConvertFieldHandler.handleField(testReq, true);
            System.out.println("resp: " + JSON.toJSONString(testReq));
            System.out.println("============================> time: " + started.getTime(TimeUnit.MILLISECONDS) + " ms");
        }
        System.out.println("\n");
        TestReq02 testReq = new TestReq02();
        System.out.println("req: " + JSON.toJSONString(testReq));
        TestReq02 result = test02Controller.testReq02(testReq);
        System.out.println("result: " + JSON.toJSONString(result));
    }
}
