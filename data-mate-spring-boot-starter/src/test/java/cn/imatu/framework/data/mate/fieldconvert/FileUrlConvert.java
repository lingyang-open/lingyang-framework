package cn.imatu.framework.data.mate.fieldconvert;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 当前注解有以下作用, 只用用在基础类型上
 * 1. 将提交的数据url, 解析出key并赋值到key字段
 * 2. 将返回的数据中key转成url
 * @author shenguangyang
 */
@Target({ElementType.PARAMETER, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface FileUrlConvert {
    /**
     * 指定存储的数据key, eg: /image/test.jpg
     * 如果不填写就是当前字段
     */
    String targetField() default "";

    /**
     * 如果将数组转成字符串使用指定的分隔符进行拼接, 字符串转成数组使用指定字符串切割
     */
    String delimiter() default ",";
}
