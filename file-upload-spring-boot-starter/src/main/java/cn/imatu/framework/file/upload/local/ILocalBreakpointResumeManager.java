package cn.imatu.framework.file.upload.local;

import cn.imatu.framework.file.upload.local.model.*;
import cn.imatu.framework.file.upload.local.model.*;

/**
 * 将文件传输到本地(拥有断点续传功能)
 *
 * @author shenguangyang
 */
public interface ILocalBreakpointResumeManager {
    /**
     * 断点续传注册
     */
    BreakpointRegisterDTO register(BreakpointRegister breakpointRegister);

    /**
     * 上传分片(断点续传)
     */
    UploadChunkDTO uploadChunk(UploadChunk uploadChunk);

    /**
     * 检查分块是否存在
     *
     * @param fileCode  文件code
     * @param chunk     第几片
     * @param chunkSize 分片大小
     * @return false 分片不存在  true 分片存在
     */
    boolean checkChunk(String fileCode, Integer chunk, Integer chunkSize);

    /**
     * 合并文件块
     */
    MergeChunksDTO mergeChunks(MergeChunks mergeChunks) throws Exception;
}
