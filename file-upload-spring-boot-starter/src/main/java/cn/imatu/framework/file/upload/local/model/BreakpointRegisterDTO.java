package cn.imatu.framework.file.upload.local.model;

import java.util.Map;

/**
 * @author shenguangyang
 */
public class BreakpointRegisterDTO {
    /**
     * 是否已经被上传
     */
    private Boolean isUploaded;
    private String fileId;
    private String filePath;

    private Map<String, Object> ext;

    public Boolean getUploaded() {
        return isUploaded;
    }

    public void setUploaded(Boolean uploaded) {
        isUploaded = uploaded;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Map<String, Object> getExt() {
        return ext;
    }

    public void setExt(Map<String, Object> ext) {
        this.ext = ext;
    }

    public static BreakpointRegisterDTOBuilder builder() {
        return new BreakpointRegisterDTOBuilder();
    }

    public static final class BreakpointRegisterDTOBuilder {
        private Boolean isUploaded;
        private String fileId;
        private String filePath;
        private Map<String, Object> ext;

        private BreakpointRegisterDTOBuilder() {
        }

        public BreakpointRegisterDTOBuilder isUploaded(Boolean isUploaded) {
            this.isUploaded = isUploaded;
            return this;
        }

        public BreakpointRegisterDTOBuilder fileId(String fileId) {
            this.fileId = fileId;
            return this;
        }

        public BreakpointRegisterDTOBuilder filePath(String filePath) {
            this.filePath = filePath;
            return this;
        }

        public BreakpointRegisterDTOBuilder ext(Map<String, Object> ext) {
            this.ext = ext;
            return this;
        }

        public BreakpointRegisterDTO build() {
            BreakpointRegisterDTO breakpointRegisterDTO = new BreakpointRegisterDTO();
            breakpointRegisterDTO.setFileId(fileId);
            breakpointRegisterDTO.setFilePath(filePath);
            breakpointRegisterDTO.setExt(ext);
            breakpointRegisterDTO.isUploaded = this.isUploaded;
            return breakpointRegisterDTO;
        }
    }
}
