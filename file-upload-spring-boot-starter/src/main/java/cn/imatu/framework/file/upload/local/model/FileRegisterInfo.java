package cn.imatu.framework.file.upload.local.model;

import cn.imatu.framework.file.upload.local.enums.FileUploadStateEnum;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.unit.DataSize;

import java.util.Map;

/**
 * @author shenguangyang
 */
@Getter
@Setter
@Builder
public class FileRegisterInfo {
    /**
     * 主键ID
     */
    private String id;
    /**
     * 文件扩展名
     */
    private String fileExt;
    /**
     * 文件名称
     */
    private String fileName;
    /**
     * 文件大小
     */
    private DataSize fileSize;
    /**
     * 文件路径
     */
    private String filePath;

    /**
     * 对象路径,存储在文件服务器上的路径
     */
    private String objectPath;

    /**
     * 上传状态
     */
    private FileUploadStateEnum uploadStatus;

    /**
     * 文件唯一标识
     */
    private String fileCode;

    private Map<String, Object> ext;
}
