package cn.imatu.framework.file.upload.local.model;

import cn.imatu.framework.file.upload.exception.MergeChunksException;
import cn.imatu.framework.tool.core.exception.Assert;
import cn.imatu.framework.tool.core.file.MimeTypesUtils;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * @author shenguangyang
 */
@Getter
@Setter
public class MergeChunks {
    /**
     * 文件id
     */
    private String fileId;
    /**
     * 文件code
     */
    private String fileCode;
    /**
     * 文件名
     */
    private String fileName;
    /**
     * 文件大小
     */
    private Long fileSize;
    /**
     * 文件类型
     */
    private String contentType;

    /**
     * 文件后缀
     */
    private String fileExt;
    /**
     * 文件存放目录
     */
    private String filePath;
    /**
     * 对象路径
     */
    private String objectPath;
    /**
     * 额外参数
     */
    private Map<String, Object> ext;

    /**
     * 分片路径
     */
    private String chunkPath;

    /**
     * 分片文件
     */
    private List<File> chunkFiles;

    /**
     * 真实分片数量，0(操作运行上传的最大文件大小)或者等于chunkFiles大小，
     */
    private int chunks;

    public void check() {
        Assert.notEmpty(fileId, new MergeChunksException("lack fileId parameter"));
        Assert.notEmpty(fileCode, new MergeChunksException("lack fileCode parameter"));
        Assert.notEmpty(fileName, new MergeChunksException("lack fileName parameter"));
    }

    public void setContentType(String contentType) {
        if (StringUtils.isEmpty(this.contentType)) {
            if (StringUtils.isEmpty(this.fileExt)) {
                this.contentType = contentType;
            } else {
                this.contentType = MimeTypesUtils.getInstance().getMimetype("." + this.fileExt);
            }
        }
    }
}
