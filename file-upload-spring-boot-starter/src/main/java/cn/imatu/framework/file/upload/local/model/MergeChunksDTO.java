package cn.imatu.framework.file.upload.local.model;

import java.util.Map;

/**
 * @author shenguangyang
 */
public class MergeChunksDTO {
    /**
     * 文件id(对应数据库id)
     */
    private String fileId;
    /**
     * 本地文件路径
     */
    private String filePath;

    private Map<String, Object> ext;

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Map<String, Object> getExt() {
        return ext;
    }

    public void setExt(Map<String, Object> ext) {
        this.ext = ext;
    }

    public static MergeChunksDTOBuilder builder() {
        return new MergeChunksDTOBuilder();
    }

    public static final class MergeChunksDTOBuilder {
        private String fileId;
        private String filePath;
        private Map<String, Object> ext;

        private MergeChunksDTOBuilder() {
        }

        public MergeChunksDTOBuilder fileId(String fileId) {
            this.fileId = fileId;
            return this;
        }

        public MergeChunksDTOBuilder filePath(String filePath) {
            this.filePath = filePath;
            return this;
        }

        public MergeChunksDTOBuilder ext(Map<String, Object> ext) {
            this.ext = ext;
            return this;
        }

        public MergeChunksDTO build() {
            MergeChunksDTO mergeChunksDTO = new MergeChunksDTO();
            mergeChunksDTO.setFileId(fileId);
            mergeChunksDTO.setFilePath(filePath);
            mergeChunksDTO.setExt(ext);
            return mergeChunksDTO;
        }
    }
}
