package cn.imatu.framework.file.upload.local.model;

import java.io.InputStream;

/**
 * @author shenguangyang
 */
public class UploadChunk {
    private InputStream inputStream;

    /**
     * 文件code, 只要保证不同的文件code不一样即可，eg: 可以是md5
     */
    private String fileCode;
    /**
     * 分片数
     */
    private Integer chunk;

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public String getFileCode() {
        return fileCode;
    }

    public void setFileCode(String fileCode) {
        this.fileCode = fileCode;
    }

    public Integer getChunk() {
        return chunk;
    }

    public void setChunk(Integer chunk) {
        this.chunk = chunk;
    }

    public static UploadFileBOBuilder builder() {
        return new UploadFileBOBuilder();
    }

    public static final class UploadFileBOBuilder {
        private InputStream inputStream;
        private String fileCode;
        private Integer chunk;

        private UploadFileBOBuilder() {
        }

        public UploadFileBOBuilder inputStream(InputStream inputStream) {
            this.inputStream = inputStream;
            return this;
        }

        public UploadFileBOBuilder fileCode(String fileCode) {
            this.fileCode = fileCode;
            return this;
        }

        public UploadFileBOBuilder chunk(Integer chunk) {
            this.chunk = chunk;
            return this;
        }

        public UploadChunk build() {
            UploadChunk uploadChunk = new UploadChunk();
            uploadChunk.setInputStream(inputStream);
            uploadChunk.setFileCode(fileCode);
            uploadChunk.setChunk(chunk);
            return uploadChunk;
        }
    }
}
