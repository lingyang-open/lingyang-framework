package cn.imatu.framework.file.upload.local.model;

/**
 * @author shenguangyang
 */
public class UploadChunkDTO {
    /**
     * 文件id, 对应数据库中的文件id
     */
    private String fileId;

    public static UploadFileDTOBuilder builder() {
        return new UploadFileDTOBuilder();
    }

    public static final class UploadFileDTOBuilder {
        private String fileId;

        private UploadFileDTOBuilder() {
        }

        public UploadFileDTOBuilder fileId(String fileId) {
            this.fileId = fileId;
            return this;
        }

        public UploadChunkDTO build() {
            UploadChunkDTO uploadFileDTO = new UploadChunkDTO();
            uploadFileDTO.fileId = this.fileId;
            return uploadFileDTO;
        }
    }
}
