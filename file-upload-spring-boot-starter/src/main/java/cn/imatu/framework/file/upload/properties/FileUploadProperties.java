package cn.imatu.framework.file.upload.properties;

import cn.imatu.framework.core.constant.LyCoreConstants;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.unit.DataSize;

/**
 * @author shenguangyang
 */
@Configuration
@ConfigurationProperties(prefix = LyCoreConstants.PROPERTIES_PRE + "file.upload")
public class FileUploadProperties {
    /**
     * 上传的文件最大值
     */
    private DataSize maxUploadSize = DataSize.ofGigabytes(1L);
    /**
     * 上传的分片大小, 需要和前端保持一致
     */
    private DataSize chunkSize = DataSize.ofMegabytes(10L);

    private String clientType;
    /**
     * 根目录
     */
    private String rootDir;
    /**
     * 存放已经将分片合并成一个文件目录, 即上传好的文件
     */
    private String mergeDir;

    /**
     * 断点续传中的断点(分片块)文件所在目录
     */
    private String chunkDir;

    public DataSize getMaxUploadSize() {
        return maxUploadSize;
    }

    public void setMaxUploadSize(DataSize maxUploadSize) {
        this.maxUploadSize = maxUploadSize;
    }

    public DataSize getChunkSize() {
        return chunkSize;
    }

    public void setChunkSize(DataSize chunkSize) {
        this.chunkSize = chunkSize;
    }

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    public String getRootDir() {
        return rootDir;
    }

    public void setRootDir(String rootDir) {
        this.rootDir = rootDir;
    }

    public String getMergeDir() {
        return mergeDir;
    }

    public void setMergeDir(String mergeDir) {
        this.mergeDir = mergeDir;
    }

    public String getChunkDir() {
        return chunkDir;
    }

    public void setChunkDir(String chunkDir) {
        this.chunkDir = chunkDir;
    }
}
