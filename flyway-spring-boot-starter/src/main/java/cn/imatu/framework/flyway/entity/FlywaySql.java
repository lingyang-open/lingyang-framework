package cn.imatu.framework.flyway.entity;

import lombok.Getter;
import lombok.Setter;

/**
 * @author shenguangyang
 */
@Getter
@Setter
public class FlywaySql extends Flyway {
    private String sql;
}
