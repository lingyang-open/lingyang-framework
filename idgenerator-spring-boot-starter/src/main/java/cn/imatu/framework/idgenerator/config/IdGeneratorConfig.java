package cn.imatu.framework.idgenerator.config;

import cn.imatu.framework.idgenerator.IdGeneratorHandler;
import com.github.yitter.contract.IdGeneratorOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

/**
 * Description: <a href="https://gitee.com/yitter/idgenerator/tree/master/Java">漂移雪花算法</a>
 * <a href="https://gitee.com/yitter/idgenerator/tree/master">文档地址</a>
 * <p>
 * 能用多久
 * 1. 在默认配置下，ID可用 71000 年不重复。
 * 2. 在支持 1024 个工作节点时，ID可用 4480 年不重复。
 * 3，在支持 4096 个工作节点时，ID可用 1120 年不重复。
 *
 * @author shenguangyang
 */
@Configuration
@EnableConfigurationProperties(IdGeneratorProperties.class)
public class IdGeneratorConfig {
    @Resource
    IdGeneratorProperties idGeneratorProperties;

    @Autowired(required = false)
    IdGeneratorHandler idGeneratorHandler;

    @Bean
    @ConditionalOnMissingBean(IdGeneratorOptions.class)
    public IdGeneratorOptions idGeneratorOptions() {
        // 创建 IdGeneratorOptions 对象，请在构造函数中输入 WorkerId：
        IdGeneratorOptions options = new IdGeneratorOptions();
        options.BaseTime = idGeneratorProperties.getBaseTime();
        options.MaxSeqNumber = idGeneratorProperties.getMaxSeqNumber();
        options.MinSeqNumber = idGeneratorProperties.getMinSeqNumber();
        options.SeqBitLength = idGeneratorProperties.getSeqBitLength();
        options.WorkerId = idGeneratorProperties.getWorkerId();
        options.WorkerIdBitLength = idGeneratorProperties.getWorkerIdBitLength();
        if (idGeneratorHandler != null) {
            options.WorkerId = idGeneratorHandler.getWorkerId();
        }
        return options;
    }
}
