package cn.imatu.framework.ip2region.config;

import cn.imatu.framework.ip2region.config.properties.LyIpProperties;
import com.maxmind.geoip2.DatabaseReader;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;

/**
 * @author shenguangyang
 */
@Configuration
@ConditionalOnClass(DatabaseReader.class)
public class Geoip2Config {
    @Resource
    private LyIpProperties ipProperties;

    @Bean(destroyMethod = "close")
    public DatabaseReader databaseReader() {
        try {
            return new DatabaseReader.Builder(new File(ipProperties.getDatabasePath())).build();
        } catch (IOException e) {
            System.out.println("IPDatabase error:" + e.getMessage());
            return null;
        }
    }
}
