package cn.imatu.framework.ip2region.config;

import cn.imatu.framework.core.constant.*;
import cn.imatu.framework.ip2region.constants.*;
import cn.imatu.framework.ip2region.handler.*;
import com.maxmind.geoip2.DatabaseReader;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author shenguangyang
 */
@Component
@ConditionalOnClass(DatabaseReader.class)
@ConditionalOnProperty(prefix = LyCoreConstants.PROPERTIES_PRE + "ip2region", value = "type", havingValue = IpOfflineType.GEOIP2)
public class LyGeoip2AutoConfiguration {
    @Resource
    private DatabaseReader reader;

    @Bean
    @ConditionalOnMissingBean
    public Ip2RegionHandler ip2RegionOfflineHandler() {
        return new Geoip2Impl(reader);
    }

}
