package cn.imatu.framework.ip2region.config.properties;

import cn.imatu.framework.core.constant.*;
import cn.imatu.framework.ip2region.constants.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.annotation.PostConstruct;

/**
 * @author shenguangyang
 */
@Getter
@Setter
@ConfigurationProperties(prefix = LyCoreConstants.PROPERTIES_PRE + "ip2region")
public class LyIpProperties {
    /**
     * 离线获取ip类型, 默认是通过 geoip2获取
     */
    private String type = IpOfflineType.GEOIP2;
    /**
     * 这里就是填写离线资源包的地址
     * <p>
     * 这里默认采用的是MaxMind GeoIP2
     * <a href="https://www.maxmind.com/en/home">官网</a> <br/>
     * <a href="https://dev.maxmind.com/geoip/geoip2/geolite2/#Downloads">离线资源包</a> <br/>
     */
    private String databasePath;

    /**
     * 第一次下载后会将文件存放到 {@link #databasePath} 路径下, 避免每次都下载
     */
    private String downloadUrl;

    @PostConstruct
    public void check() {
        if (!IpOfflineType.GEOIP2.equals(type) && !IpOfflineType.IP2REGION.equals(type)) {
            throw new RuntimeException("property [ ip2region.type ] value is not exist, " +
                "only support [ " + IpOfflineType.GEOIP2 + "|" + IpOfflineType.IP2REGION + " ]");
        }
    }
}
