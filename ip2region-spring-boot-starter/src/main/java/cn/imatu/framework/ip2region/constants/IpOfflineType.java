package cn.imatu.framework.ip2region.constants;

/**
 * @author shenguangyang
 */
public class IpOfflineType {
    public static final String GEOIP2 = "geoip2";
    public static final String IP2REGION = "ip2region";
}
