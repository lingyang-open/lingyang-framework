package cn.imatu.framework.ip2region.handler;

import cn.imatu.framework.ip2region.model.AddressInfo;

/**
 * @author shenguangyang
 */
public interface Ip2RegionHandler {
    /**
     * 获取地址
     */
    AddressInfo getAddress(String ipv4) throws Exception;
}
