package cn.imatu.framework.ip2region.model;

import lombok.*;

/**
 * @author shenguangyang
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AddressInfo {
    /**
     * 获取国家
     */
    private String country;
    /**
     * 省份
     */
    private String province;

    /**
     * 获取城市
     */
    private String city;

    /**
     * 区域, 比如 杭州的西湖区, geoip2不支持精确到区域
     */
    private String area;

    /**
     * 精度
     */
    private Double longitude;

    /**
     * 维度
     */
    private Double latitude;

    /**
     * 网络提供者, 联通/电信/移动
     */
    private String isp;

    public String defaultFormat() {
        if ("内网IP".equals(isp)) {
            return city;
        }
        return country + "-" + province + "-" + city;
    }
}
