package cn.imatu.framework.ip2region;

import cn.imatu.framework.ip2region.handler.*;
import cn.imatu.framework.ip2region.model.*;
import com.alibaba.fastjson2.JSON;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

/**
 * @author shenguangyang
 */
@SpringBootTest(classes = LyIp2regionAutoConfiguration.class)
public class Ip2RegionTest {
    @Resource
    private Ip2RegionHandler ip2RegionHandler;

    @Test
    public void test() throws Exception {
        AddressInfo address = ip2RegionHandler.getAddress("127.0.0.1");
        System.out.println(JSON.toJSONString(address));
    }
}
