package cn.imatu.framework.knife4j.micro.servlet;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import javax.annotation.Resource;

/**
 * Description:
 *
 * @author shenguangyang
 */
@Configuration
@EnableKnife4j
public class Knife4jConfig {
    @Value("${knife4j.enable:false}")
    private Boolean enable;

    @Resource
    private LyKnife4jApiInfoProperties lyKnife4JApiInfoProperties;

    @Bean //配置docket以配置Swagger具体参数
    public OpenAPI defaultOpenApi(Environment environment) {
        LyKnife4jApiInfoProperties properties = lyKnife4JApiInfoProperties;
        LyKnife4jApiInfoProperties.Contact contact = properties.getContact();
        return new OpenAPI()
            // 接口文档标题
            .info(new Info().title(properties.getTitle())
                // 接口文档简介
                .description(properties.getDescription())
                // 接口文档版本
                .version(properties.getVersion())
                .contact(
                    new Contact().name(contact.getName()).email(contact.getEmail()).url(contact.getUrl())
                ))
            .externalDocs(new ExternalDocumentation()
                .description(properties.getDescription())
                .url("http://127.0.0.1:8888"));

    }

//    /**
//     * 解决springboot 2.7.2 整合swagger 3, 启动报错问题
//     */
//    @Bean
//    public static BeanPostProcessor springfoxHandlerProviderBeanPostProcessor() {
//        return new BeanPostProcessor() {
//            @Override
//            public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
//                if (bean instanceof WebMvcRequestHandlerProvider) {
//                    customizeSpringfoxHandlerMappings(getHandlerMappings(bean));
//                }
//                return bean;
//            }
//
//            private <T extends RequestMappingInfoHandlerMapping> void customizeSpringfoxHandlerMappings(
//                    List<T> mappings) {
//                List<T> copy = mappings.stream().filter(mapping -> mapping.getPatternParser() == null)
//                        .collect(Collectors.toList());
//                mappings.clear();
//                mappings.addAll(copy);
//            }
//
//            @SuppressWarnings("unchecked")
//            private List<RequestMappingInfoHandlerMapping> getHandlerMappings(Object bean) {
//                try {
//                    Field field = ReflectionUtils.findField(bean.getClass(), "handlerMappings");
//                    assert field != null;
//                    field.setAccessible(true);
//                    return (List<RequestMappingInfoHandlerMapping>) field.get(bean);
//                } catch (IllegalArgumentException | IllegalAccessException e) {
//                    throw new IllegalStateException(e);
//                }
//            }
//        };
//    }
}
