package cn.imatu.framework.flyway.demo;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author shenguangyang
 */
@MapperScan(basePackages = "cn.imatu.framework.flyway.demo.mapper")
@SpringBootApplication(exclude = DruidDataSourceAutoConfigure.class)
public class FlywayDemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(FlywayDemoApplication.class, args);
    }
}
