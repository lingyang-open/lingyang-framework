package cn.imatu.framework.flyway.demo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author shenguangyang
 */
@Data
@TableName("test2")
public class Test2 {
    private Long accessKeyId;
    private Long userId;
    private String secretAccessKey;

    public static void main(String[] args) {

    }
}
