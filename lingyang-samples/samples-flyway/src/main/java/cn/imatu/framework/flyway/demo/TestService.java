package cn.imatu.framework.flyway.demo;

import cn.hutool.core.util.RandomUtil;
import cn.imatu.framework.exception.BizException;
import cn.imatu.framework.flyway.demo.mapper.Test1Mapper;
import cn.imatu.framework.flyway.demo.mapper.Test2Mapper;
import com.github.yitter.idgen.YitIdHelper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author shenguangyang
 */
@Service
public class TestService {
    @Resource
    private Test1Mapper test1Mapper;
    @Resource
    private Test2Mapper test2Mapper;


//    @Transactional(rollbackFor = Exception.class)
    public void save() {
        Test1 test1 = new Test1();
        test1.setUserId(RandomUtil.randomLong(1, 100));
        test1.setAccessKeyId(YitIdHelper.nextId());
        test1.setSecretAccessKey(RandomUtil.randomString(32));
        test1Mapper.insert(test1);

        Test2 test2 = new Test2();
        test2.setUserId(RandomUtil.randomLong(1, 100));
        test2.setAccessKeyId(YitIdHelper.nextId());
        test2.setSecretAccessKey(RandomUtil.randomString(32));
        test2Mapper.insert(test2);
        if (test1.getUserId() > 1) {
            throw new BizException("保存失败");
        }
    }
}
