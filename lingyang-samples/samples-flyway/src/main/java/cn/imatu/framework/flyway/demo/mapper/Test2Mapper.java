package cn.imatu.framework.flyway.demo.mapper;

import cn.imatu.framework.flyway.demo.Test2;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author shenguangyang
 */
public interface Test2Mapper extends BaseMapper<Test2> {
}
