-- ----------------------------
-- Table structure for sys_access_key
-- ----------------------------
CREATE TABLE IF NOT EXISTS `test2_1`  (
  `access_key_id` bigint(0) NOT NULL COMMENT '用于标识访问者的身份',
  `user_id` bigint(0) NOT NULL,
  `secret_access_key` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用于加密签名字符串和服务器端验证签名字符串的密钥，必须严格保密。',
  `status` tinyint(1) NULL DEFAULT NULL COMMENT '状态,1正常 , -1禁用',
  `create_by` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_by` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`access_key_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

INSERT INTO `test2_1`(`access_key_id`, `user_id`, `secret_access_key`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (538420375027717, 66, 'qccctu3spqa6ub9av19t9mjypktxzhxf', NULL, NULL, NULL, NULL, NULL, NULL);

CREATE TABLE IF NOT EXISTS `test2_2`  (
    `access_key_id` bigint(0) NOT NULL COMMENT '用于标识访问者的身份',
    `user_id` bigint(0) NOT NULL,
    `secret_access_key` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用于加密签名字符串和服务器端验证签名字符串的密钥，必须严格保密。',
    `status` tinyint(1) NULL DEFAULT NULL COMMENT '状态,1正常 , -1禁用',
    `create_by` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
    `create_time` datetime(0) NULL DEFAULT NULL,
    `update_by` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
    `update_time` datetime(0) NULL DEFAULT NULL,
    `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
    PRIMARY KEY (`access_key_id`) USING BTREE
    ) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;
