package cn.imatu.framework.samples;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author shenguangyang
 */
@FeignClient(url = "http://127.0.0.1:38080", value = "testLog")
public interface TestFeign {
    @GetMapping("/testLog/test2")
    String test2(@RequestParam("name") String name);
}
