package cn.imatu.framework.samples;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author shenguangyang
 */
@Slf4j
@SpringBootApplication()
public class TestTraceLogApplication {

    public static void main(String[] args) {
//        SpringApplication application = new SpringApplication(TestLog.class);
//        application.setWebApplicationType(WebApplicationType.NONE);
//        application.run(args);
        SpringApplication.run(TestTraceLogApplication.class, args);
    }
}
