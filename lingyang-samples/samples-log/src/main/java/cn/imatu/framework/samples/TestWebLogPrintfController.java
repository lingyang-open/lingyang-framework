package cn.imatu.framework.samples;

import cn.hutool.core.util.RandomUtil;
import cn.imatu.framework.core.response.Resp;
import cn.imatu.framework.exception.BizException;
import cn.imatu.framework.log.LogContext;
import cn.imatu.framework.log.annotation.LogEvent;
import cn.imatu.framework.log.annotation.OperateLog;
import com.alibaba.fastjson2.JSON;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Objects;

/**
 * @author shenguangyang
 */
@Slf4j
@RestController
@LogEvent(eventObject = "systemUser")
@RequestMapping("/testWebLog")
@EnableFeignClients
public class TestWebLogPrintfController {

    @Data
    public static class Test {
        private String k1;
        private String k2;
        private List<String> k3;
    }

    @GetMapping("/test01")
    public Resp<Object> test01(Test req) {
        return Resp.ok(req);
    }

    @OperateLog(eventType = "test02", content = "测试 [ {{ #req.k1 }} ]", isPrintOut = false, writeDb = true)
    @GetMapping("/test02")
    public Resp<Object> test02(Test req) {
        return Resp.ok(JSON.toJSONString(req));
    }

    @OperateLog(eventType = "testUpload", isPrintOut = false, writeDb = true)
    @PostMapping("/test03")
    public Resp<Object> test03(MultipartFile file) {
        return Resp.ok(RandomUtil.randomString(32));
    }

    @OperateLog(eventType = "test04", isPrintOut = false, writeDb = true)
    @GetMapping("/test04")
    public Resp<Object> test04(Test req) {
        if (Objects.nonNull(req)) {
            throw new BizException("testset");
        }
        return Resp.ok(req);
    }

    @OperateLog(eventType = "test05", content = "{{ #ctx_name }}")
    @GetMapping("/test05")
    public Resp<Object> test05(Test req) {
        LogContext.addParams("name", "测试一下");
        return Resp.ok(req);
    }
}
