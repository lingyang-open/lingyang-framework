package cn.imatu.framework.demo.controller;

import cn.imatu.framework.security.submit.RepeatSubmit;
import com.alibaba.fastjson2.JSON;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Data;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author shenguangyang
 */
@Tag(name = "test")
@RestController
public class TestController {
    @Resource
    private StringRedisTemplate stringRedisTemplate;
    @Operation(summary = "t1")
    @GetMapping("/t1")
    public String t1(){
        return "ok";
    }

    @Data
    public static class Test {
        private String userName;
        private Integer age;
    }

    @Operation(summary = "t2")
    @GetMapping("/t2")
    @RepeatSubmit(type = RepeatSubmit.Type.TOKEN)
    public String t2(@RequestParam("id") String id, Test req){
//        stringRedisTemplate.opsForValue().bitField()
        System.out.println("info: " + JSON.toJSONString(req));
        System.out.println("id: " + id);
        return "ok";
    }
}
