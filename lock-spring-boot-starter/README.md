# 简介

这是一个整合多个分布式锁框架组件, 并对外暴露统一接口, 从而实现无缝切换所支持的分布式锁框架

# 支持哪些分布式锁框架

- redisson
- zookeeper(Curator)
- spring-integration-redis

# 使用redisson实现分布式锁

pom依赖

```xml
<parent>
    <groupId>cn.imatu.framework</groupId>
    <artifactId>lingyang-dependencies</artifactId>
    <version>1.0.0</version>
</parent>

<dependencies>
    <dependency>
        <groupId>cn.imatu.framework</groupId>
        <artifactId>lock-spring-boot-starter</artifactId>
    </dependency>
    <dependency>
        <groupId>cn.imatu.framework</groupId>
        <artifactId>redis-spring-boot-starter</artifactId>
    </dependency>
    <dependency>
        <groupId>org.redisson</groupId>
        <artifactId>redisson-spring-boot-starter</artifactId>
    </dependency>
</dependencies>
```

yaml配置

```yaml
spring:
  #reids配置
  # Redis数据库索引（默认为0）
  redis:
    database: 0
    # Redis服务器地址
    host: work01
    # Redis服务器连接端口
    port: 56379
    # Redis服务器连接密码（默认为空）
    password: project_dev@nwt
    lettuce:
      pool:
        #连接池最大连接数（使用负值表示没有限制）
        max-active: 100
        # 连接池最大阻塞等待时间（使用负值表示没有限制）
        max-wait: -1ms
        # 连接池中的最大空闲连接
        max-idle: 8
        # 连接池中的最小空闲连接
        min-idle: 0
```

使用方式

```java
@Service
public class GoodService {
    private static final String REDIS_LOCK = "redis_lock";
    
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Resource
    private ILockService lockService;
    
    /**
     * 测试 LockService 服务实现的分布式锁功能,
     */
    @SuppressWarnings("unchecked")
    public void buyGoodsByLockService() {
        ILock lock = lockService.getLock(REDIS_LOCK);
        lock.lock(500, TimeUnit.MILLISECONDS);
        try {
            // 处理业务
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            lock.unlock();
        }
    }
}
```

# 使用zookeeper实现分布式锁

pom

```xml
<parent>
    <groupId>cn.imatu.framework</groupId>
    <artifactId>lingyang-dependencies</artifactId>
    <version>1.0.0</version>
</parent>

<dependencies>
    <dependency>
        <groupId>cn.imatu.framework</groupId>
        <artifactId>lock-spring-boot-starter</artifactId>
    </dependency>
    <dependency>
        <groupId>org.apache.curator</groupId>
        <artifactId>curator-framework</artifactId>
    </dependency>
    <dependency>
        <groupId>org.apache.curator</groupId>
        <artifactId>curator-recipes</artifactId>
    </dependency>
    <dependency>
        <groupId>org.apache.curator</groupId>
        <artifactId>curator-client</artifactId>
    </dependency>
</dependencies>
```

yaml配置文件

```yaml
curator:
  # 如果你在部署zookeeper集群的时候, 通过容器名访问zookeeper集群中的其他节点时,
  # 比如你配置了 ZOO_SERVERS: server.1=0.0.0.0:2888:3888;2181 server.2=zookeeper02:2888:3888;2181 server.3=zookeeper03:2888:3888;2181,
  # 由于curator会获取到这个列表到客户端然后进行访问, 所以需要你本地配置 zookeeper03 zookeeper02 zookeeper01对应的host解析, 否则会出现连接异常的错误
  # host如下
  # 192.168.116.131 zookeeper01
  # 192.168.116.131 zookeeper02
  # 192.168.116.131 zookeeper03
  # address: 192.168.116.131:2181,192.168.116.131:2182,192.168.116.131:2183
  address: work01:2181
  session-timeout: 2000ms
  connection-timeout: 2000ms
  retry-policy:
    base-sleep-time: 2000ms
    max-retries: 3
```

使用方法

```java
@Service
public class TestLock {
    @Resource
    private ILockService lockService;

    private int goodsStock = 100;

    public void testLockService() throws InterruptedException {
        ILock lock = lockService.getLock("/lock");
        try {
            lock.lock();
            if (goodsStock == 0) {
                System.err.println("商品库存不足");
                return;
            }
            goodsStock = goodsStock - 1;
            System.out.println("buy goods, stock: " + goodsStock);
            TimeUnit.MILLISECONDS.sleep(RandomUtil.randomInt(1, 100));

            // 再次获取锁, 可重入锁
            lock.lock();
            TimeUnit.MILLISECONDS.sleep(RandomUtil.randomInt(1, 50));
            lock.unlock();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } finally {
            lock.unlock();
        }
    }
}
```

测试

```java
@SpringBootTest
public class LockTest {
    @Resource
    private TestLock testLock;

    @Test
    public void test() throws InterruptedException {
        for (int i = 0; i < 20; i++) {
            new Thread(() -> {
                try {
                    testLock.testLockService();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }).start();
        }

        TimeUnit.SECONDS.sleep(60);
    }
}
```

# 更多使用方式

https://www.yuque.com/shenguangyang/rhoz1v/zgo6of


