package cn.imatu.framework.lock;

import cn.imatu.framework.core.constant.LyCoreConstants;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.time.Duration;

/**
 * @author shenguangyang
 */
@Getter
@Setter
@ToString
@ConfigurationProperties(prefix = LyCoreConstants.PROPERTIES_PRE + "curator")
public class CuratorProperties {
    private boolean enable;

    /**
     * 连接的地址, 可指定多台服务地址 127.0.0.1:2181,127.0.0.1:2182,127.0.0.1:2183
     * <p>
     * 如果你在部署zookeeper集群的时候, 通过容器名访问zookeeper集群中的其他节点时,
     * 比如你配置了 ZOO_SERVERS: server.1=0.0.0.0:2888:3888;2181 server.2=zookeeper02:2888:3888;2181 server.3=zookeeper03:2888:3888;2181,
     * 由于curator会获取到这个列表到客户端然后进行访问, 所以需要你本地配置 zookeeper03 zookeeper02 zookeeper01对应的host解析, 否则会出现连接异常的错误
     * host如下
     * 192.168.116.131 zookeeper01
     * 192.168.116.131 zookeeper02
     * 192.168.116.131 zookeeper03
     */
    private String address = "127.0.0.1:2181";

    private Duration sessionTimeout = Duration.ofMillis(2000);

    private Duration connectionTimeout = Duration.ofMillis(2000);

    /**
     * 重试策略
     */
    private RetryPolicy retryPolicy = new RetryPolicy();

    @Getter
    @Setter
    public static class RetryPolicy {
        Duration baseSleepTime = Duration.ofMillis(2000);
        int maxRetries = 3;
    }
}
