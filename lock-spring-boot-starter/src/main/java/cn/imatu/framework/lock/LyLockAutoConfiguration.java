package cn.imatu.framework.lock;

import cn.imatu.framework.lock.aspect.DistributedLockAspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.context.annotation.Import;

import javax.annotation.PostConstruct;

/**
 * @author shenguangyang
 */
@AutoConfiguration
@Import({DistributedLockAspect.class})
public class LyLockAutoConfiguration {
    private static final Logger log = LoggerFactory.getLogger(LyLockAutoConfiguration.class);

    @PostConstruct
    public void init() {
        log.info("init {}", this.getClass().getName());
    }
}
