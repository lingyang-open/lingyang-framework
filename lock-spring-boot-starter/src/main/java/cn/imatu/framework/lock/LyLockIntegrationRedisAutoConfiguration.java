package cn.imatu.framework.lock;

import cn.imatu.framework.lock.constant.LockBeanName;
import cn.imatu.framework.lock.manager.IntegrationRedisManager;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.integration.redis.util.RedisLockRegistry;

/**
 * @author shenguangyang
 */
@AutoConfiguration
@ConditionalOnClass(RedisLockRegistry.class)
public class LyLockIntegrationRedisAutoConfiguration {

    @Bean(LockBeanName.INTEGRATION_REDIS_SERVICE)
    @ConditionalOnMissingBean(IntegrationRedisManager.class)
    public IntegrationRedisManager integrationRedisService(RedisLockRegistry redisLockRegistry) {
        return new IntegrationRedisManager(redisLockRegistry);
    }

    /**
     * 分布式锁
     *
     * @param redisConnectionFactory 连接工厂
     */
    @Bean(destroyMethod = "destroy")
    public RedisLockRegistry redisLockRegistry(RedisConnectionFactory redisConnectionFactory) {
        return new RedisLockRegistry(redisConnectionFactory, "lock");
    }
}
