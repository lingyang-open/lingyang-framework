package cn.imatu.framework.lock;

import cn.imatu.framework.lock.constant.LockBeanName;
import cn.imatu.framework.lock.manager.RedissonManager;
import org.redisson.Redisson;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;

import javax.annotation.Resource;

/**
 * @author shenguangyang
 */
@AutoConfiguration
@ConditionalOnClass(Redisson.class)
public class LyLockRedissonAutoConfiguration {

    @Resource
    private Redisson redisson;

    @Bean(name = LockBeanName.REDISSON_SERVICE)
    @ConditionalOnMissingBean(RedissonManager.class)
    public RedissonManager redissonService() {
        return new RedissonManager(redisson);
    }
}
