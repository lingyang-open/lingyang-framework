package cn.imatu.framework.lock.annotation;

import cn.imatu.framework.lock.constant.LockBeanName;
import cn.imatu.framework.lock.exception.LockException;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

/**
 * 用于标记分布式锁
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DistributedLock {
    /**
     * 指定锁服务bean名称, 所有支持的分布式锁服务bean, 在 {@link LockBeanName} 类中
     * 注意, 如果你使用redisson或其他分布式锁, 你必须在你的工程中引入当前工程依赖以及redisson
     * 或其他分布式锁(redisson/curator/spring-integration-redis)相关依赖
     */
    String beanName() default LockBeanName.REDISSON_SERVICE;

    /**
     * 获取锁失败抛出的 {@link LockException} 异常, 指定的 lockFailMessage 和 lockFailCode
     * 会被传入到该异常类中
     */
    String lockFailMessage() default "服务繁忙, 请稍后再试!!!";

    int lockFailCode() default 400;

    /**
     * 锁的key值, 可使用SpEL传方法参数
     *
     * @return key
     */
    String lockKey() default "defaultLock";

    /**
     * 尝试获取锁等待时间, 通过 unit 指定单位, 默认是毫秒
     */
    long waitTime() default 500;

    TimeUnit unit() default TimeUnit.MILLISECONDS;
}
