package cn.imatu.framework.lock.constant;

/**
 * 锁的 bean 名字
 *
 * @author shenguangyang
 */
public class LockBeanName {
    public static final String INTEGRATION_REDIS_SERVICE = "integrationRedisService";
    public static final String CURATOR_SERVICE = "curatorService";
    public static final String REDISSON_SERVICE = "redissonService";
}
