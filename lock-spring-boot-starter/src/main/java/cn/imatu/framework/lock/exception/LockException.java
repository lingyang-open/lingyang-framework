package cn.imatu.framework.lock.exception;

import cn.imatu.framework.exception.BaseException;

/**
 * @author shenguangyang
 */
public class LockException extends BaseException {
    public LockException(String message) {
        super(message);
    }

    public LockException(Integer code, String message) {
        super(code, message);
    }
}
