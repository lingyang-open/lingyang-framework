package cn.imatu.framework.lock.manager;

import cn.imatu.framework.lock.model.CuratorLock;
import cn.imatu.framework.lock.model.ILock;
import org.apache.curator.framework.CuratorFramework;

/**
 * @author shenguangyang
 */
public class CuratorManager implements ILockManager {
    private final CuratorFramework curatorFramework;

    public CuratorManager(CuratorFramework curatorFramework) {
        this.curatorFramework = curatorFramework;
    }

    @Override
    public ILock getLock(String lockKey) {
        return new CuratorLock(lockKey, curatorFramework);
    }
}
