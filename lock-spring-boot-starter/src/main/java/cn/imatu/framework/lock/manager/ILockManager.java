package cn.imatu.framework.lock.manager;

import cn.imatu.framework.lock.constant.LockBeanName;
import cn.imatu.framework.lock.model.ILock;

/**
 * 分布式锁服务, 支持redis, zookeeper 以及 spring-integration-redis的分布式锁
 *
 * @see LockBeanName
 * @author shenguangyang
 */
public interface ILockManager {
    ILock getLock(String lockKey);
}
