package cn.imatu.framework.lock.manager;

import cn.imatu.framework.lock.model.ILock;
import cn.imatu.framework.lock.model.IntegrationRedisLock;
import org.springframework.integration.redis.util.RedisLockRegistry;

/**
 * @author shenguangyang
 */
public class IntegrationRedisManager implements ILockManager {

    private final RedisLockRegistry redisLockRegistry;

    public IntegrationRedisManager(RedisLockRegistry redisLockRegistry) {
        this.redisLockRegistry = redisLockRegistry;
    }

    @Override
    public ILock getLock(String lockKey) {
        return new IntegrationRedisLock(lockKey, redisLockRegistry);
    }
}
