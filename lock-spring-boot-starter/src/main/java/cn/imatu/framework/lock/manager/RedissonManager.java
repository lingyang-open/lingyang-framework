package cn.imatu.framework.lock.manager;

import cn.imatu.framework.lock.model.ILock;
import cn.imatu.framework.lock.model.RedissonLock;
import org.redisson.Redisson;
import org.springframework.context.annotation.Primary;

/**
 * @author shenguangyang
 */
@Primary
public class RedissonManager implements ILockManager {

    private final Redisson redisson;

    public RedissonManager(Redisson redisson) {
        this.redisson = redisson;
    }

    @Override
    public ILock getLock(String lockKey) {
        return new RedissonLock(lockKey, redisson);
    }
}
