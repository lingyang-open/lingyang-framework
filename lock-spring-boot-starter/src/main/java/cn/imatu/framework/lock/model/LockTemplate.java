package cn.imatu.framework.lock.model;

import cn.imatu.framework.tool.core.exception.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 锁的模板类
 *
 * @author shenguangyang
 */
public abstract class LockTemplate implements ILock {
    private static final Logger log = LoggerFactory.getLogger(LockTemplate.class);
    protected int maxTryLockCount = 2;

    /**
     * 锁的key
     */
    protected String lockKey;

    public LockTemplate(String lockKey) {
        Assert.notEmpty(lockKey, "lockKey is empty");
        this.lockKey = lockKey;
    }
}
