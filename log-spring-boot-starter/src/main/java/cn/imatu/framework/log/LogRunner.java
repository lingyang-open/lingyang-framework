package cn.imatu.framework.log;

import cn.imatu.framework.log.utils.LogUtils;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

import javax.annotation.Resource;

/**
 * 保证日志过滤启动成功后再设置, 避免启动报错, 一些框架堆栈信息被过滤了
 * @author shenguangyang
 */
@Order(value = Ordered.LOWEST_PRECEDENCE - 5)
public class LogRunner implements ApplicationRunner {
    @Resource
    private LyLogProperties logProperties;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        // 设置过滤的堆栈日志数据
        LogUtils.init(logProperties);
    }
}
