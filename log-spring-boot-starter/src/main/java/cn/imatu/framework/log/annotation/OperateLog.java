package cn.imatu.framework.log.annotation;

import java.lang.annotation.*;

/**
 * 自定义操作日志记录注解
 *
 * @author shenguangyang
 */
@Target({ElementType.PARAMETER, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface OperateLog {
    /**
     * 事件内容, 支持 spel 表达式
     *
     * 在 {{  }} 中的内容，会被当作 SpEL 表达式进行解析
     *
     * eg: 测试 [ {{ #req.k1 }} ] , 解析结果就是 测试 [ xxx ]
     */
    String content() default "";

    /**
     * 事件类型, eg: updateUser, deleteUser
     */
    String eventType() default "";

    /**
     * 事件类型名称
     */
    String name() default "";

    /**
     * 操作人类别
     */
    String oprType() default "";

    /**
     * 是否打印请求的参数
     */
    boolean isPrintIn() default true;

    /**
     * 是否打印响应数据
     */
    boolean isPrintOut() default true;

    /**
     * 是否保存到数据库中
     */
    boolean writeDb() default true;
}
