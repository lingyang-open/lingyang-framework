package cn.imatu.framework.log.event;

import cn.imatu.framework.log.*;
import org.springframework.context.ApplicationEvent;

/**
 * 请求日志事件
 *
 * @author shenguangyang
 */
public class WebLogEvent extends ApplicationEvent {
    public WebLogEvent(WebLogInfo source) {
        super(source);
    }
}
