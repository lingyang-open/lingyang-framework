<?xml version="1.0" encoding="UTF-8"?>

<!-- 日志级别从低到高分为TRACE < DEBUG < INFO < WARN < ERROR < FATAL，如果设置为WARN，则低于WARN的信息都不会输出 -->
<!--
   http://logback.qos.ch/manual/layouts.html
   %p:输出优先级，即DEBUG,INFO,WARN,ERROR,FATAL
   %r:输出自应用启动到输出该日志讯息所耗费的毫秒数
   %t:输出产生该日志事件的线程名
   %f:输出日志讯息所属的类别的类别名
   %c:输出日志讯息所属的类的全名
   %d:输出日志时间点的日期或时间，指定格式的方式： %d{yyyy-MM-dd HH:mm:ss}
   %l:输出日志事件的发生位置，即输出日志讯息的语句在他所在类别的第几行。
   %m:输出代码中指定的讯息，如log(message)中的message
   %n:输出一个换行符号
-->
<configuration>
    <!-- 引用Spring Boot 默认日志配置 -->
    <include resource="org/springframework/boot/logging/logback/defaults.xml"/>

    <springProperty name="LOG_PATH" source="logging.file.path" defaultValue="logs/"/>
    <springProperty name="APP_NAME" scope="context" source="spring.application.name" defaultValue="app"/>
    <springProperty name="MAX_HISTORY" source="logging.logback.rollingpolicy.max-history" defaultValue="7"/>
    <springProperty name="MAX_FILE_SIZE" source="logging.logback.rollingpolicy.max-file-size" defaultValue="50MB"/>

    <conversionRule conversionWord="ex"
                    converterClass="cn.imatu.framework.log.CompressedStackTraceConverter"/>

    <!--
    格式化输出：%d表示日期，%-5level：日志级别从左显示5个字符宽度(%5p),%thread表示线程名(%t),
    %logger:%L 类的全路径及行号(%c:%L),%msg：日志消息，%n是换行符
    %ex: 打印堆栈时转换器
    -->
    <property name="CONSOLE_LOG_PATTERN"
              value="%ex%d{yyyy-MM-dd HH:mm:ss.SSS} %highlight(%-5level) [%yellow(%-0.16X{X-LoginId:--}),%yellow(%X{traceId:--})] [%yellow(%14.14t)] %green(%-40.40logger{39} %L:) %msg%n"/>
    <property name="CONSOLE_LOG_CHARSET" value="${CONSOLE_LOG_CHARSET:-${file.encoding:-UTF-8}}"/>

    <!-- 往文件中输出, 不能带有颜色标志, 否则会出现乱码 -->
    <property name="FILE_LOG_PATTERN"
              value="%ex%d{yyyy-MM-dd HH:mm:ss.SSS} %-5level [%-0.16X{X-LoginId:--},%X{traceId:--}] [%14.14t] %-40.40logger{39} %L: %msg%n"/>
    <property name="FILE_LOG_CHARSET" value="${FILE_LOG_CHARSET:-${file.encoding:-UTF-8}}"/>

    <!--	<property name="CONSOLE_LOG_PATTERN"-->
    <!--			  value="%d{yyyy-MM-dd HH:mm:ss.SSS} %highlight(%-5level) %green(%-40.40logger{39} %L:) %msg%n"/>-->
    <!--	&lt;!&ndash; 往文件中输出, 不能带有颜色标志, 否则会出现乱码 &ndash;&gt;-->
    <!--	<property name="FILE_LOG_PATTERN"-->
    <!--			  value="%d{yyyy-MM-dd HH:mm:ss.SSS} %-5level %-40.40logger{39}:%L :%msg%n"/>-->

    <!-- DEBUG日志 输入到文件，按日期,RollingFileAppender代表滚动记录文件 -->
    <appender name="LOGOUT_FILE" class="ch.qos.logback.core.rolling.RollingFileAppender">
        <file>${LOG_PATH}/${APP_NAME}.log</file>
        <!--日志文件滚动策略-->
        <rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
            <!-- 按天回滚 daily -->
            <fileNamePattern>${LOG_PATH}/history/${APP_NAME}-%d{yyyy-MM-dd}.%i.log.gz</fileNamePattern>
            <!-- 日志最大的历史 30天 -->
            <maxHistory>${MAX_HISTORY}</maxHistory>
            <timeBasedFileNamingAndTriggeringPolicy class="ch.qos.logback.core.rolling.SizeAndTimeBasedFNATP">
                <!-- maxFileSize:这是活动文件的大小，默认值是10MB，这里设置为50MB -->
                <maxFileSize>${MAX_FILE_SIZE}</maxFileSize>
            </timeBasedFileNamingAndTriggeringPolicy>
        </rollingPolicy>

        <append>true</append>
        <encoder class="ch.qos.logback.classic.encoder.PatternLayoutEncoder">
            <!--日志输出格式化-->
            <pattern>${FILE_LOG_PATTERN}</pattern>
            <charset>${FILE_LOG_CHARSET}</charset>
        </encoder>
    </appender>


    <!-- rocketmq日志 -->
    <appender name="RocketmqClientAppender" class="ch.qos.logback.core.rolling.RollingFileAppender">
        <file>${LOG_PATH}/rocketmq_client.log</file>

        <!--日志文件滚动策略-->
        <rollingPolicy class="ch.qos.logback.core.rolling.TimeBasedRollingPolicy">
            <!-- 按天回滚 daily -->
            <fileNamePattern>${LOG_PATH}/history/rocketmq_client-%d{yyyy-MM-dd}.%i.log.gz</fileNamePattern>
            <!-- 日志最大的历史 30天 -->
            <maxHistory>${MAX_HISTORY}</maxHistory>
            <timeBasedFileNamingAndTriggeringPolicy class="ch.qos.logback.core.rolling.SizeAndTimeBasedFNATP">
                <!-- maxFileSize:这是活动文件的大小，默认值是10MB，这里设置为50MB -->
                <maxFileSize>${MAX_FILE_SIZE}</maxFileSize>
            </timeBasedFileNamingAndTriggeringPolicy>
        </rollingPolicy>


        <encoder charset="UTF-8">
            <pattern>%d{yy-MM-dd.HH:mm:ss.SSS} [%-16t] %-5p %-22c{0} %X{ServiceId} - %m%n</pattern>
        </encoder>
    </appender>
    <logger name="RocketmqClient" additivity="false">
        <level value="warn"/>
        <appender-ref ref="RocketmqClientAppender"/>
    </logger>

    <!-- 控制台打印 -->
    <appender name="LOGOUT_CONSOLE" class="ch.qos.logback.core.ConsoleAppender">
        <encoder>
            <pattern>${CONSOLE_LOG_PATTERN}</pattern>
            <charset>${CONSOLE_LOG_CHARSET}</charset>
        </encoder>
        <filter class="ch.qos.logback.classic.filter.ThresholdFilter">
            <level>debug</level>
        </filter>
    </appender>

    <logger name="org.apache.catalina.startup.DigesterFactory" level="ERROR"/>
    <logger name="org.apache.catalina.util.LifecycleBase" level="ERROR"/>
    <logger name="org.apache.coyote.http11.Http11NioProtocol" level="WARN"/>
    <logger name="org.apache.sshd.common.util.SecurityUtils" level="WARN"/>
    <logger name="org.apache.tomcat.util.net.NioSelectorPool" level="WARN"/>
    <logger name="org.eclipse.jetty.util.component.AbstractLifeCycle" level="ERROR"/>
    <logger name="org.hibernate.validator.internal.util.Version" level="WARN"/>
    <logger name="org.springframework.boot.actuate.endpoint.jmx" level="WARN"/>
    <logger name="org.springframework.boot.actuate.endpoint.jmx" level="WARN"/>
    <logger name="org.springframework.security" level="WARN"/>
    <logger name="org.springframework.security.oauth2" level="WARN"/>

    <!-- 控制台和日志文件输出级别,此处表示把>=INFO级别的日志都输出到下列配置文件,它是根log,是所有<log>的上级-->
    <!-- 日志级别排序为： TRACE < DEBUG < INFO < WARN < ERROR -->
    <root level="INFO">
        <appender-ref ref="LOGOUT_FILE"/>
        <appender-ref ref="LOGOUT_CONSOLE"/>
    </root>
</configuration>
