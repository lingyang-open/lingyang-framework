package cn.imatu.framework.mock.data;

import cn.imatu.framework.mock.data.generator.MockDataRule;
import cn.imatu.framework.mock.data.generator.MockHelper;
import cn.imatu.framework.mock.data.generator.impl.DefaultMockDataRuleImpl;
import cn.imatu.framework.mock.data.generator.impl.DefaultTableMockDataGenerator;
import cn.imatu.framework.mock.data.manager.impl.GenMysqlDataManagerImpl;
import cn.imatu.framework.mock.data.manager.impl.MockDbDataManagerImpl;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

import javax.annotation.PostConstruct;

/**
 * @author shenguangyang
 */
@Slf4j
@Import({
        MockDbDataManagerImpl.class, GenMysqlDataManagerImpl.class, DefaultTableMockDataGenerator.class,
        MockHelper.class
})
@MapperScan(basePackages = "cn.imatu.framework.mock.data.mapper")
@AutoConfiguration
public class LyMockDataAutoConfiguration {

    @PostConstruct
    public void init() {
        log.info("init {}", this.getClass().getName());
    }

    @Bean
    @ConditionalOnMissingBean(MockDataRule.class)
    public MockDataRule mockDataRule() {
        return new DefaultMockDataRuleImpl();
    }
}
