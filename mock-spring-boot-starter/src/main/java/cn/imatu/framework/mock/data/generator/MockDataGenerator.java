package cn.imatu.framework.mock.data.generator;

import cn.imatu.framework.mock.data.model.MysqlTableColumn;

import java.util.Map;
import java.util.function.Function;

/**
 * 数据生成器
 *
 * @author shenguangyang
 */
public interface MockDataGenerator {
    /**
     * 指定表名
     */
    String tableName();

    /**
     * @return value: 返回数据
     */
    Map<String, Function<MysqlTableColumn, Object>> data();
}
