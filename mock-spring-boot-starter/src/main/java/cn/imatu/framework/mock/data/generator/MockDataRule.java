package cn.imatu.framework.mock.data.generator;

import cn.imatu.framework.mock.data.utils.MockUtils;

import java.util.Map;
import java.util.function.Supplier;

/**
 * mock数据规则, 你可以指定字段描述中包含哪些词语然后选择合适的生成器
 *
 * @author shenguangyang
 */
public interface MockDataRule {
    /**
     * 规则数据
     *
     * @return key: 可以指定 {@link MockUtils} 中的任意方法,
     * value: 行描述或者字段名包含的词组, 如果包含就你执行的方法生成mock数据
     */
    Map<Supplier<String>, String[]> ruleData();
}
