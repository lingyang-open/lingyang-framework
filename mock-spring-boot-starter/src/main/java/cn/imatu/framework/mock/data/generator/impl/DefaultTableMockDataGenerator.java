package cn.imatu.framework.mock.data.generator.impl;

import cn.imatu.framework.mock.data.generator.MockDataGenerator;
import cn.imatu.framework.mock.data.model.MysqlTableColumn;
import cn.hutool.core.util.RandomUtil;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * @author shenguangyang
 */
public class DefaultTableMockDataGenerator implements MockDataGenerator {
    @Override
    public String tableName() {
        return "test";
    }

    @Override
    public Map<String, Function<MysqlTableColumn, Object>> data() {
        Map<String, Function<MysqlTableColumn, Object>> resp = new HashMap<>();
        resp.put("age", t -> RandomUtil.randomInt(2));
        resp.put("id_card", t -> RandomUtil.randomInt(1, 20));
        return resp;
    }
}
