package cn.imatu.framework.mock.data.manager;

import cn.imatu.framework.mock.data.generator.MockDataGenerator;
import cn.imatu.framework.mock.data.model.MysqlTableColumn;

import java.util.List;
import java.util.Map;

/**
 * @author shenguangyang
 */
public interface GenMockDataManager {
    Map<String, Object> create(MockDataGenerator mockDataGenerator, List<MysqlTableColumn> tableColumnList);
}
