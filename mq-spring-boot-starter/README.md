# 支持使能指定mq

实际项目中, 有的项目是可能是一种服务类型, 类似中间件, 为了应对用户不同的需求, 可能会引入
kafka, rabbitmq, rocketmq等消息队列, 这时候你可以通过配置文件或者注解来使能指定mq

比如我想使能rocketmq中间件

```yaml
mq:
  type: ROCKETMQ
```

或者在启动类上使用注解

```java
@EnableRocketmq()
```

如果配置文件和启动类上同时进行了配置, 则以配置文件为主


