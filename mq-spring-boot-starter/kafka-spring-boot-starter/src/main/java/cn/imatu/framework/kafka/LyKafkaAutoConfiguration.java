package cn.imatu.framework.kafka;

import cn.imatu.framework.kafka.config.KafkaConfig;
import cn.imatu.framework.kafka.enable.EnableKafka;
import cn.imatu.framework.kafka.enable.EnableKafkaBeanDefinitionRegistryPostProcessor;
import cn.imatu.framework.kafka.enable.EnableKafkaRegistrar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.context.annotation.Import;

import javax.annotation.PostConstruct;

/**
 * @author shenguangyang
 */
@EnableKafka(value = false)
@Import({EnableKafkaBeanDefinitionRegistryPostProcessor.class, EnableKafkaRegistrar.class})
@ImportAutoConfiguration({KafkaConfig.class})
public class LyKafkaAutoConfiguration {
    private static final Logger log = LoggerFactory.getLogger(LyKafkaAutoConfiguration.class);

    @PostConstruct
    public void init() {
        log.info("init {}", this.getClass().getName());
    }
}
