package cn.imatu.framework.mq.base.enums;

import cn.imatu.framework.cache.core.key.ICacheKey;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.concurrent.TimeUnit;

/**
 * @author shenguangyang
 */
@Getter
@RequiredArgsConstructor
public enum MqCacheHashKey implements ICacheKey {
    /**
     * hash: 消息id
     */
    FAIL_MESSAGE("mq::fail_message", "%s", 60 * 60, TimeUnit.MINUTES);
    private final String key;
    private final String hashKey;
    private final int expire;
    private final TimeUnit unit;
}
