package cn.imatu.framework.mq.base.manager;

import cn.imatu.framework.cache.core.key.ICacheKey;
import cn.imatu.framework.cache.core.service.CacheService;
import cn.imatu.framework.mq.base.enums.MqCacheKey;

import javax.annotation.Resource;

/**
 * @author shenguangyang
 * @since 1.1.0
 */
public class MqManager {
    @Resource
    private CacheService cacheService;

    /**
     * 是否被消费
     * @param msgId 消息唯一值
     */
    public boolean isConsumed(String msgId) {
        MqCacheKey mqCacheKey = MqCacheKey.REPEAT_CONSUME;
        String key = mqCacheKey.formatKey(msgId);
        return cacheService.opsForValue().get(key) != null;
    }

    /**
     * 标记被消费
     * @param msgId 消息唯一值
     */
    public void markConsumed(String msgId) {
        MqCacheKey mqCacheKey = MqCacheKey.REPEAT_CONSUME;
        cacheService.opsForValue().set("1", mqCacheKey, msgId);
    }

    /**
     * 是否被消费
     * @param msgId 消息唯一值
     */
    public boolean isConsumed(String msgId, ICacheKey cacheKey) {
        String key = cacheKey.formatKey(msgId);
        return cacheService.opsForValue().get(key) != null;
    }

    /**
     * 标记被消费
     * @param msgId 消息唯一值
     */
    public void markConsumed(String msgId, ICacheKey cacheKey) {
        cacheService.opsForValue().set("1", cacheKey, msgId);
    }
}
