package cn.imatu.framework.mq.base.register;

import cn.imatu.framework.mq.base.domain.MqEnable;
import cn.imatu.framework.mq.base.enums.MqTypeEnum;
import cn.imatu.framework.tool.core.StringUtils;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotationMetadata;

import java.util.Locale;

/**
 * mq属性获取, 因为需要在项目启动时候就得知道配置文件中使能的是哪个mq类型
 * 好做后续处理
 *
 * @author shenguangyang
 */
public class MqPropertiesRegister implements ImportBeanDefinitionRegistrar, EnvironmentAware {
    private Environment environment;

    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
        try {
            String mqType = environment.getProperty("mq.type");
            if (StringUtils.isNull(mqType)) {
                return;
            }
            for (MqTypeEnum mqTypeEnum : MqTypeEnum.values()) {
                if (mqTypeEnum.name().equals(mqType.toUpperCase(Locale.ROOT))) {
                    MqEnable.addEnableMq(mqTypeEnum);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }
}
