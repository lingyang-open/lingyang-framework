package cn.imatu.framework.mqtt;

import cn.imatu.framework.mqtt.config.MqttConsumerConfig;
import cn.imatu.framework.mqtt.config.MqttProducerConfig;
import cn.imatu.framework.mqtt.config.MqttProperties;
import cn.imatu.framework.mqtt.enable.EnableMqttBeanDefinitionRegistryPostProcessor;
import cn.imatu.framework.mqtt.enable.EnableMqttRegistrar;
import cn.imatu.framework.mqtt.enable.EnableMqtt;
import cn.imatu.framework.mqtt.service.MqttSendExpandServiceImpl;
import cn.imatu.framework.mqtt.service.MqttSendService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Import;
import org.springframework.integration.annotation.IntegrationComponentScan;

import javax.annotation.PostConstruct;

/**
 * @author shenguangyang
 */
@AutoConfiguration
@EnableMqtt(value = false)
@IntegrationComponentScan("cn.imatu.framework.mqtt")
@EnableConfigurationProperties(MqttProperties.class)
@ImportAutoConfiguration({MqttConsumerConfig.class, MqttProducerConfig.class})
@Import({
        EnableMqttBeanDefinitionRegistryPostProcessor.class, EnableMqttRegistrar.class,
        MqttSendExpandServiceImpl.class, MqttSendService.class
})
public class LyMqttAutoConfiguration {
    private static final Logger log = LoggerFactory.getLogger(LyMqttAutoConfiguration.class);

    @PostConstruct
    public void init() {
        log.info("init {}", this.getClass().getName());
    }
}
