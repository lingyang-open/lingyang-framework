package cn.imatu.framework.mqtt;

import cn.hutool.core.util.ObjectUtil;
import cn.imatu.framework.exception.BizException;
import org.eclipse.paho.client.mqttv3.MqttDeliveryToken;

/**
 * @author shenguangyang
 */
public class MqttHelper {
    public static MqttDeliveryToken toMqttDeliveryToken(Object sendResult) {
        if (ObjectUtil.isNotNull(sendResult)) {
            if (sendResult instanceof MqttDeliveryToken) {
                return (MqttDeliveryToken) sendResult;
            }
        }
        throw new BizException("toMqttDeliveryToken fail");
    }
}
