package cn.imatu.framework.mqtt.config;

import cn.imatu.framework.core.constant.LyCoreConstants;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author shenguangyang
 */
@Component
@ConfigurationProperties(
        prefix = LyCoreConstants.PROPERTIES_PRE + "mqtt"
)
public class MqttProperties {
    /**
     * 推送信息的连接地址，如果有多个，用逗号隔开，如：tcp://127.0.0.1:1883,tcp://192.168.60.133:1883
     */
    private String serverUri;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 设置超时时间 单位为秒
     */
    private Integer connectionTimeout = 10;

    /**
     * 设置会话心跳时间 单位为秒 服务器会每隔(1.5*keepTime)秒的时间向客户端发送个消息判断客户端是否在线
     * 但这个方法并没有重连的机制
     */
    private Integer keepAliveSeconds = 20;

    private String subType = "2060";

    private Consumer consumer = new Consumer();
    private Producer producer = new Producer();

    public static class Producer {
        private String defaultTopic;
        /**
         * mqtt客户端ID
         */
        private String clientId;

        public Producer() {
            this.defaultTopic = "DEFAULT-TOPIC";
            this.clientId = System.currentTimeMillis() + "";
        }

        public String getDefaultTopic() {
            return defaultTopic;
        }

        public void setDefaultTopic(String defaultTopic) {
            this.defaultTopic = defaultTopic;
        }

        public String getClientId() {
            return clientId;
        }

        public void setClientId(String clientId) {
            this.clientId = clientId;
        }
    }


    public static class Consumer {
        private String defaultTopic;
        /**
         * mqtt客户端ID
         */
        private String clientId;

        public Consumer() {
            this.defaultTopic = "DEFAULT-TOPIC";
            this.clientId = System.currentTimeMillis() + "";
        }

        public String getDefaultTopic() {
            return defaultTopic;
        }

        public void setDefaultTopic(String defaultTopic) {
            this.defaultTopic = defaultTopic;
        }

        public String getClientId() {
            return clientId;
        }

        public void setClientId(String clientId) {
            this.clientId = clientId;
        }
    }

    public String getServerUri() {
        return serverUri;
    }

    public void setServerUri(String serverUri) {
        this.serverUri = serverUri;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getConnectionTimeout() {
        return connectionTimeout;
    }

    public void setConnectionTimeout(Integer connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

    public Integer getKeepAliveSeconds() {
        return keepAliveSeconds;
    }

    public void setKeepAliveSeconds(Integer keepAliveSeconds) {
        this.keepAliveSeconds = keepAliveSeconds;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public Consumer getConsumer() {
        return consumer;
    }

    public void setConsumer(Consumer consumer) {
        this.consumer = consumer;
    }

    public Producer getProducer() {
        return producer;
    }

    public void setProducer(Producer producer) {
        this.producer = producer;
    }
}
