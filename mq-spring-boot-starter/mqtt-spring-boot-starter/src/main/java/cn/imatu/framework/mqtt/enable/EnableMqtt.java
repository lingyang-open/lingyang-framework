package cn.imatu.framework.mqtt.enable;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 使能mqtt
 *
 * @author shenguangyang
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Documented
@Import({EnableMqttRegistrar.class})
public @interface EnableMqtt {
    /**
     * 是否使能, 默认使能
     */
    boolean value() default true;
}
