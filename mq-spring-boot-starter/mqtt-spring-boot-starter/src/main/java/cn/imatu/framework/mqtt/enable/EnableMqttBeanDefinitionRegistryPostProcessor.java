package cn.imatu.framework.mqtt.enable;

import cn.imatu.framework.tool.core.StringUtils;
import cn.imatu.framework.mq.base.domain.MqEnable;
import cn.imatu.framework.mq.base.enums.MqTypeEnum;
import cn.imatu.framework.mqtt.config.MqttConsumerConfig;
import cn.imatu.framework.mqtt.config.MqttEnabled;
import cn.imatu.framework.mqtt.config.MqttProducerConfig;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;

/**
 * BeanDefinitionRegistryPostProcessor 后置处理器, 这里用于判断是否使能rocketmq
 * 如果不使能则移除, 相关自动配置类
 *
 * @author shenguangyang
 */
public class EnableMqttBeanDefinitionRegistryPostProcessor implements BeanDefinitionRegistryPostProcessor {
    private volatile static Boolean isEnable = true;

    public static void setEnable(Boolean enable) {
        isEnable = enable;
    }

    public EnableMqttBeanDefinitionRegistryPostProcessor() {
    }

    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry beanDefinitionRegistry) throws BeansException {
        if (isEnable) {
            return;
        }
        if (MqEnable.isEnabled(MqTypeEnum.ROCKETMQ)) {
            return;
        }
        beanDefinitionRegistry.removeBeanDefinition(StringUtils.uncapitalize(MqttEnabled.class.getSimpleName()));
        beanDefinitionRegistry.removeBeanDefinition(StringUtils.uncapitalize(MqttProducerConfig.class.getSimpleName()));
        beanDefinitionRegistry.removeBeanDefinition(StringUtils.uncapitalize(MqttConsumerConfig.class.getSimpleName()));
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory configurableListableBeanFactory) throws BeansException {

    }
}
