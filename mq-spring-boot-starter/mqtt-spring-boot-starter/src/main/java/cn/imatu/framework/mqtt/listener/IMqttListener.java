package cn.imatu.framework.mqtt.listener;

/**
 * @author shenguangyang
 */
public interface IMqttListener {
    void onMessage(String topic, String message);
}
