package cn.imatu.framework.rabbitmq.callback;

import cn.imatu.framework.rabbitmq.entity.Conversion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import javax.annotation.Resource;

/**
 * 通过实现 ConfirmCallback 接口，消息发送到 Broker 后触发回调，确认消息是否到达 Broker 服务器，也就是只确认是否正确到达 Exchange 中
 * <p>
 * 如何生效:
 * 配置yaml: spring.rabbitmq.publisher-confirm-type (消息发送后,如果发送成功到队列,则会回调成功信息)
 * <p>
 * publisher-confirm-type三种类型
 * NONE: 禁用发布确认模式，是默认值
 * CORRELATED: 发布消息成功到交换器后会触发回调方法
 * SIMPLE: 经测试有两种效果，其一效果和CORRELATED值一样会触发回调方法，其二在发布消息成功后使用rabbitTemplate调用waitForConfirms或
 * waitForConfirmsOrDie方法等待broker节点返回发送结果，根据返回结果来判定下一步的逻辑，要注意的点是waitForConfirmsOrDie方法如
 * 果返回false则会关闭channel，则接下来无法发送消息到broker;
 *
 * @author shenguangyang
 */
public class MessageConfirmCallback implements RabbitTemplate.ConfirmCallback {
    private static final Logger log = LoggerFactory.getLogger(MessageConfirmCallback.class);
    @Resource
    private MqSendFailService<RabbitSendFailMqMessage> sendFailService;

    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {
        if (ack) {
            log.debug("message send success ===> correlationData: {}, cause: {}", correlationData, cause);
            if (correlationData != null) {
                String id = correlationData.getId();
                sendFailService.delete(id);
            }
        } else {
            log.error("message send fail, id: {}， cause: {}", correlationData == null ? "" : correlationData.getId(), cause);
            // 如果 correlationData 为空, 则消息无法达到具体的交换机或者队列
            if (correlationData == null) {
                return;
            }
            RabbitSendFailMqMessage rabbitMqMessage = Conversion.to(correlationData);
            sendFailService.save(rabbitMqMessage);
        }
    }
}
