package cn.imatu.framework.mybatis.mate.annotations;

import cn.imatu.framework.mybatis.mate.inter.EntityFieldUniqueCheck;
import com.baomidou.mybatisplus.annotation.TableName;

import java.lang.annotation.*;

/**
 * 实体类唯一约束注解, 可以防止数据重复, 只能使用mybatis plus提供的保存或者更新操作接口
 * 要求实体类需要实现 {@link EntityFieldUniqueCheck} 只是作为一个标记
 * <p>
 * 底层为了获取到表名, 会先看有没有 {@link TableName}, 如果没有的话就会将类名驼峰转为下划线
 * 分割的字符串作为表明
 *
 * @author shenguangyang
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface FieldUnique {
    /**
     * 指定数据库字段, 如果不指定就将实体类的字段名转为下划线
     */
    String field() default "";

    /**
     * 条件, 只有当其等于 {@link Condition#NONE} 时候, {@link FieldUniqueConfig} 配置才会生效 <br/>
     * 否则会强制sql 等于或者不等于当前注解所在字段值 <br/>
     *
     * <br/>
     * 多个{@link Condition#NONE}当前注解所在字段的拼接条件是{@link FieldUniqueConfig}配置中所指定的, 且
     * 每个字段都是等于 <br/>
     * eg: a = 1 and b = 2
     */
    Condition condition() default Condition.NONE;

    /**
     * 重复时报错错误消息
     */
    String message() default "数据重复";

    /**
     * 指定字段值, 如果指定了字段值, 则不会取对象的字段值, 而是用该值
     */
    String value() default "";

    /**
     * 指定请求头key, 如果指定了 headerKey, 则不会从 {@link #value()} 和 当前注解所在字段 获取值, 而是从请求头中获取 <br/>
     */
    String headerKey() default "";

    enum Condition {
        EQ, NE, NONE
    }
}
