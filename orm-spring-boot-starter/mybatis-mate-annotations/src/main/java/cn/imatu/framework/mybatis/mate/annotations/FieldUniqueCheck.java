package cn.imatu.framework.mybatis.mate.annotations;

import cn.imatu.framework.mybatis.mate.inter.EntityFieldUniqueCheck;
import cn.imatu.framework.mybatis.mate.enums.OperationTypeEnum;

import java.lang.annotation.*;

/**
 * 保存实体数据或者更新实体数据, 唯一性校验
 * 该注解通常被使用在service层的方法中, 而且要求方法的入参是一个且必须是数据库实体类
 * <p>
 * 被当前注解标注的方法通常只做更新或者保存数据到数据库中, 并不会做其他多余的操作
 * <p>
 * 如果遇到复杂业务, 你可以定义个ServiceApp层服务接口, 该接口中用于实现某个复杂业务, 同时可以调用多个
 * service接口中的任意方法
 * <p>
 * 总结需要注意两个地方:
 * 1. 参数目前只能是一个且必须是数据库实体类
 * 2. 数据库实体类需要实现 {@link EntityFieldUniqueCheck} 接口
 *
 * @author shenguangyang
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface FieldUniqueCheck {

    /**
     * 操作类型, 是保存还是更新
     */
    OperationTypeEnum type();
}
