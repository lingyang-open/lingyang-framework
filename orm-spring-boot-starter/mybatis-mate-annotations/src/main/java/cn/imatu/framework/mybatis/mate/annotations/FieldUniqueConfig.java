package cn.imatu.framework.mybatis.mate.annotations;

import cn.imatu.framework.mybatis.mate.enums.SqlConditionTypeEnum;

import java.lang.annotation.*;

/**
 * 字段唯一约束配置, 只针对 {@link FieldUnique} 注解
 *
 * @author shenguangyang
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface FieldUniqueConfig {
    /**
     * 表的主键名
     */
    String tableId() default "id";

    /**
     * 对象中中的主键字段名称
     */
    String entityId() default "id";

    /**
     * sql类型, 用于进行字段唯一约束时候, 查询的sql条件
     * 只能是or 或者 and
     */
    SqlConditionTypeEnum sqlCondition() default SqlConditionTypeEnum.OR;
}
