package cn.imatu.framework.mybatis.mate.config;

import com.github.pagehelper.autoconfigure.PageHelperAutoConfiguration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.List;

/**
 * @author shenguangyang
 */
@Configuration
@AutoConfigureAfter(PageHelperAutoConfiguration.class)
public class MybatisInterceptorCustomConfig {

    @Resource
    private List<SqlSessionFactory> sqlSessionFactoryList;

    /**
     * 可解决 spring boot 在使用 pagehelper-spring-boot-starter 后 ，自定义拦截器失效的问题。
     */
    @PostConstruct
    public void addMySqlInterceptor() {

    }
}