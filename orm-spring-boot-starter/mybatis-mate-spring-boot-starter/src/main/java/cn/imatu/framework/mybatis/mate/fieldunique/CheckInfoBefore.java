package cn.imatu.framework.mybatis.mate.fieldunique;

import cn.imatu.framework.mybatis.mate.annotations.FieldUnique;
import org.apache.ibatis.reflection.MetaObject;

import java.util.Map;

/**
 * @author shenguangyang
 */
public class CheckInfoBefore {
    private final Object object;
    /**
     * sql语句
     */
    private final String sql;
    /**
     * 缓存key, 用于实现并发安全
     * 缓存字符串key, 格式是标注{@link FieldUnique} 注解的字段名 + 字段值(或者注解中value中值)的拼接
     */
    private final String cacheKey;
    /**
     * 字段值重复时候, 提示的消息
     */
    private final Map<String, String> fieldMessageMap;
    /**
     * 对象元数据
     */
    private final MetaObject metaObject;

    public CheckInfoBefore(Object object, String sql, String cacheKey, Map<String, String> fieldMessageMap, MetaObject metaObject) {
        this.object = object;
        this.sql = sql;
        this.cacheKey = cacheKey;
        this.fieldMessageMap = fieldMessageMap;
        this.metaObject = metaObject;
    }

    public Object getObject() {
        return object;
    }

    public String getSql() {
        return sql;
    }

    public String getCacheKey() {
        return cacheKey;
    }

    public Map<String, String> getFieldMessageMap() {
        return fieldMessageMap;
    }

    public MetaObject getMetaObject() {
        return metaObject;
    }
}
