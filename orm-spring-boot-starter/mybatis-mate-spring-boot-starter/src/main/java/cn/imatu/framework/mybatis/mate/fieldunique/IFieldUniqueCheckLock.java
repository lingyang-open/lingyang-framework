package cn.imatu.framework.mybatis.mate.fieldunique;

import cn.imatu.framework.mybatis.mate.annotations.FieldUnique;

/**
 * 在进行字段唯一性校验时候, 对于微服务部署方式, 为了保证并发依旧能保证数据唯一性校验
 * 需要用户实现如下接口, 并标注 org.springframework.stereotype.Component 注解
 * <p>
 * 如果没有引入redis, 则在字段唯一性校验时候会采用jvm级别锁, 也就是单体部署是线程安全的,
 * 分布式部署是非安全的
 *
 * @author shenguangyang
 */
public interface IFieldUniqueCheckLock {
    /**
     * 上锁
     *
     * @param cacheKey 缓存字符串key, 格式是标注{@link FieldUnique} 注解的字段名 + 字段值(或者注解中value中值)的拼接
     */
    boolean lock(String cacheKey) throws Exception;

    /**
     * 释放锁
     */
    void unlock(String cacheKey);
}
