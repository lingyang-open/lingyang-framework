package cn.imatu.framework.mybatis.plus;

import cn.imatu.framework.mybatis.plus.config.CustomIdGenerator;
import cn.imatu.framework.mybatis.plus.config.MybatisPlusExtendProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Import;

/**
 * 如果使用了自定义sql 且有字段使用了自动填充,则xml中这些字段不要做空判断
 *
 * @author shenguangyang
 */
@Import({CustomIdGenerator.class})
@EnableConfigurationProperties(MybatisPlusExtendProperties.class)
public class LyMybatisPlusAutoConfiguration {
    private static final Logger log = LoggerFactory.getLogger(LyMybatisPlusAutoConfiguration.class);
}
