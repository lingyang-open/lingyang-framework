package cn.imatu.framework.mybatis.plus.config;


import com.baomidou.mybatisplus.autoconfigure.IdentifierGeneratorAutoConfiguration;
import com.baomidou.mybatisplus.core.incrementer.IdentifierGenerator;
import com.github.yitter.idgen.YitIdHelper;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;

/**
 * 自定全局唯一id生成器
 *
 * @author shenguangyang
 */
@AutoConfigureBefore(IdentifierGeneratorAutoConfiguration.class)
@ConditionalOnExpression("${lingyang-framework.mybatis-plus.custom-id-generator.enable-yit-id:true}")
public class CustomIdGenerator implements IdentifierGenerator {
    @Override
    public Long nextId(Object entity) {
        return YitIdHelper.nextId();
    }
}
