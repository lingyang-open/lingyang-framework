package cn.imatu.framework.mybatis.plus.utils;

import cn.imatu.framework.core.page.PageModel;
import cn.imatu.framework.core.page.TableSupport;
import cn.imatu.framework.core.response.PageData;
import cn.imatu.framework.core.response.Resp;
import cn.imatu.framework.exception.BizException;
import cn.imatu.framework.model.BasePageReq;
import cn.imatu.framework.tool.core.CollectionUtils;
import cn.imatu.framework.tool.core.SqlUtils;
import cn.imatu.framework.tool.core.StringUtils;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

/**
 * 分页工具
 *
 * @author shenguangyang
 */
public class PageUtil {
    /**
     * 设置请求分页数据
     */
    public static void startPage() {
        PageModel pageModel = TableSupport.buildPageRequest();
        Integer pageNum = pageModel.getPageNum();
        Integer pageSize = pageModel.getPageSize();
        if (StringUtils.isNotNull(pageNum) && StringUtils.isNotNull(pageSize)) {
            String orderBy = SqlUtils.escapeOrderBySql(pageModel.getOrderBy());
            PageHelper.startPage(pageNum <= 0 ? 1 : pageNum, pageSize, orderBy);
        } else {
            throw new BizException("Illegal parameter");
        }
    }

    /**
     * 响应请求分页数据
     * @deprecated
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    public static <T> Resp<PageData<T>> getDataTable(List<T> list) {
        PageModel pageModel = TableSupport.buildPageRequest();
        Integer pageNum = pageModel.getPageNum();
        Integer pageSize = pageModel.getPageSize();
        long total = CollectionUtils.isEmpty(list) ? 0 : new PageInfo(list).getTotal();

        PageData<T> pageData = new PageData<>();
        pageData.setPageNum(pageNum);
        pageData.setRecords(list);
        pageData.setPageSize(pageSize);
        pageData.setTotal(total);
        return Resp.ok(pageData);
    }


    public static <T> Resp<PageData<T>> getDataTable(IPage<T> page) {
        PageModel pageModel = TableSupport.buildPageRequest();
        Integer pageNum = pageModel.getPageNum();
        Integer pageSize = pageModel.getPageSize();
        List<T> records = page.getRecords();
        long total = page.getTotal();

        PageData<T> pageData = new PageData<>();
        pageData.setPageNum(pageNum);
        pageData.setRecords(records);
        pageData.setPageSize(pageSize);
        pageData.setTotal(total);
        return Resp.ok(pageData);
    }

    /**
     * 对IPage对象的泛型进行转换比如 IPage<XxPO> 转成 IPage<XxResp>
     * <p>
     * <code>
     *     IPage<SysMenuGroupPO> pageData = sysMenuGroupMapper.selectPage(page, wrapper);
     *     return PageUtil.to(pageData, sysMenuAssembly::toSysMenuGroupResp);
     * </code>
     * </p>
     * @param tran 转换实体类, 比如将XxPO转成 XxResp
     */
    public static <T, R> IPage<T> to(IPage<R> page, Function<R, T> tran) {
        IPage<T> respPage = new Page<>(page.getCurrent(), page.getSize(), page.getTotal(), page.searchCount());
        List<R> records = page.getRecords();
        List<T> respList = new ArrayList<>(records.size());
        records.forEach(e -> respList.add(tran.apply(e)));
        respPage.setRecords(respList);
        return respPage;
    }

    /**
     * @deprecated
     */
    public static <T> IPage<T> to(PageModel pageModel) {
        return new Page<>(pageModel.getPageNum(), pageModel.getPageSize());
    }

    public static <T> IPage<T> to(BasePageReq basePageReq) {
        return new Page<>(basePageReq.getPageNum(), basePageReq.getPageSize());
    }

    public static <T> IPage<T> getPage() {
        PageModel pageModel = TableSupport.buildPageRequest();
        return to(pageModel);
    }
}
