package cn.imatu.framework.mybatis.plus;

import org.apache.ibatis.reflection.DefaultReflectorFactory;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.reflection.factory.DefaultObjectFactory;
import org.apache.ibatis.reflection.wrapper.DefaultObjectWrapperFactory;
import org.junit.jupiter.api.Test;

/**
 * @author shenguangyang
 * @date 2022-09-30 21:29
 */
public class IMetaObjectTest {
    private static final DefaultObjectFactory defaultObjectFactory = new DefaultObjectFactory();
    private static final DefaultObjectWrapperFactory defaultObjectWrapperFactory = new DefaultObjectWrapperFactory();
    private static final DefaultReflectorFactory defaultReflectorFactory = new DefaultReflectorFactory();

    @Test
    public void test() {
        for (int j = 0; j < 10; j++) {
            MetaObjectPOTest target = new MetaObjectPOTest();
            long start = System.currentTimeMillis();
            Boolean isSuccess = null;
            for (int i = 0; i < 200; i++) {
                MetaObject metaObject = MetaObject.forObject(target, defaultObjectFactory, defaultObjectWrapperFactory, defaultReflectorFactory);
                metaObject.setValue("age", i);
                metaObject.getValue("age");
                metaObject.setValue("isSuccess", Boolean.TRUE);
                isSuccess = (Boolean) metaObject.getValue("isSuccess");
            }
            System.out.println(isSuccess);
            long end = System.currentTimeMillis();
            System.out.println("timeout=" + (end - start));//809 753 880 875 816
        }

    }

    public static class MetaObjectPOTest {
        private Integer age;
        private Boolean isSuccess;
        private String name;

        public Integer getAge() {
            return age;
        }

        public void setAge(Integer age) {
            this.age = age;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
