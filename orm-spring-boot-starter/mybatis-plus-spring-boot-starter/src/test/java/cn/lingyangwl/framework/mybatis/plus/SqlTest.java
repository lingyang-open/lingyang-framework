package cn.imatu.framework.mybatis.plus;

import cn.imatu.framework.tool.core.SqlUtils;

/**
 * @author shenguangyang
 * @date 2022-07-16 8:15
 */
public class SqlTest {
    public static void main(String[] args) {
        String str = "1 \"1 = 0";
        System.out.println(SqlUtils.containsSqlInjection(str));
    }
}
