package cn.imatu.framework.security;

import cn.imatu.framework.security.ratelimit.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

import javax.annotation.PostConstruct;

/**
 * @author shenguangyang
 */
@AutoConfiguration
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
@Import({
        RateLimitAspect.class, RedisScriptConfig.class, RateLimitManager.class
})
@EnableConfigurationProperties({RateLimitProperties.class})
public class LySecurityRateLimitServletAutoConfiguration {
    private static final Logger log = LoggerFactory.getLogger(LySecurityAutoConfiguration.class);

    @PostConstruct
    public void init() {
        log.info("init {}", this.getClass().getName());
    }

    @Bean
    @ConditionalOnMissingBean
    public RateLimitConfig rateLimitConfig() {
        return new RateLimitConfig();
    }
}
