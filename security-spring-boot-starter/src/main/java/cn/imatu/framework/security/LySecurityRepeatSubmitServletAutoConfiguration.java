package cn.imatu.framework.security;

import cn.imatu.framework.security.submit.RepeatSubmitAspect;
import cn.imatu.framework.security.submit.RepeatSubmitProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Import;

import javax.annotation.PostConstruct;

/**
 * @author shenguangyang
 */
@Slf4j
@AutoConfiguration
@Import({
        RepeatSubmitAspect.class
})
@EnableConfigurationProperties({RepeatSubmitProperties.class})
@ConditionalOnProperty(prefix = RepeatSubmitProperties.PREFIX, name = "enabled",
        havingValue = "true", matchIfMissing = false)
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
public class LySecurityRepeatSubmitServletAutoConfiguration {

    @PostConstruct
    public void init() {
        log.info("init {}", this.getClass().getName());
    }
}
