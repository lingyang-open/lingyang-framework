package cn.imatu.framework.security.ratelimit;

/**
 * 限流类型
 *
 * @author shenguangyang
 */

public enum LimitTypeEnum {
    /**
     * 默认策略全局限流
     */
    DEFAULT,

    /**
     * 根据请求者IP进行限流
     */
    IP,

    /**
     * 根据token限流
     * @see RateLimitConfig
     */
    TOKEN
}
