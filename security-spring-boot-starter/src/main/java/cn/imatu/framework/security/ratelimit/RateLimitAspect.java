package cn.imatu.framework.security.ratelimit;

import cn.hutool.crypto.digest.DigestUtil;
import cn.imatu.framework.core.utils.IpUtils;
import cn.imatu.framework.exception.BizException;
import cn.imatu.framework.tool.core.DateUtils;
import cn.imatu.framework.tool.core.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.RedisScript;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.lang.reflect.Method;
import java.time.Duration;
import java.util.Collections;
import java.util.List;

/**
 * 限流处理
 *
 * @author shenguangyang
 */
@Aspect
@Component
public class RateLimitAspect {
    private static final Logger log = LoggerFactory.getLogger(RateLimitAspect.class);

    @Resource
    private RateLimitManager rateLimitManager;

    @Resource(name = "securityRedisTemplate")
    private RedisTemplate<String, Object> securityRedisTemplate;

    @Resource
    private RedisScript<Long> limitScript;

    @Resource
    private RateLimitProperties rateLimitProperties;

    @Resource
    private RateLimitConfig rateLimitConfig;

    @Before("@annotation(rateLimit)")
    public void doBefore(JoinPoint point, RateLimit rateLimit) throws Exception {
        int time = rateLimit.time();
        int count = rateLimit.count();

        RateLimitProperties.Blacklist blacklist = rateLimitProperties.getBlacklist();
        boolean enabled = blacklist.isEnabled();
        if (!enabled) {
            return;
        }

        String combineKey = getCombineKey(rateLimit, point);
        List<String> keys = Collections.singletonList(combineKey);

        // 判断是否是黑名单
        rateLimitManager.checkBlacklist(combineKey, rateLimit);
        Long number = securityRedisTemplate.execute(limitScript, keys, count, time);
        if (StringUtils.isNull(number) || number.intValue() > count) {
            // 将ip添加到黑名单中
            rateLimitManager.addBlacklist(combineKey, rateLimit.type());

            throw new BizException(parseMsg(rateLimit.msg()));
        }
        log.debug("限流 ==> 限制类型: {}, 限制请求: {}, 当前请求: {}, 缓存key: {}", rateLimit.type().name(), count, number.intValue(), combineKey);
    }

    public String parseMsg(String msg) {
        if (StringUtils.isEmpty(msg)) {
            return msg;
        }
        Duration limitTime = rateLimitProperties.getBlacklist().getLimitTime();
        long timeMillis = System.currentTimeMillis();
        String remainingTime = DateUtils.distance(timeMillis, timeMillis + limitTime.getSeconds() * 1000L, true);
        msg = msg.replace(RateLimitCons.REMAINING_TIME_ARG, remainingTime);
        return msg;
    }

    public String getCombineKey(RateLimit rateLimit, JoinPoint point) {
        StringBuilder stringBuffer = new StringBuilder("rateLimit::" + rateLimit.key());
        MethodSignature signature = (MethodSignature) point.getSignature();
        Method method = signature.getMethod();
        Class<?> targetClass = method.getDeclaringClass();
        stringBuffer.append(":").append(DigestUtil.md5Hex(targetClass.getName() + "-" + method.getName()));

        if (rateLimit.type() == LimitTypeEnum.IP) {
            stringBuffer.append(":").append(IpUtils.getRequestIp());
        } else if (rateLimit.type() == LimitTypeEnum.TOKEN) {
            if (rateLimitConfig.tokenSupplier == null) {
                throw new BizException("当前是以用户维度进行限流, 请配置限流token提供器配置");
            }
            String token = rateLimitConfig.tokenSupplier.get();
            stringBuffer.append(":").append(token);
        }
        return stringBuffer.toString();
    }
}
