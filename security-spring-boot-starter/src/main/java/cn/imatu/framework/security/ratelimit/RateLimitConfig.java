package cn.imatu.framework.security.ratelimit;

import cn.imatu.framework.core.utils.IpUtils;

import java.util.function.Supplier;

/**
 * @author shenguangyang
 */
public class RateLimitConfig {
    public Supplier<String> tokenSupplier = IpUtils::getRequestIp;
}
