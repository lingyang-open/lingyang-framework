package cn.imatu.framework.security.ratelimit;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.script.DefaultRedisScript;

/**
 * @author shenguangyang
 */
@Configuration
public class RedisScriptConfig {

    @Bean
    @ConditionalOnMissingBean(name = "limitScript")
    public DefaultRedisScript<Long> limitScript() {
        DefaultRedisScript<Long> redisScript = new DefaultRedisScript<>();
        redisScript.setLocation(new ClassPathResource("lua/limit-script.lua"));
        redisScript.setResultType(Long.class);
        return redisScript;
    }
}
