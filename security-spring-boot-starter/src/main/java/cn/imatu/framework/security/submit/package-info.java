/**
 * 重复提交处理, 主要用于支付等重要场景, 避免发起多次请求
 * @author shenguangyang
 */
package cn.imatu.framework.security.submit;