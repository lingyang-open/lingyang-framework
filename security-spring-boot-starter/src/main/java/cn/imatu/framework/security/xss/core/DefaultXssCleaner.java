package cn.imatu.framework.security.xss.core;

import cn.imatu.framework.security.xss.config.SecurityXssProperties;
import cn.imatu.framework.security.xss.utils.XssUtils;
import cn.hutool.core.util.CharsetUtil;
import org.jsoup.Jsoup;
import org.jsoup.internal.StringUtil;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Entities;
import org.springframework.web.util.HtmlUtils;

/**
 * 默认的 xss 清理器
 *
 * @author shenguangyang
 */
public class DefaultXssCleaner implements XssCleaner {

    private final SecurityXssProperties properties;

    public DefaultXssCleaner(SecurityXssProperties properties) {
        this.properties = properties;
    }

    private static Document.OutputSettings getOutputSettings(SecurityXssProperties properties) {
        return new Document.OutputSettings()
                // 2. 转义，没找到关闭的方法，目前这个规则最少
                .escapeMode(Entities.EscapeMode.xhtml)
                // 3. 保留换行
                .prettyPrint(properties.isPrettyPrint());
    }

    @Override
    public String clean(String bodyHtml, XssType type) {
        // 1. 为空直接返回
        if (StringUtil.isBlank(bodyHtml)) {
            return bodyHtml;
        }
        SecurityXssProperties.Mode mode = properties.getMode();
        if (SecurityXssProperties.Mode.escape == mode) {
            // html 转义
            return HtmlUtils.htmlEscape(bodyHtml, CharsetUtil.UTF_8);
        } else if (SecurityXssProperties.Mode.validate == mode) {
            // 校验
            if (Jsoup.isValid(bodyHtml, XssUtils.WHITE_LIST)) {
                return bodyHtml;
            }
            throw type.getXssException(bodyHtml, "Xss validate fail, input value:" + bodyHtml);
        } else {
            // 4. 清理后的 html
            String escapedHtml = Jsoup.clean(bodyHtml, "", XssUtils.WHITE_LIST, getOutputSettings(properties));
            if (properties.isEnableEscape()) {
                return escapedHtml;
            }
            // 5. 反转义
            return Entities.unescape(escapedHtml);
        }
    }

}
