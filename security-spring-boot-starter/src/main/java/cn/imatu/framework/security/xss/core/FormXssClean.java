package cn.imatu.framework.security.xss.core;

import cn.imatu.framework.security.xss.config.SecurityXssProperties;
import cn.imatu.framework.security.xss.utils.XssUtils;
import cn.hutool.core.util.StrUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.InitBinder;

import java.beans.PropertyEditorSupport;

/**
 * 表单xss清除
 *
 * @author shenguangyang
 */
@ControllerAdvice
@ConditionalOnProperty(prefix = SecurityXssProperties.PREFIX, name = "enabled", havingValue = "true", matchIfMissing = true)
@RequiredArgsConstructor
public class FormXssClean {

    private final SecurityXssProperties properties;

    private final XssCleaner xssCleaner;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        // 处理前端传来的表单字符串
        binder.registerCustomEditor(String.class, new StringPropertiesEditor(xssCleaner, properties));
    }

    @Slf4j
    @RequiredArgsConstructor
    public static class StringPropertiesEditor extends PropertyEditorSupport {

        private final XssCleaner xssCleaner;

        private final SecurityXssProperties properties;

        @Override
        public String getAsText() {
            Object value = getValue();
            return value != null ? value.toString() : StrUtil.EMPTY;
        }

        @Override
        public void setAsText(String text) throws IllegalArgumentException {
            if (text == null) {
                setValue(null);
            } else if (XssHolder.isEnabled()) {
                String value = xssCleaner.clean(XssUtils.trim(text, properties.isTrimText()));
                setValue(value);
                log.debug("Request parameter value:{} cleaned up by mica-xss, current value is:{}.", text, value);
            } else {
                setValue(XssUtils.trim(text, properties.isTrimText()));
            }
        }

    }

}
