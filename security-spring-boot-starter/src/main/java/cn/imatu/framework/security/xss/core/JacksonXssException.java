package cn.imatu.framework.security.xss.core;

import cn.imatu.framework.exception.BaseException;
import lombok.Getter;

/**
 * xss jackson 异常
 *
 * @author shenguangyang
 */
@Getter
public class JacksonXssException extends BaseException {

    private final String input;

    public JacksonXssException(String input, String message) {
        super(message);
        this.input = input;
    }

}
