local key = KEYS[1]
local count = ARGV[1]
local time = tonumber(ARGV[2])
local current = redis.call('incr', key)
redis.call('set', 'test', time)
if tonumber(current) == 1 then
    redis.call('expire', key, time)
end
return tonumber(current);