package cn.imatu.framework.sms;

import cn.imatu.framework.sms.manager.AliyunSmsManager;
import cn.imatu.framework.sms.properties.AliyunSmsProperties;
import com.aliyun.dysmsapi20170525.models.SendSmsRequest;
import com.aliyun.dysmsapi20170525.models.SendSmsResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * @author shenguangyang
 */
@AutoConfiguration
@ConditionalOnClass({SendSmsResponse.class, SendSmsRequest.class})
@EnableConfigurationProperties(AliyunSmsProperties.class)
public class LyAliyunSmsAutoConfiguration {
    private static final Logger log = LoggerFactory.getLogger(LyAliyunSmsAutoConfiguration.class);
    @Resource
    private AliyunSmsProperties aliyunSmsProperties;

    @PostConstruct
    public void init() {
        log.info("init {}", this.getClass().getSimpleName());
    }

    @Bean
    @ConditionalOnMissingBean(AliyunSmsManager.class)
    public AliyunSmsManager aliyunSmsManager() {
        return new AliyunSmsManager(aliyunSmsProperties);
    }
}
