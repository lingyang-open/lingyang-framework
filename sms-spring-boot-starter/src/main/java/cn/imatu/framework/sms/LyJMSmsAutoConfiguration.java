package cn.imatu.framework.sms;

import cn.imatu.framework.core.constant.LyCoreConstants;
import cn.imatu.framework.sms.manager.JMSmsManager;
import cn.imatu.framework.sms.properties.JMSmsProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * @author shenguangyang
 */
@AutoConfiguration
@ConditionalOnProperty(prefix = LyCoreConstants.PROPERTIES_PRE + "sms.ju-mei", value = "enable", havingValue = "true")
@EnableConfigurationProperties(JMSmsProperties.class)
public class LyJMSmsAutoConfiguration {
    private static final Logger log = LoggerFactory.getLogger(LyJMSmsAutoConfiguration.class);
    @Resource
    private JMSmsProperties jmSmsProperties;

    @Resource
    private RestTemplate restTemplate;

    @PostConstruct
    public void init() {
        log.info("init {}", this.getClass().getSimpleName());
    }

    @Bean
    public JMSmsManager aliyunSmsManager() {
        return new JMSmsManager(jmSmsProperties, restTemplate);
    }
}
