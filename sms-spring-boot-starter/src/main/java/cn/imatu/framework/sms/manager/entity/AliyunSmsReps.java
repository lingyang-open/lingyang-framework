package cn.imatu.framework.sms.manager.entity;

import lombok.Getter;
import lombok.Setter;

/**
 * @author shenguangyang
 */
@Getter
@Setter
public class AliyunSmsReps extends SmsBaseReps {
    /**
     * 发送回执ID。
     * <p>
     * 可根据发送回执ID在接口QuerySendDetails中查询具体的发送状态。
     */
    private String bizId;

    public static AliyunSmsRepsBuilder builder() {
        return new AliyunSmsRepsBuilder();
    }

    public static final class AliyunSmsRepsBuilder {
        private String bizId;
        private String requestId;
        private String code;
        private String message;
        private boolean isSuccess;

        private AliyunSmsRepsBuilder() {
        }

        public AliyunSmsRepsBuilder bizId(String bizId) {
            this.bizId = bizId;
            return this;
        }

        public AliyunSmsRepsBuilder requestId(String requestId) {
            this.requestId = requestId;
            return this;
        }

        public AliyunSmsRepsBuilder code(String code) {
            this.code = code;
            return this;
        }

        public AliyunSmsRepsBuilder message(String message) {
            this.message = message;
            return this;
        }

        public AliyunSmsRepsBuilder isSuccess(boolean isSuccess) {
            this.isSuccess = isSuccess;
            return this;
        }

        public AliyunSmsReps build() {
            AliyunSmsReps aliyunSmsReps = new AliyunSmsReps();
            aliyunSmsReps.setBizId(bizId);
            aliyunSmsReps.setRequestId(requestId);
            aliyunSmsReps.setCode(code);
            aliyunSmsReps.setMessage(message);
            aliyunSmsReps.isSuccess = this.isSuccess;
            return aliyunSmsReps;
        }
    }
}
