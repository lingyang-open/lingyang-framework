package cn.imatu.framework.sms.manager.entity;

import cn.imatu.framework.tool.core.exception.Assert;
import lombok.*;

/**
 * @author shenguangyang
 */
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AliyunSmsReq extends SmsBaseReq {
    /**
     * 短信签名
     */
    protected String signName;

    /**
     * 外部流水扩展字段
     */
    private String outId;
    /**
     * 上行短信扩展码，无特殊需要此字段的用户请忽略此字段。
     */
    private String smsUpExtendCode;

    @Override
    public void check() {
        super.check();
        Assert.notEmpty(signName, "signName is null");
    }
}
