package cn.imatu.framework.sms.manager.entity;

import lombok.Getter;
import lombok.Setter;

/**
 * @author shenguangyang
 */
@Getter
@Setter
public class JMSmsReps extends SmsBaseReps {
    /**
     * 短信发送ID，可用于查询短信发送状态，建议保存
     */
    private String bizId;

    public static JMSmsRepsBuilder builder() {
        return new JMSmsRepsBuilder();
    }

    public static final class JMSmsRepsBuilder {
        private String bizId;
        private String requestId;
        private String code;
        private String message;
        private boolean isSuccess;

        private JMSmsRepsBuilder() {
        }

        public JMSmsRepsBuilder bizId(String bizId) {
            this.bizId = bizId;
            return this;
        }

        public JMSmsRepsBuilder requestId(String requestId) {
            this.requestId = requestId;
            return this;
        }

        public JMSmsRepsBuilder code(String code) {
            this.code = code;
            return this;
        }

        public JMSmsRepsBuilder message(String message) {
            this.message = message;
            return this;
        }

        public JMSmsRepsBuilder isSuccess(boolean isSuccess) {
            this.isSuccess = isSuccess;
            return this;
        }

        public JMSmsReps build() {
            JMSmsReps jMSmsReps = new JMSmsReps();
            jMSmsReps.setBizId(bizId);
            jMSmsReps.setRequestId(requestId);
            jMSmsReps.setCode(code);
            jMSmsReps.setMessage(message);
            jMSmsReps.isSuccess = this.isSuccess;
            return jMSmsReps;
        }
    }
}
