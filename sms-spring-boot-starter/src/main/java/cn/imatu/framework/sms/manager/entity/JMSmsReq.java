package cn.imatu.framework.sms.manager.entity;

import com.alibaba.fastjson2.JSONObject;

/**
 * @author shenguangyang
 */
public class JMSmsReq extends SmsBaseReq {

    public static JMSmsReqBuilder builder() {
        return new JMSmsReqBuilder();
    }

    public static final class JMSmsReqBuilder {
        private String phoneNumbers;
        private String templateId;
        private JSONObject templateParam;

        private JMSmsReqBuilder() {
        }

        public JMSmsReqBuilder phoneNumbers(String phoneNumbers) {
            this.phoneNumbers = phoneNumbers;
            return this;
        }

        public JMSmsReqBuilder templateId(String templateId) {
            this.templateId = templateId;
            return this;
        }

        public JMSmsReqBuilder templateParam(JSONObject templateParam) {
            this.templateParam = templateParam;
            return this;
        }

        public JMSmsReq build() {
            JMSmsReq jMSmsReq = new JMSmsReq();
            jMSmsReq.setPhoneNumbers(phoneNumbers);
            jMSmsReq.setTemplateId(templateId);
            jMSmsReq.setTemplateParam(templateParam);
            return jMSmsReq;
        }
    }
}
