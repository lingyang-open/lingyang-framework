package cn.imatu.framework.sms.manager.entity;

import lombok.Getter;
import lombok.Setter;

/**
 * @author shenguangyang
 */
@Getter
@Setter
public abstract class SmsBaseReps {
    /**
     * 请求ID
     */
    protected String requestId;

    /**
     * 请求响应状态
     */
    protected String code;

    protected String message;
    /**
     * 是否发送成功
     */
    protected boolean isSuccess;
}
