package cn.imatu.framework.sms.manager.entity;

import cn.imatu.framework.tool.core.exception.Assert;
import com.alibaba.fastjson2.JSONObject;
import lombok.Getter;
import lombok.Setter;

/**
 * 短信服务请求参数
 *
 * @author shenguangyang
 */
@Getter
@Setter
public abstract class SmsBaseReq {
    /**
     * 短信接收号码, 多个手机号用逗号隔开
     */
    protected String phoneNumbers;
    /**
     * 短信模板ID
     */
    protected String templateId;
    /**
     * 短信模板变量对应的实际值。支持传入多个参数，示例：{"name":"张三","number":"1390000****"}。
     */
    protected JSONObject templateParam;

    public void check() {
        Assert.notNull(templateId, "templateId is null");
        Assert.notNull(templateParam, "templateParam is null");
        Assert.notEmpty(phoneNumbers, "phoneNumbers is null");
    }
}
