package cn.imatu.framework.sms.properties;

import cn.imatu.framework.core.constant.LyCoreConstants;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * <a hrep="https://market.aliyun.com/products/57000002/cmapi00046920.html">提供测试模板，免审核，测试成本更低</a>
 * 聚美智数短信验证码, 仅供测试使用
 *
 * @author shenguangyang
 */
@Getter
@Setter
@ConfigurationProperties(prefix = LyCoreConstants.PROPERTIES_PRE + "sms.ju-mei")
public class JMSmsProperties {
    private boolean enable = false;
    private String appCode;
}
