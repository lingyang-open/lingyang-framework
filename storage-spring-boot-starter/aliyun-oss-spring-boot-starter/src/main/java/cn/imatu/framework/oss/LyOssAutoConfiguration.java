package cn.imatu.framework.oss;

import cn.imatu.framework.oss.config.AliyunOssConfig;
import cn.imatu.framework.oss.manager.AliyunStorageManager;
import cn.imatu.framework.oss.utils.OssUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

import javax.annotation.PostConstruct;

/**
 * @author shenguangyang
 */
@AutoConfiguration
@Import({OssUtils.class})
public class LyOssAutoConfiguration {
    private static final Logger log = LoggerFactory.getLogger(LyOssAutoConfiguration.class);

    @PostConstruct
    public void init() {
        log.info("init {}", this.getClass().getName());
    }

    @Bean
    @ConditionalOnMissingBean(AliyunOssConfig.class)
    public AliyunOssConfig aliyunOssConfig() {
        return new AliyunOssConfig();
    }

    @Bean
    @ConditionalOnMissingBean(AliyunStorageManager.class)
    public AliyunStorageManager aliyunStorageManager() {
        return new AliyunStorageManager();
    }
}
