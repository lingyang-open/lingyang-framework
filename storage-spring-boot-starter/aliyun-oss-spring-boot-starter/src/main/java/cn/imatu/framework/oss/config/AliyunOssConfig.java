package cn.imatu.framework.oss.config;

import cn.imatu.framework.storage.base.config.AliyunStorageProperties;
import cn.imatu.framework.storage.base.config.StorageProperties;
import cn.imatu.framework.storage.base.constants.FileStorageEnum;
import cn.imatu.framework.storage.base.manager.BaseStorageManager;
import cn.imatu.framework.storage.base.manager.StorageManager;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * Description:
 *
 * @author shenguangyang
 */
@Slf4j
public class AliyunOssConfig {
    @Resource
    StorageProperties storageProperties;

    @Resource
    AliyunStorageProperties aliyunStorageProperties;

    @PostConstruct
    public void init() throws Exception {
        if (!storageProperties.getInitClient()) {
            return;
        }
        FileStorageEnum type = storageProperties.getStorage();
        if (type == FileStorageEnum.ALIYUN || type == FileStorageEnum.ALL) {
            StorageManager storageManager = BaseStorageManager.get(FileStorageEnum.ALIYUN);
            storageManager.createClient(aliyunStorageProperties.getAccessKeyId(), aliyunStorageProperties.getSecretAccessKey());
        }
    }
}
