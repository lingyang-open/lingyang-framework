package cn.imatu.framework.minio;

import cn.imatu.framework.minio.autoconfig.MinioConfig;
import cn.imatu.framework.minio.manager.MinioMultiPartUploadStorageManager;
import cn.imatu.framework.minio.manager.MinioStorageManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;

import javax.annotation.PostConstruct;

/**
 * @author shenguangyang
 */
@AutoConfiguration
public class LyMinioAutoConfiguration {
    private static final Logger log = LoggerFactory.getLogger(LyMinioAutoConfiguration.class);

    @PostConstruct
    public void init() {
        log.info("init {}", this.getClass().getName());
    }

    @Bean
    @ConditionalOnMissingBean(MinioConfig.class)
    public MinioConfig minioConfig() {
        return new MinioConfig();
    }

    @Bean
    @ConditionalOnMissingBean(MinioStorageManager.class)
    public MinioStorageManager minioStorageManager() {
        return new MinioStorageManager();
    }

    @Bean
    @ConditionalOnMissingBean(MinioMultiPartUploadStorageManager.class)
    public MinioMultiPartUploadStorageManager minioMultiPartUploadStorageManager() {
        return new MinioMultiPartUploadStorageManager();
    }
}
