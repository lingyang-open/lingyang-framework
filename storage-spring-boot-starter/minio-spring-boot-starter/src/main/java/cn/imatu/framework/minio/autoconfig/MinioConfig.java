package cn.imatu.framework.minio.autoconfig;

import cn.imatu.framework.storage.base.config.MinioStorageProperties;
import cn.imatu.framework.storage.base.config.StorageProperties;
import cn.imatu.framework.storage.base.constants.FileStorageEnum;
import cn.imatu.framework.storage.base.manager.BaseStorageManager;
import cn.imatu.framework.storage.base.manager.StorageManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * Description: minio配置类,如果服务器与客户端主机的时间相差太大会启动失败
 *
 * @author shenguangyang
 */
public class MinioConfig {
    /**
     * logger
     */
    private final static Logger log = LoggerFactory.getLogger(MinioConfig.class);

    @Resource
    private StorageProperties storageProperties;

    @Resource
    private MinioStorageProperties minioStorageProperties;

    @PostConstruct
    public void init() throws Exception {
        if (!storageProperties.getInitClient()) {
            return;
        }
        FileStorageEnum type = storageProperties.getStorage();
        if (type == FileStorageEnum.MINIO || type == FileStorageEnum.ALL) {
            StorageManager storageManager = BaseStorageManager.get(FileStorageEnum.MINIO);
            storageManager.createClient(minioStorageProperties.getAccessKey(), minioStorageProperties.getSecretKey());
        }
        log.info("Minio文件系统初始化加载");
    }
}
