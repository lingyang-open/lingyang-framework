package cn.imatu.framework.minio.manager;

import cn.hutool.http.HttpRequest;
import cn.imatu.framework.minio.utils.MinioUtils;
import cn.imatu.framework.storage.base.config.MinioStorageProperties;
import cn.imatu.framework.storage.base.partupload.MultiPartUploadInit;
import cn.imatu.framework.storage.base.partupload.MultiPartUploadService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.File;

/**
 * @author shenguangyang
 */
@Component
public class MinioMultiPartUploadStorageManager implements MultiPartUploadService {
    @Resource
    private MinioStorageProperties minioStorageProperties;

    @Override
    public MultiPartUploadInit init(String objectName, int totalPart) {
        return MinioUtils.MultiPartUpload.init(minioStorageProperties.getBucketName(), objectName, totalPart);
    }


    @Override
    public void uploadPart(String uploadUrl, String contentType, File file) {
        HttpRequest.put(uploadUrl)
                .contentType(contentType)
                .form("key", file)
                .execute();
    }

    @Override
    public void merge(String objectName, String uploadId) {
        MinioUtils.MultiPartUpload.merge(minioStorageProperties.getBucketName(), objectName, uploadId);
    }
}
