package cn.imatu.framework.storage.base.config;

import cn.imatu.framework.core.constant.LyCoreConstants;
import cn.imatu.framework.tool.core.UrlUtils;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.annotation.PostConstruct;

/**
 * @author shenguangyang
 */
@Getter
@Setter
@ConfigurationProperties(prefix = LyCoreConstants.PROPERTIES_PRE + "storage.minio")
public class MinioStorageProperties extends BaseStorageProperties {
    protected String bucketName;

    private String accessKey;

    private String secretKey;

    @PostConstruct
    public void init() {
        this.setEndpoint(UrlUtils.addEndSlash(this.getEndpoint()));
    }
}
