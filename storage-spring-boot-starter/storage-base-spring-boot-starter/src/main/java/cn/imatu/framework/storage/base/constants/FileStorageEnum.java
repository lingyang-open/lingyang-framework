package cn.imatu.framework.storage.base.constants;

/**
 * @author shenguangyang
 */
public enum FileStorageEnum {
    MINIO,
    ALIYUN,
    ALL
}
