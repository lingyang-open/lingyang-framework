package cn.imatu.framework.storage.base.manager;

import java.io.InputStream;
import java.util.List;
import java.util.function.Consumer;

/**
 * 存储服务公共接口
 *
 * @author shenguangyang
 */
public interface StorageManager {
    void createClient(String accessKey,  String secretKey) throws Exception;

    /**
     * 获取内网访问url
     * @param bucketName 桶名
     * @param objectName 对象名
     * @return 可以直接访问的url
     */
    String getIntranetUrl(String bucketName, String objectName);

    /**
     * 获取内网访问url
     * @param objectName 对象名
     * @return 可以直接访问的url
     */
    default String getIntranetUrl(String objectName) {
        throw new UnsupportedOperationException();
    }

    /**
     * 上传文本
     *
     * @param text       文本内容
     * @param objectName 对象名 xx/yy/zz/fileName.text
     */
    default void uploadText(String text, String objectName) throws Exception {
        throw new UnsupportedOperationException();
    }

    /**
     * 上传文件
     */
    default void uploadFile(InputStream inputStream, String contentType, String objectName)
            throws Exception {
        throw new UnsupportedOperationException();
    }

    /**
     * 上传文件
     */
    default void uploadFile(String bucketName, InputStream inputStream, String contentType, String objectName)
            throws Exception {
        throw new UnsupportedOperationException();
    }

    /**
     * 上传文件夹
     */
    default void uploadDir(String dirPath) {
        throw new UnsupportedOperationException();
    }

    /**
     * 获取具有时效性的对象url
     *
     * @param objectName 对象名
     * @return 经过签名的url
     * @apiNote 生成的url是一个有时效性的
     */
    default String getObjectUrl(String objectName) {
        throw new UnsupportedOperationException();
    }

    /**
     * 获取文件
     *
     * @param objectName 对象名
     */
    default InputStream getFile(String objectName) {
        throw new UnsupportedOperationException();
    }

    /**
     * 获取指定路径下的所有文件
     *
     * @param pathPrefix 路径前缀
     * @return 路径下所有文件的完整路径
     */
    default List<String> listFilePath(String pathPrefix) {
        throw new UnsupportedOperationException();
    }

    /**
     * 获取指定路径下的所有文件
     *
     * @param pathPrefix       路径前缀
     * @param filePathCallback 文件路径回调
     * @return 返回文件个数
     */
    default List<String> listFilePath(String pathPrefix, Consumer<String> filePathCallback) {
        throw new UnsupportedOperationException();
    }

    /**
     * 创建桶
     *
     * @param randomSuffix 是否使能随机后缀,防止桶名存在
     */
    default Boolean createBucket(Boolean randomSuffix) {
        throw new UnsupportedOperationException();
    }

    /**
     * 创建桶
     */
    default Boolean createBucket(String bucketName) {
        throw new UnsupportedOperationException();
    }

    /**
     * 校验桶是否存在
     * @param bucketName 桶名
     * @return true 存在
     */
    default Boolean doesBucketExist(String bucketName) {
        throw new UnsupportedOperationException();
    }

    /**
     * 批量删除文件
     *
     * @param objectNameList 对象名集合
     * @throws Exception 异常
     */
    default void deleteObjects(List<String> objectNameList) throws Exception {
        throw new UnsupportedOperationException();
    }

    /**
     * 批量删除文件
     * @param bucketName 桶名
     * @param objectNameList 对象名集合
     * @throws Exception 异常
     */
    default void deleteObjects(String bucketName, List<String> objectNameList) throws Exception {
        throw new UnsupportedOperationException();
    }

    /**
     * 删除文件
     *
     * @param objectName 对象名集合
     * @throws Exception 异常
     */
    default void deleteObject(String objectName) throws Exception {
        throw new UnsupportedOperationException();
    }

    /**
     * 删除文件
     * @param bucketName 桶名
     * @param objectName 对象名集合
     * @throws Exception 异常
     */
    default void deleteObject(String bucketName, String objectName) throws Exception {
        throw new UnsupportedOperationException();
    }

    /**
     * 判断对象是否存在
     *
     * @param objectName 对象名
     */
    default boolean checkFileIsExist(String objectName) {
        throw new UnsupportedOperationException();
    }

    /**
     * 判断文件夹是否存在
     *
     * @param folderName 文件夹名称
     */
    default boolean checkFolderIsExist(String folderName) {
        throw new UnsupportedOperationException();
    }
}
