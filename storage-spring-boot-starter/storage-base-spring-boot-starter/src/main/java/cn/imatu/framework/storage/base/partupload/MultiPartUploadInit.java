package cn.imatu.framework.storage.base.partupload;

import java.util.Map;

/**
 * @author shenguangyang
 */
public class MultiPartUploadInit {
    /**
     * 上次id
     */
    private String uploadId;
    /**
     * 上传url
     */
    private Map<String, String> uploadUrls;

    public String getUploadId() {
        return uploadId;
    }

    public void setUploadId(String uploadId) {
        this.uploadId = uploadId;
    }

    public Map<String, String> getUploadUrls() {
        return uploadUrls;
    }

    public void setUploadUrls(Map<String, String> uploadUrls) {
        this.uploadUrls = uploadUrls;
    }
}
