package cn.imatu.framework.validation;

/**
 * Description: 用于实现枚举类的校验
 *
 * @author shenguangyang
 */
public interface EnumValidate<T> {
    /**
     * 校验枚举值是否存在
     *
     * @param code 比较的值
     * @return false 或者null
     */
    Boolean isExistCode(T code);

    /**
     * 获取描述
     *
     * @return 字符串
     */
    String getDesc();

    /**
     * 获取code码
     *
     * @return 整数类型
     */
    T getCode();
}
