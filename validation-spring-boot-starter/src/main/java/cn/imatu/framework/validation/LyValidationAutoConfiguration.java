package cn.imatu.framework.validation;

import cn.imatu.framework.core.config.ValidatorProperties;
import cn.imatu.framework.validation.config.ValidationConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Import;

import javax.annotation.PostConstruct;

/**
 * @author shenguangyang
 */
@Slf4j
@AutoConfiguration
@Import({
        ValidationConfig.class
})
@EnableConfigurationProperties(ValidatorProperties.class)
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
public class LyValidationAutoConfiguration {
    @PostConstruct
    public void init() {
        log.info("init {}", this.getClass().getName());
    }
}
