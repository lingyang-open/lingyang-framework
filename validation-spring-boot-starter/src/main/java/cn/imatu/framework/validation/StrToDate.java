package cn.imatu.framework.validation;

import cn.imatu.framework.validation.validator.StrToDateValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 字符串转日期
 *
 * @author shenguangyang
 */
@Documented
@Constraint(validatedBy = {StrToDateValidator.class})
@Target({METHOD, FIELD, CONSTRUCTOR, PARAMETER})
@Retention(RUNTIME)
public @interface StrToDate {
    String message() default "{javax.validation.constraints.DateTimeStr.message}";

    String format() default "yyyy-MM-dd HH:mm:ss";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
