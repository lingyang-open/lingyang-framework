# 自带校验注解说明

有参数传递的地方都少不了参数校验。在web开发中，前端的参数校验是为了用户体验，后端的参数校验是为了安全。试想一下，如果在controller层中没有经过任何校验的参数通过service层、dao层一路来到了数据库就可能导致严重的后果，最好的结果是查不出数据，严重一点就是报错，如果这些没有被校验的参数中包含了恶意代码，那就可能导致更严重的后果。

这里我们主要介绍在springboot中的几种参数校验方式。常用的用于参数校验的注解如下：

其中，`message` 是提示消息，`groups` 可以根据情况来分组。

**以下每一个注解都可以在相同元素上定义多个。**

## @AssertFalse

检查元素是否为 false，支持数据类型：boolean、Boolean

## @AssertTrue

检查元素是否为 true，支持数据类型：boolean、Boolean

## @DecimalMax(value=, inclusive=)

inclusive：boolean，默认 true，表示是否包含，是否等于
value：当 inclusive=false 时，检查带注解的值是否小于指定的最大值。当 inclusive=true 检查该值是否小于或等于指定的最大值。参数值是根据 bigdecimal 字符串表示的最大值。
支持数据类型：BigDecimal、BigInteger、CharSequence、（byte、short、int、long 和其封装类）

## @DecimalMin(value=, inclusive=)

支持数据类型：BigDecimal、BigInteger、CharSequence、（byte、short、int、long 和其封装类）
inclusive：boolean，默认 true，表示是否包含，是否等于
value：
当 inclusive=false 时，检查带注解的值是否大于指定的最大值。当 inclusive=true 检查该值是否大于或等于指定的最大值。参数值是根据 bigdecimal 字符串表示的最小值。

## @Digits(integer=, fraction=)

检查值是否为最多包含 `integer` 位整数和 `fraction` 位小数的数字
支持的数据类型：
BigDecimal, BigInteger, CharSequence, byte, short, int, long 、原生类型的封装类、任何 Number 子类。

## @Email

检查指定的字符序列是否为有效的电子邮件地址。可选参数 `regexp` 和 `flags` 允许指定电子邮件必须匹配的附加正则表达式（包括正则表达式标志）。
支持的数据类型：CharSequence

## @Max(value=)

检查值是否小于或等于指定的最大值
支持的数据类型：
BigDecimal, BigInteger, byte, short, int, long, 原生类型的封装类, CharSequence 的任意子类（**字符序列表示的数字**）, Number 的任意子类,
javax.money.MonetaryAmount 的任意子类

## @Min(value=)

检查值是否大于或等于指定的最大值
支持的数据类型：
BigDecimal, BigInteger, byte, short, int, long, 原生类型的封装类, CharSequence 的任意子类（**字符序列表示的数字**）, Number 的任意子类,
javax.money.MonetaryAmount 的任意子类

## @NotBlank

检查字符序列是否为空，以及去空格后的长度是否大于 0。与 `@NotEmpty` 的不同之处在于，此约束只能应用于字符序列，并且忽略尾随空格。
支持数据类型：CharSequence

## @NotNull

检查值是否**不**为 `null`
支持数据类型：任何类型

## @NotEmpty

检查元素是否为 `null` 或 `空`
支持数据类型：CharSequence, Collection, Map, arrays

## @Size(min=, max=)

检查元素个数是否在 min（含）和 max（含）之间
支持数据类型：CharSequence，Collection，Map, arrays

## @Negative

检查元素是否严格为负数。零值被认为无效。
支持数据类型：
BigDecimal, BigInteger, byte, short, int, long, 原生类型的封装类, CharSequence 的任意子类（**字符序列表示的数字**）, Number 的任意子类,
javax.money.MonetaryAmount 的任意子类

## @NegativeOrZero

检查元素是否为负或零。
支持数据类型：
BigDecimal, BigInteger, byte, short, int, long, 原生类型的封装类, CharSequence 的任意子类（**字符序列表示的数字**）, Number 的任意子类,
javax.money.MonetaryAmount 的任意子类

## @Positive

检查元素是否严格为正。零值被视为无效。
支持数据类型：
BigDecimal, BigInteger, byte, short, int, long, 原生类型的封装类, CharSequence 的任意子类（**字符序列表示的数字**）, Number 的任意子类,
javax.money.MonetaryAmount 的任意子类

## @PositiveOrZero

检查元素是否为正或零。
支持数据类型：
BigDecimal, BigInteger, byte, short, int, long, 原生类型的封装类, CharSequence 的任意子类（**字符序列表示的数字**）, Number 的任意子类,
javax.money.MonetaryAmount 的任意子类

## @Null

检查值是否为 `null`
支持数据类型：任何类型

## @Future

检查日期是否在未来
支持的数据类型：
java.util.Date, java.util.Calendar, java.time.Instant, java.time.LocalDate, java.time.LocalDateTime,
java.time.LocalTime, java.time.MonthDay, java.time.OffsetDateTime, java.time.OffsetTime, java.time.Year,
java.time.YearMonth, java.time.ZonedDateTime, java.time.chrono.HijrahDate, java.time.chrono.JapaneseDate,
java.time.chrono.MinguoDate, java.time.chrono.ThaiBuddhistDate
如果 [Joda Time](http://www.joda.org/joda-time/) API 在类路径中，`ReadablePartial` 和`ReadableInstant` 的任何实现类

## @FutureOrPresent

检查日期是现在或将来
支持数据类型：同@Future

## @Past

检查日期是否在过去
支持数据类型：同@Future

## @PastOrPresent

检查日期是否在过去或现在
支持数据类型：同@Future

## @Pattern(regex=, flags=)

根据给定的 `flag` 匹配，检查字符串是否与正则表达式 `regex` 匹配
支持数据类型：CharSequence

# 扩展校验注解

## @EnumValue

校验值是否存在于枚举中，参数中指定的枚举需要实现EnumValidate接口并实现提供的方法

# 配置文件

```yaml
validator:
  # 是否将枚举的信息添加到消息中,默认是true
  is-add-enum-value: false
```

# controller层参数校验

在controller层的参数校验可以分为两种场景：

1. 单个参数校验
2. 实体类参数校验

## 单个参数校验

```java
@Api(tags = {"validator"},value = "参数校验")
@Validated
@ResultBody
@RestController
@RequestMapping("/validator")
public class ValidatorControllerDemo {
    /**
     * 查找
     * @param grade
     * @param classroom
     * @return
     */
    @GetMapping("/")
    public String test1(
            @Range(min = 1, max = 9, message = "年级只能从1-9")
            @RequestParam(name = "grade", required = true) int grade,

            @Min(value = 1, message = "班级最小只能1")
            @Max(value = 99, message = "班级最大只能99")
            @RequestParam(name = "classroom", required = true) int classroom) {
        return grade + " " + classroom;
    }
}
```

当处理`GET`请求时或只传入少量参数的时候，我们可能不会建一个bean来接收这些参数，就可以像上面这样直接在`controller`方法的参数中进行校验。

> 注意：这里一定要在方法所在的controller类上加入`@Validated`注解，不然没有任何效果。

参数校验失败后会抛出异常，交给全局异常处理器完成

## 实体类参数校验

当处理post请求或者请求参数较多的时候我们一般会选择使用一个bean来接收参数，然后在每个需要校验的属性上使用参数校验注解：

### EmployeeEntity

```java
@Data
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class EmployeeEntity implements Serializable {

	private Integer id;

    private String name;

    @AssertFalse(message = "isFalse必须为false")
    private Boolean isFalse;

    @Length(min = 5, max = 17, message = "length长度在[5,17]之间")
    private String length;

    /**
     * 如果是空，则不校验，如果不为空，则校验
     */
    @Pattern(regexp="^[0-9]{4}-[0-9]{2}-[0-9]{2}$",message="birthday格式不正确")
    private String birthday;
    
    /**
     * @Size 不能验证Integer，适用于String, Collection, Map and arrays
     */
    @Size(min = 1, max = 3, message = "age在[1,3]之间")
    private String age;

    @Range(min = 150, max = 250, message = "high在[150,250]之间")
    private int high;

    @Size(min = 3,max = 5,message = "list的Size在[3,5]")
    private List<@NotEmpty(message = "list元素不能为空") String> list;

    /**
     * 性别  0男 1女
     */
    @EnumValue(message = "gender不在指定范围内!",enumClass = GenderEnum.class)
    private Integer gender;

    @Valid
    private DeptEntity deptEntity;

    /**
     * 用户状态 0 删除 1 正常  2停用  3 锁定
     */
    private Short statusCode;
    
    private Date createTime;


    private static final long serialVersionUID = 1L;

    public enum GenderEnum implements EnumValidate<Integer> {
        /**
         * 男
         */
        MAN(1);
        private final Integer value;
        GenderEnum(Integer value) {
            this.value = value;
        }

        public Integer getValue() {
            return value;
        }

        @Override
        public Boolean existValidate(Integer value) {
            for (GenderEnum genderEnum : GenderEnum.values()) {
                if (genderEnum.value.equals(value)) {
                    return true;
                }
            }
            return false;
        }

        public static String init() {
            return "";
        }
        @Override
        public String addMessage() {
            StringBuilder sb = new StringBuilder();
            for (GenderEnum genderEnum : GenderEnum.values()) {
                sb.append(genderEnum.value).append(",");
            }
            sb.replace(sb.lastIndexOf(","),sb.length(),"");
            return sb.toString();
        }
    }
}

```

### DeptEntity

```java
@Data
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class DeptEntity {

    private Integer id;
    @Length(min = 5, max = 17, message = "deptName长度在[5,17]之间")
    private String deptName;

    @EnumValue(message = "部门名称不存在",enumClass = DeptEnum.class)
    private String name;

    private Date createTime;

    public enum DeptEnum implements EnumValidate<String> {
        /**
         * 开发部
         */
        DEV("开发部"),
        /**
         * 销售部
         */
        MARKET("销售部");

        private final String name;

        DeptEnum(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        /**
         * 校验枚举值是否存在
         *
         * @param value 比较的值
         * @return false 或者null
         */
        @Override
        public Boolean existValidate(String value) {
            for (DeptEnum deptEnum : DeptEnum.values()) {
                if (deptEnum.name.equals(value)) {
                    return true;
                }
            }
            return false;
        }

        /**
         * 获取值,用于提示用户可以输入的值
         *
         * @return 使用逗号隔开
         */
        @Override
        public String addMessage() {
            StringBuilder stringBuilder = new StringBuilder();
            for (DeptEnum deptEnum : DeptEnum.values()) {
                stringBuilder.append(deptEnum.getName()).append(",");
            }
            stringBuilder.replace(stringBuilder.lastIndexOf(","),stringBuilder.length(),"");
            return stringBuilder.toString();
        }
    }
}
```

### controller

```java
@Api(tags = {"validator"},value = "参数校验")
@Validated
@ResultBody
@RestController
@RequestMapping("/validator")
public class ValidatorControllerDemo {

    /**
     * 保存
     * @param employeeEntity
     * @return
     */
    @PostMapping("/")
    public EmployeeEntity save(@RequestBody @Validated EmployeeEntity employeeEntity) {
        return employeeEntity;
    }

    /**
     * 更新
     * @param employeeEntity
     * @return
     */
    @PutMapping("/")
    public EmployeeEntity update(@RequestBody @Validated EmployeeEntity employeeEntity) {
        return employeeEntity;
    }

    /**
     * 删除
     */
    @DeleteMapping("/")
    public void delete(@NotEmpty(message = "id是必填参数!") String id) {
        System.out.println(id);
    }
}
```

需要注意的是，如果想让EmployeeEntity中的参数注解生效，还必须在Controller参数中使用`@Validated`注解。这种参数校验方式的校验结果会被放到`BindingResult`
中，我们这里写了一个统一的方法来处理这些结果，通过抛出异常的方式得到`GlobalExceptionHandler`的统一处理。

## 参数校验分组

### 默认提供的分组

- DeleteGroup
- InsertGroup
- SelectGroup
- UpdateGroup

在实际开发中经常会遇到这种情况：想要用一个实体类去接收多个controller的参数，但是不同controller所需要的参数又有些许不同，而你又不想为这点不同去建个新的类接收参数。比如有一个`/setUser`接口不需要`id`
参数，而`/getUser`接口又需要该参数，这种时候就可以使用**参数分组**来实现。

### 使用方法

1. 在`@Validated`中指定使用哪个组；

```java
@Api(tags = {"validator"},value = "参数校验")
@Validated
@ResultBody
@RestController
@RequestMapping("/validator")
public class ValidatorControllerDemo {
    /**
     * 保存
     * 其中Default为javax.validation.groups中的类，表示参数类中其他没有分组的参数，如果没有，接口的参数校验就只会有标记了InsertGroup的参数校验生效。
     * @param employeeEntity
     * @return
     */
    @PostMapping("/")
    public EmployeeEntity save(@RequestBody @Validated({InsertGroup.class, Default.class} )EmployeeEntity employeeEntity) {
        return employeeEntity;
    }

    /**
     * 更新
     * @param employeeEntity
     * @return
     */
    @PutMapping("/")
    public EmployeeEntity update(@RequestBody @Validated({UpdateGroup.class, Default.class}) EmployeeEntity employeeEntity) {
        return employeeEntity;
    }
}
```

2. 在实体类的注解中标记这个哪个组所使用的参数；

```java
@Data
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class EmployeeEntity implements Serializable {
    
    @NotEmpty(message = "id是必填参数",groups = {UpdateGroup.class})
	private String id;
	
    // 其余....
    
    /**
     * 性别  0男 1女
     */
    @EnumValue(message = "gender不在指定范围内!",enumClass = GenderEnum.class)
    private Integer gender;

    @Valid
    private DeptEntity deptEntity;

    /**
     * 用户状态 0 删除 1 正常  2停用  3 锁定
     */
    private Short statusCode;
    
    private Date createTime;


    private static final long serialVersionUID = 1L;

    public enum GenderEnum implements EnumValidate<Integer> {
        /**
         * 男
         */
        MAN(1);
        private final Integer value;
        GenderEnum(Integer value) {
            this.value = value;
        }

        public Integer getValue() {
            return value;
        }

        @Override
        public Boolean existValidate(Integer value) {
            for (GenderEnum genderEnum : GenderEnum.values()) {
                if (genderEnum.value.equals(value)) {
                    return true;
                }
            }
            return false;
        }

        public static String init() {
            return "";
        }
        @Override
        public String addMessage() {
            StringBuilder sb = new StringBuilder();
            for (GenderEnum genderEnum : GenderEnum.values()) {
                sb.append(genderEnum.value).append(",");
            }
            sb.replace(sb.lastIndexOf(","),sb.length(),"");
            return sb.toString();
        }
    }
}
```

### 级联参数校验

当参数bean中的属性又是一个复杂数据类型或者是一个集合的时候，如果需要对其进行进一步的校验需要考虑哪些情况呢？

```java
@Data
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class EmployeeEntity implements Serializable {
    /**
     * 性别  0男 1女
     */
    @EnumValue(message = "gender不在指定范围内!",enumClass = GenderEnum.class)
    private Integer gender;

    @NotEmpty
    private List<@NotNull @Valid DeptEntity> deptEntityList;

    @Valid
    private DeptEntity deptEntity;
}
```

比如对于`deptEntityList`参数，`@NotEmpty`只能保证list不为空，但是list中的元素是否为空、DeptEntity对象中的属性是否合格，还需要进一步的校验。这个时候我们可以这样写:

```java
@NotEmpty
private List<@NotNull @Valid DeptEntity> deptEntityList;
```

然后再继续在DeptEntity类中使用注解对每个参数进行校验。

但是我们再回过头来看看，在controller中对实体类进行校验的时候使用的`@Validated`，在这里只能使用`@Valid`
，否则会报错。关于这两个注解的具体区别可以参考[@Valid 和@Validated的关系](https://blog.csdn.net/gaojp008/article/details/80583301)
，但是在这里我想说的是使用`@Valid`就没办法对`UserInfo`进行分组校验。这种时候我们就会想，如果能够定义自己的validator就好了，最好能支持分组，像函数一样调用对目标参数进行校验，就像下面的`validObject`
方法一样：

```java
@RestController
public class PingController {
    @Autowired
    private Validator validator;

    @PostMapping("/setUser")
    public String setUser(@RequestBody @Validated UserInfo user, BindingResult bindingResult) {
        validData(bindingResult);
        Parent parent = user.getParent();
        validObject(parent, validator, GroupB.class, Default.class);
        return "name: " + user.getName() + ", age:" + user.getAge();
    }

    private void validData(BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            StringBuffer sb = new StringBuffer();
            for (ObjectError error : bindingResult.getAllErrors()) {
                sb.append(error.getDefaultMessage());
            }
            throw new ValidationException(sb.toString());
        }
    }

    /**
     * 实体类参数有效性验证
     * @param bean 验证的实体对象
     * @param groups 验证组
     * @return 验证成功：返回true；验证失败：将错误信息添加到message中
     */
    public void validObject(Object bean, Validator validator, Class<?> ...groups) {
        Set<ConstraintViolation<Object>> constraintViolationSet = validator.validate(bean, groups);
        if (!constraintViolationSet.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            for (ConstraintViolation violation: constraintViolationSet) {
                sb.append(violation.getMessage());
            }

            throw new ValidationException(sb.toString());
        }
    }
}


@Data
public class Parent {
    @NotEmpty(message = "parent name cannot be empty", groups = {GroupB.class})
    private String name;

    @Email(message = "should be email format")
    private String email;
}
```

## 自定义参数校验

虽然JSR303和Hibernate Validtor
已经提供了很多校验注解，但是当面对复杂参数校验时，还是不能满足我们的要求，这时候我们就需要自定义校验注解。这里我们再回到上面的例子介绍一下自定义参数校验的步骤。`private List<@NotNull @Valid UserInfo> parents`
这种在容器中进行参数校验是`Bean Validation2.0`的新特性，假如没有这个特性，我们来试着自定义一个**List数组中不能含有null元素**的注解。这个过程大概可以分为两步：

1. 自定义一个用于参数校验的注解，并为该注解指定校验规则的实现类
2. 实现校验规则的实现类

### 自定义注解

定义`@ListNotHasNull`注解， 用于校验 List 集合中是否有null 元素

```java
@Target({ElementType.ANNOTATION_TYPE, ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
//此处指定了注解的实现类为ListNotHasNullValidatorImpl
@Constraint(validatedBy = ListNotHasNullValidatorImpl.class)
public @interface ListNotHasNull {

    /**
     * 添加value属性，可以作为校验时的条件,若不需要，可去掉此处定义
     */
    int value() default 0;

    String message() default "List集合中不能含有null元素";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * 定义List，为了让Bean的一个属性上可以添加多套规则
     */
    @Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER})
    @Retention(RUNTIME)
    @Documented
    @interface List {
        ListNotHasNull[] value();
    }
}
```

> 注意：message、groups、payload属性都需要定义在参数校验注解中不能缺省

### 注解实现类

该类需要实现`ConstraintValidator`

```java
public class ListNotHasNullValidatorImpl implements ConstraintValidator<ListNotHasNull, List> {

    private int value;

    @Override
    public void initialize(ListNotHasNull constraintAnnotation) {
        //传入value 值，可以在校验中使用
        this.value = constraintAnnotation.value();
    }

    public boolean isValid(List list, ConstraintValidatorContext constraintValidatorContext) {
        for (Object object : list) {
            if (object == null) {
                //如果List集合中含有Null元素，校验失败
                return false;
            }
        }
        return true;
    }
}
```

然后我们就能在之前的例子中使用该注解了：

```java
@NotEmpty
@ListNotHasNull
private List<@Valid UserInfo> parents;
```

# 调用工具类进行参数校验

```java
/**
 * 更新
 * @param employeeEntity
 * @return
 */
@PutMapping("/update2")
public EmployeeEntity update2(@RequestBody EmployeeEntity employeeEntity) {
    ValidationHelper.validate(employeeEntity);
    return employeeEntity;
}
```





